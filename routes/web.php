<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home','HomeController@index')->name('home');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->middleware(['web', 'guest']);
Route::post('logout', 'Auth\LoginController@logout')->middleware(['web'])->name('logout');
Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->middleware(['web', 'guest'])->name('password.email');
Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->middleware(['web','guest'])->name('password.request');
Route::post('password/reset', 'ResetPasswordController@reset')->middleware(['web','guest']);
Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->middleware(['web','guest'])->name('password.reset');
Route::get('register', 'CustomRegisterController@showRegistrationForm')->middleware(['web','guest'])->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::post('payment/{invoice?}', 'PayController@paypal')->name('payment');
Route::get('payment', 'PayController@payment');
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::resource('periods', 'Periods');
Route::resource('settings', 'Schools');

//Route to handle news
Route::resource('news','NewsController');

//Route for user read news
Route::get('readnews','NewsController@readnews');

//Read a particular article


Route::resource('bundles','DozenController');

//Create the dozens controller
Route::resource('bundles','DozenController');

//Handle the customer sales
Route::resource('buybundles','SalesController');

//Route to handle the creation of an invite link
Route::get('/invite','InvitesController@index');

//Route to get user commissions
Route::get('/invite/usercommissions','InvitesController@usercommissions');

//Route to get user commissions
Route::get('/invite/userinterests','InvitesController@userinterests');

//Route to get user commissions
Route::get('/invite/userstatement','InvitesController@userstatement');

//Post of the user to create a withdraw request
Route::post('/invite/withdrawrequest','InvitesController@postwithdrawrequest')->middleware('auth');

//Resource controller to handle the registration
Route::resource('registration','RegistrationController');

//Route to handle registration with paypal
Route::resource('pregistration','PalRegistrationController');

//It handles the display of daily transactions
Route::resource('invest','InvestController');

//Route to handle reinvesting
Route::resource('reinvest','ReinvestController');

//Handles the posting of expenses transactions after the end of day
Route::resource('postjournal','PostJournalTransController');

//Handles the posting of income transactions
Route::resource('postinc/fathers','PostFathersIncomeTransController');

//It handles the display of daily expenses transactions
Route::resource('journal','PeriodicalController');

//Handles the posting of expenses transactions after the end of day
Route::resource('post/fathers','PostFathersExpTransController');

//Search mpesa registrations codes
Route::get('/cashbook/mpesacodes','CashBookController@mpesacodes');

//Route to handle the post of mpesacodes
Route::post('/cashbook/postmpesacodes','CashBookController@postmpesacodes');

//Controller to get the due interess
Route::get('/cashbook/interests','CashBookController@interests');

//Route to handle the processing of payments
Route::post('/cashbook/postinterests','CashBookController@postinterests');

//Route to check the processed withdraws
Route::get('/cashbook/withdraws','CashBookController@withdraws')->middleware('auth');

Route::post('/cashbook/postwithdraws','CashBookController@postwithdraws');

//Route to handle the processing of withdraws
Route::post('/cashbook/postwithdraws','CashBookController@postwithdraws');

//Handles the processing of vouchers
Route::resource('cash/vouchers','CashVoucherController');

//Diisplay the master reports for the admininstrator
Route::resource('master/reports','MasterReportController',['only'=>['index']]);

//Voteheads
Route::resource('voteheads', 'AccountTypeController');

//All system accounts
Route::resource('accounts', 'AccountController');

//Get the incomes dashboard
Route::get('incomesdash', function()
{
 return View ('vouchers.income_dash');
});

//It handles the display income journal transactions
Route::resource('income/journal','IncomePeriodicalController');

//It handles the display asset journal transactions
Route::resource('investjournal','InvestJournalController');

//Display the expenses menu
Route::resource('expenses','ExpenseController');

//Displays the expenditure reports page
Route::get('general/reports',function(){

    return view ('cover.general_reports');
    });
//Get a list of all system accounts
Route::resource('system/accounts','SystemAccountsController');

//Handles the display of divisions
Route::resource('divisions','DivisionController',['only' => ['index']]);

//Trial balance request
Route::get('tb/fathers','TrialBalanceController@fathers');

//Post to search the trial balance
Route::post('tb/postfathers','TrialBalanceController@postfathers');

//General ledger request
Route::get('gledger/fathers','TrialBalanceController@gledger');

//Post to search the trial balance
Route::post('postgledger','TrialBalanceController@postgledger');

//Displays the income reports page
Route::get('inc/reports',function(){

    return view ('cover.master_income_reports');
    });

//get the incomes within a range
Route::resource('incomerange/reports','IncomeController');

//Get the votehead incomes
//Route::get('voteheadincomes','IncomeController@voteheadincomes');

//Display student fee transactions under specific accounts
Route::resource('incomes/rpt','IncomesAccountRptController');

//Handle the bank transfers Father's Expenditure
Route::resource('frbanktransfers','FrBankTransfersController');

//Display the expenditure reports
Route::get('expenditure/reports',function(){

    return view ('cover.expenditure_reports');
    });

//Searching Expenses within a Range
Route::resource('expenditures','ExpenditureController');

//Father's Cash Book Income and Expenditure Report
Route::resource('frincomeexpenditure','FrIncomeExpenditureController');

//Users controller
Route::resource('users','UserController');
