Press Ctrl+Shift+V to view this message if you are using VSCode

# Model
I have create a new model to store invoices
Once an invoice is paid, the status will be 'PAID' and the
ref will be "'paypal ' {{ date of payment }} 'paypalID'"

You can check if the invoice is paid and redirect user as required

# Controller
The controller handling the logic is PayController

## line #62
Checks if you has passed an invoice id in your post request
If none is passed,
  1. it generates one for the user
  2. check's if it's paid
  3. Redirect if paid

## line #71 & #79
Details & description for payment

## line #78
Currency
[Supported currencies](https://developer.paypal.com/docs/api/reference/currency-codes/)

## line #81
as per comment

## line #85
Must stay as it is

## line #87
You can create similar items and pass them in the array in line #94

## line #113
Redirect urls; success url redirects to PayController@payment to execute the payment


## line #122
the try block attempts payment with the set parameters, incase of errors, you can see it in the storage/logs/paypal.log file

## line #131
redirect to paypal for the user authorization.

After the user pays, paypal redirect back to our return url with several route query for payment execution

## line #181
The invoice has now been paid

# Test credetials for payment

email: mwatha.kinyua-buyer@hotmail.com

password: 12345678

This account has $9000

The paypal sandbox credentials are in .env.example

Create an app at [Paypal](https://developer.paypal.com/developer/applications/) and test accounts [here](https://developer.paypal.com/developer/accounts/)

# Don't forget to run
```
composer update
```
```
php artisan migrate
```
```
php artisan config:clear
```
