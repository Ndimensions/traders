<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promote extends Model {

	protected $dates = ['deleted_at'];

	protected $fillable=['amount','user_id','paid_flag','mature_flag','transaction_date','transaction_id','expiry_date','code'];

}
