<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

	protected $fillable=['transaction_no'];
	
		//A transaction has many tiers items
		public function tier() {
		return $this->hasOne('App\Tier');
	}

	//A transaction has many briefs
	public function brief() {
			return $this->hasOne('App\Brief');
	}

	//A transaction has many briefs
	public function bank() {
			return $this->hasOne('App\Bank');
	}

	//A transaction has many vouchers
	public function voucher() {
			return $this->hasOne('App\Voucher');
	}

	//A transaction has many vouchers
	public function periodicals() {
			return $this->hasMany('App\Periodical');
	}

	//A transaction has many vouchers
	public function invests() {
		return $this->hasMany('App\Invest');
	}

	//A transaction has many vouchers
	public function incomes() {
			return $this->hasMany('App\Income');
	}

	//A transaction one or many cheques
	public function cheques() {
			return $this->hasMany('App\Cheque');
	}



}
