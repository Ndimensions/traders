<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brief extends Model {

	use SoftDeletes;

    protected $dates = ['deleted_at'];

		protected $fillable=['ledger_id','transaction_id','transaction_type','amount','posting_date','transaction_date','deletion_flag','creaated_at','updated_at','category','particulars','description'];

	// A brief belongs to a ledger
	public function ledger()
	{
		return $this->belongsTo('App\Ledger');
	}

	// A brief belongs to a transaction
	public function transaction()
	{
		return $this->belongsTo('App\Transaction');
	}


}
