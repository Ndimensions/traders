<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sole extends Model
{
    protected $fillable=['financial_id','transaction_id','transaction_type','posting_date','debit','credit','amount','user_id','transaction_date'];

	public function user(){
			return $this->belongsTo('App\User');
		}

}
