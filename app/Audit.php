<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model {

	//
       protected $fillable = ['referer', 'origin', 'request', 'method', 'user_id'];


     //A user has many audit 
	 public function user(){
	  return $this->belongsTo('App\User');
	 }
}
