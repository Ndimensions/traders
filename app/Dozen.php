<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Dozen extends Model {
   
    use SoftDeletes;

	//
	 protected $dates = ['deleted_at'];

	protected $fillable=['user_id','category_id','name','min','max','expirydate','rate'];

}
