<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commission extends Model {

	protected $dates = ['deleted_at'];

	protected $fillable=['promote_id','amount','user_id','paid_flag','mature_flag','transaction_date','maturity_date','transaction_id','code','cust_id'];

//An commission record belongs to a user
public function user(){
	return $this->belongsTo('App\User');
   }

}
