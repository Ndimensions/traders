<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Division extends Model {

	//
	protected $fillable=['name','description'];

		use SoftDeletes;

	    protected $dates = ['deleted_at'];


	//A division belongs to a fund
	public function fund()
	{
		return $this->belongsTo('App\Fund');
	}
	//A division has many functionbs
	// public function routines()
	// {
	// 	return $this->hasMany('App\Routine');
	// }

	//A division has many types
	public function types()
	{
		return $this->hasMany('App\Type');
	}

	public function ledgers()
    {
        return $this->hasManyThrough('App\Ledger', 'App\Type');
    }

}
