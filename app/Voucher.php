<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Voucher extends Model {

use SoftDeletes;

    protected $dates = ['deleted_at'];
	//
protected $fillable=['id','cust_id','ref','transaction_date','periodical_id','transaction_no','transaction_type','user_id','amount','account_id','type_id','description','code',
	'financial_id','comments','payees_name','posted_flag','credit','debit','deletion_flag','deleted_by','deletion_date','reversion_flag','current_balance','transaction_id','reversed_by','reversion_date','particulars','category'];


	//An account has many voucher transactions
	public function periodical()
	{
		return $this->hasOne('App\Periodical');
	}

	//An voucher transaction one journal entry
	public function journal()
	{
		return $this->belongsTo('App\Journal');
	}

	//A voucher is transacted against an account
	public function account()
	{
		return $this->belongsTo('App\Ledger');
	}

	//An voucher has one expenses transactions relating to creditors
	public function expense()
	{
		return $this->hasOne('App\Expense');
	}

	//An voucher belongs to a transaction
	public function transaction()
	{
		return $this->belongsTo('App\Transaction');
	}

}
