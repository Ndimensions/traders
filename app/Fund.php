<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fund extends Model {

	//
////A type has many accounts

	public function divisions()
	{
		return $this->hasMany('App\Division');
	}
	public function routines()
	{
		return $this->hasMany('App\Routine');
	}

	public function types()
	{
		return $this->hasMany('App\Type');
	}

	public function accounts()
	{
		return $this->hasMany('App\Account');
	}



}
