<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Withdraw extends Model {

	protected $dates = ['deleted_at'];

	protected $fillable=['amount','paid_flag','user_id','cust_id','transaction_date','posting_date'];

	 //An interest record belongs to a useer
	public function user(){
		return $this->belongsTo('App\User');
	   }
}
