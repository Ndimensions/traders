<?php namespace App;

use Illuminate\Database\Eloquent\Model;

//This is the journals table for the expenses of the system sicne the incomes uses the journals table

class Periodical extends Model {

	protected $fillable=['transaction_id'];
 
	//A periodical is associated with a transaction
	public function transaction()
	{
		return $this->belongsTo('App\Transaction');
	}
 
	 
}
