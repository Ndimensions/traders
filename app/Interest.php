<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interest extends Model {

	
	protected $dates = ['deleted_at'];

	protected $fillable=['status','principal','amount','rate','user_id','paid_flag','mature_flag','transaction_date','maturity_date','transaction_id','interest','process_flag','cust_id'];

	
	 //An interest record belongs to a user
	 public function user(){
		return $this->belongsTo('App\User');
	   }
}
