<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Auth;
use Carbon;
use App\Type;
use Session;
use Redirect;
use App\Brief;
 
use App\Expense;

class IncomeController extends Controller {

 var $first_date;
 var $last_date;

 var $first_date_variation;
 var $last_date_variation;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('expenses.income_search_range');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		 
		
		$first_date = $request->input('first_date');
		$second_date = $request->input('second_date');


		if($first_date ==="" && $second_date===""){

				Session::flash('message', 'At Least one Date is required');
		        return Redirect::to('incomerange/reports');
		}
		else
		{


				$deletion_flag = 1;

				$values = array($deletion_flag);

				$range_expense=Brief:://whereNotIn('deletion_flag', $values) 
										whereBetween('transaction_date', array($first_date, $second_date))
										->where('transaction_type', 'Credit') 	
										->where('description','Income')
										->get();

				$receipts=$range_expense->sum('amount');	
 
				
		        //return ('students.search_res',compact('student'));
				if($receipts>0)
				{
					 	
				 return view('cover.range_incomes',compact('range_expense','receipts','first_date','second_date'));
				 //$range_expense->setPath('expenditures');
				}
				else
				{
				Session::flash('message', 'Income transactions not entered.');
		        return Redirect::to('incomerange/reports');
				}
		}
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function voteheadincomes(){

	$expense_flag = 1;

	$expense_values = array($expense_flag);

	$acctypes=Type::whereIn('division_id', $expense_values)
							->get();


	return view('account_types.search_votehead_balances_fr',compact('acctypes'));
	}
	

}
