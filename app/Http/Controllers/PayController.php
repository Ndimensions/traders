<?php

namespace App\Http\Controllers;

use Auth;
use App\Invoice;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use Mockery\CountValidator\Exception;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Redirect;
use PayPal\Exception\PayPalConnectionException;
use Illuminate\Support\Facades\Cache;
use PayPal\Api\Payout;
use PayPal\Api\PayoutSenderBatchHeader;
use PayPal\Api\PayoutItem;
use PayPal\Api\Currency;
use App\Notifications\AdvertInvoicePaid;
use App\Notifications\ContractInvoicePaid;
use App\User;
use Carbon\Carbon;
use function GuzzleHttp\json_decode;

class PayController extends Controller
{
    private $paypal, $id, $secret;

    public function __construct()
    {
        $this->middleware('auth');

        $id = config('paypal.id');
        $secret = config('paypal.secret');

        $this->id = $id;
        $this->secret = $secret;

        $paypal = new ApiContext(
            new OAuthTokenCredential(
                $id,
                $secret
            )
        );

        $paypal->setConfig(config('paypal.settings'));
        $this->paypal = $paypal;
    }
    /**
     * Pay contract
     */
    public function paypal(Request $request, $invoice = null)
    {
        if (!$invoice) {
            $user = Auth::user();
            $invoice = Invoice::firstOrCreate(['user_id' => $user->id], ['amount' => 100]);
            if ($invoice->status != 'unpaid' && $invoice->ref) {
                return back()->with(['status' => 'Invoice already paid']);
            }
        }
        $amount = $invoice->amount;
        // product name
        $name = 'Pre-registration';

        $shipping = 0.00;
        $tax = 0.00;
        $price = $amount;
        $product = $name;
        // SET currency
        $currency = 'USD';
        $description = 'Pre-registration Payment';

        /* Prevents a payment from being repeated */
        $invoiceId = $invoice->id;

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item1 = new Item();
        $item1->setName($product)
            ->setCurrency($currency)
            ->setQuantity(1)
            ->setPrice($price);

        $items = new ItemList();
        $items->setItems([$item1]);

        $details = new Details();
        $details->setShipping($shipping)
            ->setTax($tax)
            ->setSubtotal($price);

        $amount = new Amount();
        $amount->setCurrency($currency)
            ->setTotal($price + $tax + $shipping)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($items)
            ->setDescription($description)
            ->setInvoiceNumber($invoiceId);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('payment?sucess=true&invoiceId=' . $invoice->id))
            ->setCancelUrl(url('/pregistration/create?payment=failure&cause=USER_CANCELLED_TRANSACTION'));

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        try {
            $payment->create($this->paypal);
        } catch (PayPalConnectionException $e) {
            \Log::info($e);
            \Log::info($e->getData());
            return redirect('/pregistration/create?payment=failure&cause=CONNECTION_TO_PAYPAL_UNSUCCESSFUL')->with('status', 'CONNECTION TO PAYPAL UNSUCCESSFUL');
        }
        $approvalUrl = $payment->getApprovalLink();

        return redirect($approvalUrl);
    }

    /**
     * Execute payment for contract
     */
    public function payment(Request $request)
    {
        $invoiceId = $request->query('invoiceId');

        if (!$request->query('PayerID') || !$request->query('token')) {
            return redirect('/pregistration/create?payment=failure&cause=MISSING_PARAMETER(S)')->with('status', 'MISSING_PARAMETER(S)');
        }

        $paymentId = $request->query('paymentId');
        $PayerID = $request->query('PayerID');

        try {
            $payment = Payment::get($paymentId, $this->paypal);
            // return $payment;
        } catch (\PayPal\Exception\PayPalConnectionException $e) {
            \Log::error($e);
            \Log::error($e->getData());
            $message = $e->getMessage();
            \Log::error($message);
            return redirect('/pregistration/create?payment=failure&cause=CONNECTION_TO_PAYPAL_UNSUCCESSFUL')->with('status', 'CONNECTION TO PAYPAL UNSUCCESSFUL');
        }

        $execute = new PaymentExecution();
        $execute->setPayerId($PayerID);

        try {
            $result = $payment->execute($execute, $this->paypal);
        } catch (PayPalConnectionException $e) {
            \Log::error($e);
            \Log::error($e->getData());
            $message = $e->getData();
            $encoded = json_encode($message);
            return redirect('/pregistration/create?payment=failure&cause=CONNECTION_TO_PAYPAL_UNSUCCESSFUL')->with('status', 'CONNECTION TO PAYPAL UNSUCCESSFUL');
        }

        $id = $result->getId();

        if ($result->getState() == 'approved') {
            if ($invoiceId) {
                Invoice::where('id', $invoiceId)->update([
                    'ref' => 'Paypal '. Carbon::now()->toDateTimeString() . ' '.$id,
                    'status'    => 'PAID'
                ]);
            }
            return redirect('/pregistration/create?success=paypal')->with('status', 'Successfully Paid');
        }

        return redirect('/pregistration/create')->with('status', 'Unknown error: Contact Admin');
    }
}
