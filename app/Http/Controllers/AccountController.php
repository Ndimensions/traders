<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Exceptions\DatabaseStoreException;

use Illuminate\Http\Request;

use App\Type;
use App\Ledger;
use App\Division;
use App\Brief;
use Auth;
use Carbon;
use Session;
use App\Voucher;
use Redirect;
use DB;

class AccountController extends Controller {


	public function __construct()
	{
		$this->middleware('auth');
         
	}
protected  $rules = ['name' => ['required', 'min:3','unique:Ledgers'],
					'description' => ['required'],
					'type_id'=>	['required'],
					//'code'=>['required']
					//'active_flag'=>['required']
					];

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	
		$subcriber_type=7;
		$values=array($subcriber_type);
		//Determing all the financial divisions
		$revenue_division=division::find(1);
		$expense_division=division::find(2);
		$asset_division=division::find(3);
		$liabilities_division=division::find(4);

		//Get all the accounts under each column
		$revenues=$revenue_division->ledgers;
		$expenses=$expense_division->ledgers;
		$assets=$asset_division->ledgers;

		$types=Type::select('id')
					->where('division_id',4)
					->whereNotIn('id',$values)->get()->toArray();
		$liabilities=Ledger::whereIn('type_id',$types)->get();
		 		 

		return view('accounts.index',compact('revenues','expenses','assets','liabilities'));

 	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	
	$account_types = Type::pluck('name')->all();

	return View('accounts.create',compact('account_types'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		 $this->validate($request, $this->rules);

		 $id=$request->input('type_id');
		 $type_id=$request->input('type_id');

DB::beginTransaction();

		//Get the votehead of the account
		$type = Type::findorfail($type_id);
		$type_id=$type->id;
		$fund_id=1;
		$division_id=$type->division_id;
		 

		//Create the record of the account in the ledger
		$ledger=Ledger::create([
		'name'=>$request->input('name'),
		'description'=>$request->input('description'),
		'type_id'=>$type_id,
		'user_id'=>Auth::user()->id	 
		]);
 
 
DB::commit();

		 if($ledger)
        {

       	Session::flash('message', 'Successfully created account.');
        return Redirect::to('accounts');
        }

        Session::flash('message','Failed to create record');
         return Redirect:: to('accounts');
		}

	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$account=Ledger::find($id);

		//$accounts->load('type');
       
        // $account_type=$subjects->toArray();
        return View ('accounts.show',compact('account'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$account_types = Type::lists('name','id');
		
		$account = Account::find($id);

        // show the edit form and pass the nerd
        return View('accounts.edit',compact('account','account_types'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)

	{
		$account = Account::find($id);

		$deletion_flag = 1;

		$values = array($deletion_flag);


		//if(count($account->expenses()->whereNotIn('deletion_flag', $values)->get()) >0 )
		if(count($account->expenses()->get()) >0 )

		{
			  
        Session::flash('message', 'Update failed,account has associated transactions.');
        return Redirect::to('accounts');
		}

		$rules = ['name' => ['required', 'min:3','unique:Accounts,id,{$id}'],
					'description' => ['required'],
					'type_id'=>	['required']
					//'active_flag'=>['required']
					];

		$this->validate($request, $rules);

		//Update the ledger record
 		$ledger_id=$account->ledger_id;
 		$ledger=Ledger::find($ledger_id);
		$ledger=$ledger->update(array(
			'name'=>$request->input('name'),
		 	'description'=>$request->input('description')
		 	));
 		

		$account->update(array(
		 	'type_id'=>$request->input('type_id'),
		 	));
 
		 if($account)
        {

       	Session::flash('message', 'Successfully updated account,');
        return Redirect::to('accounts');
        }

        Session::flash('message','Failed to create record.');
         return Redirect:: to('accounts');
		}
	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$account = Ledger::find($id);

		$ledger_id=$account->ledger_id;

		$deletion_flag = 1;

		$values = array($deletion_flag);


		//if(count($account->expenses()->whereNotIn('deletion_flag', $values)->get()) >0 )
		if(Voucher::Where('credit',$ledger_id)
					->orwhere('debit',$ledger_id)->exists())

		{	  
        Session::flash('message', 'Deletion failed,account has associated transactions.');
        return Redirect::to('accounts');
    	}else
		{
		 
		DB::beginTransaction();	
				 
		$account->delete();

		DB::commit();
		Session::flash('message', 'Successfully deleted the account.');
        return Redirect::to('accounts');
		}
       

      
	}
	

}
