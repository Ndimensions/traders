<?php namespace App\Http\Controllers;

use App\Financial;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Promote;
use App\Commission;
use App\Interest;
use App\Ledger;
use App\Statement;
use App\Withdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Redirect;


class InvitesController extends Controller {

	//function to generate th invite link
	public  function Index(){

		//Generate the code and insert to the prmotes table
		$user_id=Auth::user()->id;
		$code=Promote::where('user_id',$user_id)->first()->code;
			
		 $url=config('app.url');
		 $link=$url.'/'.'register?utm_source='.$code;
		
		 $commissions=Commission::where('code',$code)->orderBy('created_at','desc')->get();

		return view('commissions.invite_link',compact('commissions','link'));
	}//end of getindex

	//Get user commsisions
	public function usercommissions(){

		$user_id=Auth::user()->id;	//Get user id
		$cust_id=Ledger::where('user_id',$user_id)->first()->id;

		$paid_commissions=Commission::where('cust_id',$cust_id)
									->where('paid_flag',1)
									->where('mature_flag',1)
									->orderBy('created_at','Asc')->get();

		$in_process=Commission::where('cust_id',$cust_id)
								->where('paid_flag',0)
								->where('mature_flag',1)
								->orderBy('created_at','Asc')->get();

		return view('commissions.index',compact('paid_commissions','in_process'));
	}//end of the getusercommisions

	//Get user interest
	public function userinterests(){

		$user_id=Auth::user()->id;	//Get user id
		$cust_id=Ledger::where('user_id',$user_id)->first()->id;
		$record=$cust_id;

		$paid_interests=Interest::where('cust_id',$cust_id)
									->where('paid_flag',1)
									->where('mature_flag',1)
									->where('status',0)
									->orderBy('created_at','Asc')->get();

		$in_process=Interest::where('cust_id',$cust_id)
								->where('paid_flag',0)
								->where('mature_flag',1)
								->orderBy('created_at','Asc')->get();

		$reinvest=Interest::where('cust_id',$cust_id)
							->where('paid_flag',0)->exists();
		 
		$credit=Statement::where('transaction_type','Credit')
							->where('cust_id',$cust_id)->get()->sum('amount');
		$debits=Statement::where('transaction_type','Debit')
							->where('cust_id',$cust_id)->get()->sum('amount');

		$withdraw_requests=Withdraw::where('cust_id',$cust_id)
							->where('paid_flag',0)
							->orderBy('created_at','Asc')
							->get()->sum('amount');

		$balance=$credit-$debits-$withdraw_requests;
		 
		return view('commissions.interest_index',compact('paid_interests','in_process','reinvest','balance','record'));

		 
	}//end of the getusercommisions

	//Get user statement
	public  function userstatement(){

		$user_id=Auth::user()->id;	//Get user id
		$cust_id=Ledger::where('user_id',$user_id)->first()->id;

		$statement=Statement::where('cust_id',$cust_id)
								->orderBy('created_at','Asc')->get();

		$withdraw_requests=Withdraw::where('cust_id',$cust_id)
									->where('paid_flag',0)
									->orderBy('created_at','Asc')
									->get();
	
		return view('commissions.statement',compact('statement','withdraw_requests'));
	}//end of the getusercommisions

	//post statement to handle a withdraw request
	public  function postwithdrawrequest(Request $request){
		
		$user_id=Auth::user()->id;
		$cust_id=Ledger::where('user_id',$user_id)->first()->id;
		$amount=$request->input('amount');
		$transaction_date=Carbon::now('EAT');
		//Create the record in the withdraws tabel
		Withdraw::create([
			'user_id'=>$user_id,
			'amount'=>$amount,
			'cust_id'=>$cust_id,
			'posting_date'=>$transaction_date,
			'transaction_date'=>$transaction_date
		]);
		
        Session::flash('message', 'Successfully sent request');
		return redirect()->back();	

	}//end of postwithdrawrequest

}
