<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Exceptions\DatabaseStoreException;

use Illuminate\Http\Request;
use App\Dozen;
use Auth;
use Session;
use Redirect;
use DB;

class DozenController extends Controller {


	public function __construct()
	{
		$this->middleware('auth');
        
	}

protected  $rules = ['name' => ['required', 'min:3','unique:Ledgers'],
					'min' => ['required'],
					'max' => ['required'],
					'date'=>	['required']
					];

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	
		$dozens=Dozen::all();	 		 
		return view('dozens.index',compact('dozens'));

 	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
       return View('dozens.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		 $this->validate($request, $this->rules);
		 
		 //Get the use input
		 $name=$request->input('name');
		 $min=$request->input('min');
		 $max=$request->input('max');
		 $date=$request->input('date');
		 $rate=$request->input('rate');
		 $category_id='1';
		 $user_id=Auth::user()->id;
		 
		DB::beginTransaction();
	
			//Create the record of the account in the ledger
			$dozen=Dozen::create([
				'name'=>$name,
				'min'=>$min,
				'max'=>$max,
				'rate'=>$rate,
				'expirydate'=>$date,
				'user_id'=>$user_id, 
				'category_id'=>$category_id
			]);
 
		DB::commit();

		if($dozen){

       	Session::flash('message', 'Successfully created bundle ');
        return Redirect::to('bundles/create');
        }

        Session::flash('message','Failed to create record');
        return Redirect:: to('bundles/create');
		
	}

	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$account=Ledger::find($id);


        return View ('dozens.show',compact('account'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
		$dozen = Dozen::find($id);

        // show the edit form and pass the dozen
        return View('dozens.edit',compact('dozen'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)

	{

		$dozen = Dozen::find($id);

		$rules = ['name' => ['required'],
					'amount' => ['required'],
					'date'=>	['required']
					//'active_flag'=>['required']
					];

		$this->validate($request, $rules);

		
		$dozen=$dozen->update(array(
			'name'=>$request->input('name'),
		 	'amount'=>$request->input('amount'), 
		    'expirydate'=>$request->input('date'),
			'rate'=>$request->input('rate')
		 	));
 		
 
		 if($dozen)
        {

       	Session::flash('message', 'Successfully updated bundle details');
        return Redirect::to('bundles');
        }

        Session::flash('message','Failed to create record.');
         return Redirect:: to('bundles');
		}
	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$dozen=Dozen::find($id);
		
     	DB::beginTransaction();	
				 
		$dozen->delete();

		DB::commit();
		Session::flash('message', 'Successfully deleted the bundle.');
        return Redirect::to('bundles');
      
	}
	

}
