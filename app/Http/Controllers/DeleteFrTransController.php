<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Brief;
use App\Bank;
use App\History;
use App\Voucher;
use App\Transaction;
use App\Periodical;
use App\Account;
use App\Financial;
use App\Income;
use Session;
use Carbon;
use Auth;
use DB;
use Redirect;

class DeleteFrTransController extends Controller {

	//Display the form to enter a transaction 
	// public function getTransaction(){

	// }

 


	//Get the fr transaction 
	function getFrexptransactions(){
		 
		 //Return the view to display the transaction
		 return view('expenses.search_del_range');

	}


	//Displays a list of transactions matching the criterial
	function postFrexptransactions(Request $request){

		$first_date = $request->input('first_date');
		$second_date = $request->input('second_date');


		if($first_date ==="" && $second_date===""){

				Session::flash('message', 'At Least one Date is required');
		        return Redirect::back();
		}
		else
		{
 				$deletion_flag=1;

				$values = array($deletion_flag);
 			
				 
					$range_expense=Voucher::whereNotIn('deletion_flag', $values) 
											->where('posted_flag',1)
											->whereBetween('transaction_date', array($first_date, $second_date))
				        	        		->where('transaction_type', 'Expense') 				    
		        							->get();

		        	$receipts=$range_expense->sum('amount');	

		         
	
				
		        //return ('students.search_res',compact('student'));
				if($range_expense->count()>0)
				{
				 return view('expenses.del_expenses_res',compact('range_expense','receipts','first_date','second_date'));
				 //$range_expense->setPath('expenditures');
				}
				else
				{
				Session::flash('message', 'Results not available');
		        return Redirect::back();
				}
		}
	}

	 

	//Button initiated for deletion.
	public function postFrexpconfirmtrans(Request $request){


	   $value=$request->input('value');
	   $voucher=Voucher::find($value);
	   $transaction_date=Carbon::now('EAT');

	   //Get the transaction id
	   $transaction_id=$voucher->transaction_id;


		DB::beginTransaction();
		//Update the voucher reversion flag to true
 		$reversion_update=array(
			'reversion_flag'=>1,
			'reversion_date'=>$transaction_date,
			'reversed_by'=>Auth::user()->id
			);
 		$vouchers=$voucher->update($reversion_update);

 		$voucher->delete();

 		//Get all the briefs
 		$briefs=Brief::where('transaction_id',$transaction_id)->get();

 		foreach ($briefs as $key => $value) {
 			$brief=Brief::find($value->id);
 			$brief->delete();
 		}
		 
 		DB::commit();
		 
		  Session::flash('message', 'Successfully deleted.');
		  return Redirect::back();

	}//end of frexpconfirmtrans

	//Get the fr transaction 
	function getFrinctransactions(){
		 
		 //Return the view to display the transaction
		 return view('expenses.search_inc_del_range');

	}


	//Displays a list of transactions matching the criterial
	function postFrinctransactions(Request $request){

		$first_date = $request->input('first_date');
		$second_date = $request->input('second_date');

 
		if($first_date ==="" && $second_date===""){

				Session::flash('message', 'At Least one Date is required');
		        return Redirect::back();
		}
		else
		{
				$deletion_flag = 1;
				$reversion_flag=1;
				 
				$values = array($deletion_flag);

				
			 

					$range_expense=Voucher::whereNotIn('deletion_flag', $values) 
										    ->where('posted_flag',1)
											->whereBetween('transaction_date', array($first_date, $second_date))
				        	        		->where('transaction_type', 'Income') 				    
		        							->get();

		        	$receipts=$range_expense->sum('amount');	

		         
				

				
		        //return ('students.search_res',compact('student'));
				if($range_expense->count()>0)
				{
				 return view('expenses.del_incomes_res',compact('range_expense','receipts','first_date','second_date'));
				 //$range_expense->setPath('expenditures');
				}
				else
				{
				Session::flash('message', 'Results not available');
		        return Redirect::back();
				}
		}
	}


	//Button initiated for deletion.
	public function postFrincconfirmtrans(Request $request){

 	$value=$request->input('value');
	   $voucher=Voucher::find($value);
	   $transaction_date=Carbon::now('EAT');

	   //Get the transaction id
	   $transaction_id=$voucher->transaction_id;


		DB::beginTransaction();
		//Update the voucher reversion flag to true
 		$reversion_update=array(
			'reversion_flag'=>1,
			'reversion_date'=>$transaction_date,
			'reversed_by'=>Auth::user()->id
			);
 		$vouchers=$voucher->update($reversion_update);

 		$voucher->delete();

 		//Get all the briefs
 		$briefs=Brief::where('transaction_id',$transaction_id)->get();

 		foreach ($briefs as $key => $value) {
 			$brief=Brief::find($value->id);
 			$brief->delete();
 		}
		 
 		DB::commit();
		 
		  Session::flash('message', 'Successfully deleted.');
		  return Redirect::back();

	}



	public function getFrbankupdates(){
		$banks=Bank::get();

		DB::beginTransaction();
		foreach ($banks as $key => $value) {
			//Create the record in the briefs table for the credit bank and cash
			       	$banks=Brief::create([ 
			            'transaction_id'=>$value->transaction_id,
			            'posting_date'=>$value->posting_date,
			            'transaction_date'=>$value->transaction_date,
			            'transaction_type'=>$value->transaction_type,
			            'ledger_id'=>$value->ledger_id,
			            'amount'=>$value->amount,
			            'created_at'=>$value->created_at,
			            'updated_at'=>$value->updated_at
	            	]);
		}

		DB::commit();
		dd('complete');
	}


	// //Update the expense transactions and the income transactions
	// public function getTransupdates(){

	// 	$transaction_type='Income';
	// 	$vouchers=Voucher::where('debit','=',1)->get();
	// 	//where('debit', 'LIKE','%'.$transaction_type.'%')->get();

	// 	foreach ($vouchers as $key => $value) {
	// 		$id=$value->id;
	// 		$update=array(
	// 			'transaction_type'=>'Income'
	// 			);

	// 		$voucher=Voucher::find($id);
	// 		$voucher->update($update);	
	// 	}

	// 	dd('complete update');




	// }
}

