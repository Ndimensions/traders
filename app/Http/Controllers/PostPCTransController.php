<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Version;
use App\Book;
use App\Temporary;
use App\Paper;
use App\Slip;
use App\History;
use App\User;
use App\Archive;
use Carbon;
use Auth;
use DB;

class PostPCTransController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
			//
 	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$deletion_flag = 1;
		$values = array($deletion_flag);
		$transactions_array=array();
		$not_posted_flag=0;
		$np_values=array($not_posted_flag);

		//Declare an array to hold the ledgers that are being posted
		$ledger_array=array();

		$journals=Paper::all();

DB::beginTransaction();
	 
		
	 
		foreach ($journals as $key => $value)
			{
				   
				$slip_id=Slip::where('operation_id','=',$value->operation_id)->pluck('id');
				$slip=Slip::find($slip_id);    
				$posted_update=array(
		        	'posted_flag'=>'1'
		        	);
		        $slip->update($posted_update);


		        $trans=slip::find($value->operation->slip->id);
		        $debit_acc=$trans->debit;

		        //Get the record with the debit account
		        $vouch_debit=slip::where('debit','=',$debit_acc)
		        						->where('operation_id',$value->operation_id)
		        						->first();


		       	//Create the record in the temporaries table for the other accounts entry
		       	$temporaries=Temporary::create([
		            'operation_id'=>$value->operation_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_type'=>'Debit',
		            'book_id'=>$debit_acc,
		            'amount'=>$vouch_debit->amount
            	]);


            	$credit_acc=$trans->credit;

		        //Get the record with the debit account
		        $voucher_credit=SLip::where('credit','=',$credit_acc)
		        						->where('operation_id',$value->operation_id)
		        						->first();


		       	//Create the record in the briefs table for the credit bank and cash
		       	$temporaries=Temporary::create([ 
		            'operation_id'=>$value->operation_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_type'=>'Credit',
		            'book_id'=>$credit_acc,
		            'amount'=>$voucher_credit->amount
            	]);
		  			
 }
	 DB::commit();

	 DB::beginTransaction();
 			
 			//Get the transactions in the briefs table and od the totals
			$papers=Paper::all();

			foreach ($papers as $key => $value) {
			

			 $transaction=Slip::find($value->operation->slip->id);

		     $debit_acc=$transaction->debit;


		     //Get the record with the debit account
		     $trans=Temporary::where('book_id','=',$debit_acc)
		        						//->where('posting_date','=',Carbon::now('EAT'))
		        						->where('transaction_type','=','Debit')
		        						->sum('amount');
		     //Post to the histories table the summarized amounts   						
		     if($trans>0){
			     $history_record=Archive::create([
	        	'book_id'=>$debit_acc,
	           	'transaction_type'=> 'Debit',
	        	'amount'=>$trans,
	        	'posting_date'=>Carbon::now('EAT')
	        	]); 

			     $user = User::find(Auth::user()->id);
				$user= $user->histories()->save($history_record);
		     
		     }
		     $trans=Temporary::where('book_id','=',$debit_acc)
		     				//->where('posting_date','=',Carbon::now('EAT'))
		        						->where('transaction_type','=','Credit')
		        						->sum('amount');
		     
		      //Post to the histories table the summarized amounts
		      if($trans>0){
		      	   $history_record=Archive::create([
	        	'book_id'=>$debit_acc,
	           	'transaction_type'=> 'Credit',
	        	'amount'=>$trans,
	        	'posting_date'=>Carbon::now('EAT')
	        	]); 

		      	  $user = User::find(Auth::user()->id);
				$user= $user->histories()->save($history_record);
		      }						


		      //Other account transaction 
		      $credit_acc=$transaction->credit;


		     //Get the record with the debit account
		     $trans=Temporary::where('book_id','=',$credit_acc)
		        						//->where('posting_date','=',Carbon::now('EAT'))
		        						->where('transaction_type','=','Debit')
		        						->sum('amount');
		     //Post to the histories table the summarized amounts   						
		     if($trans>0){
			     $history_record=Archive::create([
	        	'book_id'=>$credit_acc,
	           	'transaction_type'=> 'Debit',
	        	'amount'=>$trans,
	        	'posting_date'=>Carbon::now('EAT')
	        	]); 
			      $user = User::find(Auth::user()->id);
				$user= $user->histories()->save($history_record);
		     
		     }
		     $trans=Temporary::where('book_id','=',$credit_acc)
		     				//->where('posting_date','=',Carbon::now('EAT'))
		        						->where('transaction_type','=','Credit')
		        						->sum('amount');
		     
		      //Post to the histories table the summarized amounts
		      if($trans>0){
		      	   $history_record=Archive::create([
	        	'book_id'=>$credit_acc,
	           	'transaction_type'=> 'Credit',
	        	'amount'=>$trans,
	        	'posting_date'=>Carbon::now('EAT')
	        	]); 
		      	    $user = User::find(Auth::user()->id);
				$user= $user->histories()->save($history_record);
		      }						

			//Delete each of the records in the periodicals table
			$periodical_delete=Paper::find($value->id);
			$periodical_delete->delete();
			}
	DB::commit();

			// $deletion_flag = 1;

			// $values = array($deletion_flag);
			 
			$papers= Paper::all();

			// $journals->setPath('journal');

			// //$journals->load('user','account');
		 
      		 return View('papers.index',compact('papers'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
