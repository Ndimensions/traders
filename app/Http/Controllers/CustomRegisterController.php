<?php

namespace App\Http\Controllers;

use App\Promote;
use Illuminate\Http\Request;
use Session;
use Redirect;

class CustomRegisterController extends Controller
{
    /**
     * Show the registration form
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {
        $source = $request->query('utm_source');
        $type = $request->query('utm_id');

        if ($source || $type) {
            // Save the info (referral)
        }
        
        if(Promote::where('code',$source)->exists()){
            return view('auth.register',compact('source'));
        }else{
            return view('welcome');
        }
        
        
    }
}
