<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Carbon;
use Session;
use Redirect;
use App\Brief;
use App\Expense;
use DB;

class FrIncomeExpenditureController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('cover.income_exp');

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public static function create($ledger_id, $first_date,$second_date)
	{
		
		if(Brief::whereBetween('transaction_date', array($first_date, $second_date))
				        	        		->where('transaction_type', 'Credit') 
				 							->where('description','Income')
				 							->where('ledger_id',$ledger_id)->exists() ){

			$income_account_sum=Brief::whereBetween('transaction_date', array($first_date, $second_date))
				        	        		->where('transaction_type', 'Credit') 
				 							->where('description','Income')
				 							->where('ledger_id',$ledger_id)
				 							->sum('amount');
			return $income_account_sum;

		}else{
			return 0;
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$first_date = $request->input('first_date');
		$second_date = $request->input('second_date');


		if($first_date ==="" && $second_date===""){

				Session::flash('message', 'At Least one Date is required');
		        return Redirect::to('frincomeexpenditure');
		}
		else
		{


				$deletion_flag = 1;

				$values = array($deletion_flag);

					$range_expense=Brief:://whereNotIn('deletion_flag', $values) 
											whereBetween('transaction_date', array($first_date, $second_date))
				        	        		->where('transaction_type', 'Debit') 	
				        	        		->where('description','Expense')
				        	        		->groupBy('ledger_id')			    
		        							->get();

										  		 

		        	$range_income=Brief:://whereNotIn('deletion_flag', $values) 
											whereBetween('transaction_date', array($first_date, $second_date))
				        	        		->where('transaction_type', 'Credit') 
				        	        		->where('description','Income')		
				        	        		->groupBy('ledger_id')		    
		        							->get();


		        	$expenses=$range_expense->sum('amount');
		         
 				 	$incomes=$range_income->sum('amount');	

				
		        //return ('students.search_res',compact('student'));
				if($range_expense->count()<0 && $range_income <0 )
				{
				 Session::flash('message', 'Transactions not entered.');
		        return Redirect::to('expenditures');
				}
				else
				{
				return view('cover.fr_income_exp_res',compact('range_expense','range_income','expenses','incomes','first_date','second_date'));
				
				}
	}
}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public static function show($ledger_id, $first_date,$second_date)
	{
			if(Brief::whereBetween('transaction_date', array($first_date, $second_date))
 				        	        		->where('transaction_type', 'Debit') 
				 							->where('description','Expense')
				 							->where('ledger_id',$ledger_id)->exists() ){

			$expense_account_sum=Brief::whereBetween('transaction_date', array($first_date, $second_date))
 				        	        		->where('transaction_type', 'Debit') 
				 							->where('description','Expense')
				 							->where('ledger_id',$ledger_id)
				 							->sum('amount');
			return $expense_account_sum;

		}else{
			return 0;
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
