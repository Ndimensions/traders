<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Type;
use App\Ledger;
use App\Kind;
use App\Book;
 

class ScriptController extends Controller {

	//sentence case for all types
	public static function getTypes(){

		$types=Book::get();

		foreach ($types as $key => $value) {
			$type=Book::find($value->id);
			$name=$value->name;
			$description=$value->description; 

			$type_update=array(
			'name'=> ucwords(strtolower($name)),
			'description'=> ucwords(strtolower($description))
			);
			$type->update($type_update);
		}
		dd('complete');
	}//end of the types function

}
