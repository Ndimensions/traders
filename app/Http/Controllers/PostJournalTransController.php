<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Voucher;
use App\Invest;
use App\Interest;
use App\Financial;
use App\Sole;
use App\Brief;
use Carbon;
use Auth;
use DB;

class PostJournalTransController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		 
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
	 
		
		$deletion_flag = 1;
		$values = array($deletion_flag);
		$transactions_array=array();
		$not_posted_flag=0;
		$np_values=array($not_posted_flag);

		//Declare an array to hold the ledgers that are being posted
		$ledger_array=array();

		$journals=Invest::all();

		$financial_id=Financial::CurrentFinancial()->first()->id;

		DB::beginTransaction();
	 
		foreach ($journals as $key => $value)
			{
				   
				$voucher_id=Voucher::where('transaction_id','=',$value->transaction_id)->pluck('id');
				 
				$voucher=Voucher::find($voucher_id[0]);    
				$posted_update=array(
		        	'posted_flag'=>'1'
		        	);
		        $voucher->update($posted_update);

		        //Store the user records in the soles table for querying
		        //Create the record in the briefs table for the other accounts entry
		       	// $soles=Sole::create([
		        //     'transaction_id'=>$value->transaction_id,
		        //     'posting_date'=>Carbon::now('EAT'),
		        //     'transaction_type'=>'Debit',
		        //     'transaction_date'=>$value->transaction->voucher->transaction_date,
 		        //     'amount'=>$value->transaction->voucher->amount,
 		        //     'user_id'=>$value->transaction->voucher->user_id,
 		        //     'financial_id'=>$value->transaction->voucher->financial_id
            	// ]);



		        $trans=Voucher::find($value->transaction->voucher->id);

		        $debit_acc=$trans->debit;

		        //Get the record with the debit account
		        $vouch_debit=Voucher::where('debit','=',$debit_acc)
		        						->where('transaction_id',$value->transaction_id)
		        						->first();


		       	//Create the record in the briefs table for the other accounts entry
		       	$banks=Brief::create([
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Debit',
		            'ledger_id'=>$debit_acc,
		            'amount'=>$vouch_debit->amount,
		            'category'=>$vouch_debit->category,
		            'particulars'=>$vouch_debit->particulars,
		            'description'=>$vouch_debit->transaction_type
            	]);


            	$credit_acc=$trans->credit;

		        //Get the record with the credit account
		        $voucher_credit=Voucher::where('credit','=',$credit_acc)
		        						->where('transaction_id',$value->transaction_id)
		        						->first();


		       	//Create the record in the briefs table for the credit bank and cash
		       	$briefs=Brief::create([ 
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Credit',
		            'ledger_id'=>$credit_acc,
		            'amount'=>$voucher_credit->amount,
		            'category'=>$voucher_credit->category,
		            'particulars'=>$voucher_credit->particulars,
		            'description'=>$voucher_credit->transaction_type
            	]);
		  			
 }
	 	DB::commit();

	 	DB::beginTransaction();
 			
 			//Get the transactions in the briefs table and do the totals
			$periodicals=Invest::all();

			foreach ($periodicals as $key => $value) {

			//Delete each of the records in the periodicals table
			$periodical_delete=Invest::find($value->id);
			$periodical_delete->delete();
			}
		DB::commit();

			$journals = Invest::all();
      		return View('periodicals.income_index',compact('journals'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		 
		$deletion_flag = 1;
		$values = array($deletion_flag);
		$transactions_array=array();
		$not_posted_flag=0;
		$np_values=array($not_posted_flag);

		//Declare an array to hold the ledgers that are being posted
		$ledger_array=array();

		$journals=Invest::where('id',$id)->get();

		$financial_id=Financial::CurrentFinancial()->first()->id;


		DB::beginTransaction();
	 
		foreach ($journals as $key => $value)
			{
				//Update the interests table
				$invest_id=Interest::where('transaction_id',$value->transaction_id)->first()->id;
				$invest=Interest::find($invest_id);
				$interest=$invest->interest;
				 
				$invest_update=array(
					'mature_flag'=>1
				);
				$invest->update($invest_update);

				$voucher_id=Voucher::where('transaction_id','=',$value->transaction_id)->pluck('id');
				 
				$voucher=Voucher::find($voucher_id[0]);    
				$posted_update=array(
		        	'posted_flag'=>'1'
		        	);
		        $voucher->update($posted_update);

		        //Store the user records in the soles table for querying
		        //Create the record in the briefs table for the other accounts entry
		       	// $soles=Sole::create([
		        //     'transaction_id'=>$value->transaction_id,
		        //     'posting_date'=>Carbon::now('EAT'),
		        //     'transaction_type'=>'Debit',
		        //     'transaction_date'=>$value->transaction->voucher->transaction_date,
 		        //     'amount'=>$value->transaction->voucher->amount,
 		        //     'user_id'=>$value->transaction->voucher->user_id,
 		        //     'financial_id'=>$value->transaction->voucher->financial_id
            	// ]);



		        $trans=Voucher::find($value->transaction->voucher->id);

		        $debit_acc=$trans->debit;

		        //Get the record with the debit account
		        $vouch_debit=Voucher::where('debit','=',$debit_acc)
		        						->where('transaction_id',$value->transaction_id)
		        						->first();


		       	//Create the record in the briefs table for the other accounts entry
		       	$banks=Brief::create([
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Debit',
		            'ledger_id'=>$debit_acc,
		            'amount'=>$vouch_debit->amount,
		            'category'=>$vouch_debit->category,
		            'particulars'=>$vouch_debit->particulars,
		            'description'=>$vouch_debit->transaction_type
            	]);

				//Debit the interest expense account
				//Create the record in the briefs table for the other accounts entry
				$banks=Brief::create([
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Debit',
		            'ledger_id'=>9,
		            'amount'=>$interest,
		            'category'=>$vouch_debit->category,
		            'particulars'=>'Interest payables',
		            'description'=>$vouch_debit->transaction_type
            	]);

            	$credit_acc=$trans->credit;

		        //Get the record with the credit account
		        $voucher_credit=Voucher::where('credit','=',$credit_acc)
		        						->where('transaction_id',$value->transaction_id)
		        						->first();


		       	//Create the record in the briefs table for the credit bank and cash
		       	$briefs=Brief::create([ 
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Credit',
		            'ledger_id'=>$credit_acc,
		            'amount'=>$voucher_credit->amount,
		            'category'=>$voucher_credit->category,
		            'particulars'=>$voucher_credit->particulars,
		            'description'=>$voucher_credit->transaction_type
				]);
				
				//Credit the interest payables
				$briefs=Brief::create([ 
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Credit',
		            'ledger_id'=>4,
		            'amount'=>$interest,
		            'category'=>$voucher_credit->category,
		            'particulars'=>'Interest expense',
		            'description'=>$voucher_credit->transaction_type
            	]);
		  			
 }
	 	DB::commit();

	 	DB::beginTransaction();
 			
 			//Get the transactions in the briefs table and do the totals
			$periodicals=Invest::where('id',$id)->get();

			foreach ($periodicals as $key => $value) {

			//Delete each of the records in the periodicals table
			$periodical_delete=Invest::find($value->id);
			$periodical_delete->delete();
			}
		DB::commit();

			 
			return redirect('invest');
      		 
		}
	

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
