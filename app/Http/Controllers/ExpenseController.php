<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Expense;
use App\Division;
use App\Account;
use App\Invoice;
use App\Journal;
use App\Ledger;
use App\Entry;
use App\Periodical;
use App\Bank;
use App\Receipt;
use App\Voucher;
use App\Type;
use Carbon;
use Auth;
use DB;

class ExpenseController extends Controller {

	   public function __construct()
    {
        $this->middleware('auth');
        
    }


protected  $rules = ['invoice_ref_number'=>['required'],
					 'amount' =>	 ['required','numeric'],
					 'description' => ['required'],
					 'account_id'=>['required'],
                     'cheque_number'=>['numeric']
					  						];

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		 
       return View('expenses.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('expenses.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
		$this->validate($request, $this->rules);

DB::beginTransaction();

        $ledger_id=$request->input('account_id');

        $values = array($ledger_id);

        $account_id= Account::wherein('ledger_id',$values)->pluck('id');

        $account_type=Account::TypeId($account_id)->pluck('type_id');

        $values = array($ledger_id);

        //Check if the account exists
        $check_account=Account::wherein('ledger_id',$values)->pluck('id');

        $invoice_ref_number=$request->input('invoice_ref_number');


        $invoice_value=array($invoice_ref_number);

        $check_invoice=Invoice::wherein('ref_number',$invoice_value)->pluck('id');

        if(!$check_invoice){

        	Session::flash('message', ' The Invoice number does not exist/Please search the invoice to confirm');
        	return view ('expenses.create');

        }

        if(!$check_account)
        {
        	Session::flash('message', ' The Account ID does not exist/Please search account to confirm');
        	return view ('expenses.create');

        }

        $votehead_id=Account::TypeId($account_id)->pluck('type_id');

        $votehead_name=Type::where('id',$votehead_id)->pluck('name');

        $account_name=Ledger::where('id',$ledger_id)->pluck('name');


        //Get the particular invoice id
        $invoice_id=Invoice::where('ref_number','=',$invoice_ref_number)->pluck('id');

        //Amount paid under the invoice
        $invoice_paid=Expense::where('invoice_id','=',$invoice_id)->sum('amount');

        //Get the instance of the particular invoice
       	$invoice=Invoice::find($invoice_id);
       	$invoice_balance=$invoice->balance;
  
       
       //New values for the invoice both the new balance and new amounts paid
        $invoice_new_balance=$invoice_balance-$request->input('amount');
        $new_paid=$invoice_paid+$request->input('amount');
 


        if($request->input('amount')>$invoice_balance){
        	Session::flash('message', ' Overpayment of invoice amount, transaction rejected.');
        	return view ('expenses.create');
        }

        $last=Expense::orderBy('created_at','desc')->first();

        //Process the receipting
        //create the receipt funciton        

        $receipt=Receipt::where('id','=','1')->pluck('payments');

        $receipt_id=$receipt+1;
        
          $receipt_update=array(
            'payments'=>$receipt_id
            );
        
        $receipts=Receipt::find(1);
          
        $receipts->update($receipt_update);

        $receipt_id=$receipt_id;
	 

        //Check if the transaction is has a bank selected 
        if($request->has('bank') && !$request->has('cheque_number')){
            Session::flash('message', 'Please, enter the cheque number');
            return view ('expenses.create');
        }
        elseif ($request->has('bank') && $request->has('cheque_number')) {

                  $account=Account::find($account_id);
                    $ledger_ids=$account->ledger_id;
          
                    $journal_update=Periodical::create([
                    'transaction_no'=>$receipt_id,
                    'transaction_type'=>'CHEQUE PAYMENT',
                    'transaction_remarks'=>'INVOICE PAYMENT',
                    'transaction_date'=> Carbon::now('EAT'),
                    'particulars'=>$request->input('description'),
                    'debit'=>$ledger_ids,
                    'credit'=>'8',
                    'amount'=>$request->input('amount')
                    ]);

                    //Get the id of the inserted journal entry
                    $periodical_id = $journal_update->id;

                    //Get the id of the inserted journal entry
                    $periodical_id = $journal_update->id;

                     $account=Account::find($account_id);
                    $ledger_ids=$account->ledger_id;

                        $ledger=Ledger::where('id','=',$ledger_ids)->first();
                        $account_current_balance=$ledger->current_balance;
                    $current_balance=$account_current_balance+$request->input('amount');

                        $del_flag='0';
                        $cheque_deletion_flag = array($del_flag);
                        $cheque_number=$request->input('cheque_number');
                        $bank=$request->input('bank');

                        //Ensure that the cheque number is not already entered
                        $cheque_id=Bank::where('cheque_number',$cheque_number)
                                            ->where('bank',$bank)
                                            ->wherein('periodical_deletion_flag',$cheque_deletion_flag)
                                            ->pluck('cheque_number');
                        

                        if($cheque_id ===$cheque_number){
                             Session::flash('message', 'The cheque number is already entered.'.''.$bank.'-&nbsp'.$cheque_number);
                          return view ('expenses.create');

                        }

                     $bank = new Bank(array(
                    'credit'=>$request->input('amount'),
                    'amount'=>$request->input('amount'),
                    'transaction_no'=>$receipt_id,
                    'transaction_date'=> Carbon::now('EAT'),
                    'description'=>$request->input('description'),
                    'cheque_number'=>$request->input('cheque_number'),
                    'comments'=>$request->input('comments'),
                    'user_id'=>Auth::user()->id,
                    'type_id'=>$account_type,
                    'periodical_id'=>$periodical_id,
                    'posted_flag'=>'0',
                    'current_balance'=>$current_balance
                    ));


                    $account = Account::find($account_id);

                    $account = $account->banks()->save($bank);

                     //Get the id of the inserted bank entry
                    $bank_id = $bank->id;

                        $expense = new Expense(array(
                                'amount'=>$request->input('amount'),
                                'transaction_no'=>$receipt_id,
                                'transaction_date'=> Carbon::now('EAT'),
                                'description'=>$request->input('description'),
                                'comments'=>$request->input('comments'),
                                'user_id'=>Auth::user()->id,
                                'type_id'=>$account_type,
                                'invoice_id'=>$invoice_id,
                                'bank_id'=>$bank_id,
                                'posted_flag'=>'0',
                                'bank_id'=>$bank_id
                                ));


                        $account = Account::find($account_id);

                        $account = $account->expenses()->save($expense);

                        //Increase the balance of the account in the ledger
                        $ledger_id=$ledger->id;
                        $ledger=Ledger::find($ledger_id);           
                        $ledger_update=array(
                            'current_balance'=>$current_balance
                            );

                        $ledger->update($ledger_update);

                        //Reduce the balance in the bank account
                        $ledger_id=Ledger::where('id','=','8')->pluck('id');
                        $ledger=Ledger::find($ledger_id);
                        $current_balance=$ledger->current_balance;
                        $new_balance=$current_balance-$request->input('amount');

                        $ledger_update=array(
                            'current_balance'=>$new_balance
                            );

                        $ledger->update($ledger_update);

                        //For the printing details
                        $bank_name=$request->input('bank');
                        $cheque_number=$request->input('cheque_number');

                        $trans="Cheque:&emsp;".$cheque_number.'&emsp;'.$bank_name;

        }else{


        $account=Account::find($account_id);
        $ledger_ids=$account->ledger_id;  
              
        //Creating the corresponding journal entry
        $journal_update=Periodical::create([
            'transaction_no'=>$receipt_id,
            'transaction_type'=>'CASH VOUCHER',
            'transaction_remarks'=>'INVOICE PAYMENT',
            'transaction_date'=> Carbon::now('EAT'),
            'particulars'=>$request->input('description'),
            'debit'=>$ledger_ids,
            'credit'=>'1',
            'amount'=>$request->input('amount')
            ]);

            //Get the id of the inserted journal entry
            $periodical_id = $journal_update->id;
                $account=Account::find($account_id);
                $ledger_ids=$account->ledger_id;

                $ledger=Ledger::where('id','=',$ledger_ids)->first();
                $account_current_balance=$ledger->current_balance;

            $current_balance=$account_current_balance+$request->input('amount');
     
                $voucher = new Voucher(array(
                 'credit'=>$request->input('amount'),
                'amount'=>$request->input('amount'),
                'transaction_no'=>$receipt_id,
                'transaction_date'=> Carbon::now('EAT'),
                'description'=>$request->input('description'),
                'comments'=>$request->input('comments'),
                'user_id'=>Auth::user()->id,
                'type_id'=>$account_type,
                'periodical_id'=>$periodical_id,
                'posted_flag'=>'0',
                'current_balance'=>$current_balance
                ));


                $account = Account::find($account_id);

                $account = $account->expenses()->save($voucher);

                //Get the id of the inserted voucher entry
                 $voucher_id = $voucher->id;

                $expense = new Expense(array(
                'amount'=>$request->input('amount'),
                'transaction_no'=>$receipt_id,
                'transaction_date'=> Carbon::now('EAT'),
                'description'=>$request->input('description'),
                'comments'=>$request->input('comments'),
                'user_id'=>Auth::user()->id,
                'type_id'=>$account_type,
                'invoice_id'=>$invoice_id,
                'voucher_id'=>$voucher_id,
                'posted_flag'=>'0',
                'voucher_id'=>$voucher_id
                ));


                $account = Account::find($account_id);

                $account = $account->expenses()->save($expense);

                 //Increase the balance of the account in the ledger
                $ledger_id=$ledger->id;
                $ledger=Ledger::find($ledger_id);           
                $ledger_update=array(
                    'current_balance'=>$current_balance
                    );
                $ledger->update($ledger_update);

                //Reduce the balance in the cash account
                $ledger_id=Ledger::where('id','=','1')->pluck('id');
                $ledger=Ledger::find($ledger_id);
                $current_balance=$ledger->current_balance;
                $new_balance=$current_balance-$request->input('amount');

                $ledger_update=array(
                    'current_balance'=>$new_balance
                    );
                $ledger->update($ledger_update);

                $trans="Cash";

        }
    
		//Update the Invoice Details
		$invoice_update=array(
			'balance' => $invoice_new_balance,
			'paid' => $new_paid,
			 
		);
        $invoice->update($invoice_update);

        $votehead=$votehead_name;

        //Enter the values to the respective accounts in the entries table that rep the ledger book
 DB::commit();
		if($account)
		{
		
      
       	Session::flash('message', 'Successfully added expense');

         $print='expense';

        return view ('expenses.print_expenses',compact('trans','print','expense','votehead','account_name','receipt_id'));

		}
		
         
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
			// get the School
        $expense = Expense::find($id);

         return View('expenses.edit',compact('expense'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{
		 $this->validate($request, $this->rules);

        //dd($request->all());
        $expenses =Expense::find($id);
        $expenses->update($request->all());

        if($expenses)
        {

       	Session::flash('message', 'Successfully Updated Expense');
        return Redirect::to('expenses');
        }

        Session::flash('message','Failed to Update record');
         return Redirect:: to('expenses');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	
		$expense = Expense::find($id);

		$deletion_flag=array(

					'deletion_flag'=>'1',
					'deleted_by'=>Auth::user()->id
				
				);

		$expense->update($deletion_flag);
        //$expense->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the Expense Transaction!');
        return Redirect::to('expenses');
	}

}


