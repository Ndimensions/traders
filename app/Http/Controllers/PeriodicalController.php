<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
 
use Session;
use Redirect;
use App\Expense;
use App\Account;
use App\Invoice;
use App\Journal;
use App\Ledger;
use App\Entry;
use App\Periodical;
use App\Voucher;
use App\Bank;
use Carbon;
use Auth;
use DB;

class PeriodicalController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$deletion_flag = '0';

		$np_posted='0';
		$np_values=array($np_posted);

		$values = array($deletion_flag);
		 
		$journals = Periodical::get();
	 
         return View('periodicals.index',compact('journals'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 //Update the deletion flag  in the periodicals table
	    $journal_id=$id;
	    $transaction_id=Periodical::find($journal_id)->transaction_id;
	    $voucher_id=Voucher::where('transaction_id',$transaction_id)->first()->id;
	    $journal=Voucher::find($voucher_id);
DB::beginTransaction();
	    $transaction_type=$journal->transaction_type;
	    $transaction_no=$journal->transaction_no;
	    $transaction_remarks=$journal->transaction_remarks;
	    
		$deletion_update=array(
        	'deletion_flag'=>'1',
        	'deletion_date'=> Carbon::now('EAT'),
        	'deleted_by'=>Auth::user()->first_name.Auth::user()->last_name
        	);

        $journal->update($deletion_update);

		 
 DB::commit();	


 
 	   $periodical=Periodical::find($id);
 	   $periodical->delete();

		
        $deletion_flag = '0';

		$np_posted='0';
		$np_values=array($np_posted);

		$values = array($deletion_flag);
		 
		$journals = Periodical::get();
	 

     		 	Session::flash('message', ' Successfully reversed transaction.');

        return View('periodicals.index',compact('journals'));


		 
 

	}

}
