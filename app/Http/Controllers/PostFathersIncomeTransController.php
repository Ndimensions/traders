<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Expense;
use App\Account;
use App\Invoice;
use App\Journal;
use App\Ledger;
use App\Tier;
use App\Brief;
use App\Entry;
use App\Financial;
use App\Sole;
use App\Periodical;
use App\Bank;
use App\Voucher;
use App\Income;
use App\History;
use App\Post;
use App\User;
use App\Record;
use Carbon;
use Auth;
use DB;

class PostFathersIncomeTransController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		 
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		 
 

		$journals=Income::all();

		$financial_id=Financial::CurrentFinancial()->first()->id;


	DB::beginTransaction();
	 
		foreach ($journals as $key => $value)
			{
				   
				$voucher=Voucher::where('transaction_id','=',$value->transaction_id)
									->first();
			 	$voucher_id=$voucher->id;
				$voucher=Voucher::find($voucher_id);    
				$posted_update=array(
		        	'posted_flag'=>'1'
		        	);
		      $voucher->update($posted_update);



		        //Store the user records in the soles table for querying
		        //Create the record in the briefs table for the other accounts entry
		       	// $soles=Sole::create([
		        //     'transaction_id'=>$value->transaction_id,
		        //     'posting_date'=>Carbon::now('EAT'),
		        //     'transaction_type'=>'Debit',
		        //     'transaction_date'=>$value->transaction->voucher->transaction_date,
 		        //     'amount'=>$value->transaction->voucher->amount,
 		        //     'user_id'=>$value->transaction->voucher->user_id,
				// 	 'financial_id'=>$value->transaction->voucher->financial_id,
				// 	 'debit'
            	// ]);



		        $trans=Voucher::find($value->transaction->voucher->id);

		        $debit_acc=$trans->debit;

		        //Get the record with the debit account
		        $vouch_debit=Voucher::where('debit','=',$debit_acc)
		        						->where('transaction_id',$value->transaction_id)
		        						->first();


		       	//Create the record in the briefs table for the other accounts entry
		       	$banks=Brief::create([
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Debit',
		            'ledger_id'=>$debit_acc,
		            'amount'=>$vouch_debit->amount,
		            'category'=>$vouch_debit->category,
		            'particulars'=>$vouch_debit->particulars,
		            'description'=>$vouch_debit->transaction_type
            	]);


            	$credit_acc=$trans->credit;
		        //Get the record with the credit account
		        $voucher_credit=Voucher::where('credit','=',$credit_acc)
		        						->where('transaction_id',$value->transaction_id)
		        						->first();


		       	//Create the record in the briefs table for the credit bank and cash
		       	$briefs=Brief::create([ 
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Credit',
		            'ledger_id'=>$credit_acc,
		            'amount'=>$voucher_credit->amount,
		            'category'=>$voucher_credit->category,
		            'particulars'=>$voucher_credit->particulars,
		            'description'=>$voucher_credit->transaction_type
            	]);
		  			
 }
	 DB::commit();

	 DB::beginTransaction();
 			
 			//Get the transactions in the briefs table and do the totals
			$periodicals=Income::all();
			foreach ($journals as $key => $value) {
			//Delete each of the records in the periodicals table
			$periodical_delete=Income::find($value->id);
			$periodical_delete->delete();
			}
	DB::commit();

			$journals = Income::all();
      		return View('periodicals.income_index',compact('journals'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
