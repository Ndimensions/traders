<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Fee;
use App\Secretary;
use App\Item;
use App\Note;
use App\User;
use App\ledger;
use App\Term;
use Session;
use Redirect;
use Carbon;
use Auth;
use DB;

class PostingIncomesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	 
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{ 
		$date = $request->input('date');		
		 
		$transaction_type = $request->input('transaction_type');

DB::beginTransaction();

		if($date ==="" && $transaction_type ==="")
			{
				
			Session::flash('message', 'At Least one Field is required');
	        return Redirect::to('post/incomes');
			
			}
			
		if($transaction_type==="")
			{
					Session::flash('message', 'PLease select the transaction type.');
	        return Redirect::to('post/incomes');
			}


			$term_id=Term::CurrentTerm()->pluck('id');

       	 	//Get all the users of the system
			$system_users=User::all();
			//This array holds the list of all user_names with transactions in the fee table
			$username_array=array();
			//For each user test if they have any financial transaction, if so add their names to an array
			foreach ($system_users as $key => $value) {
				$user_id=$value->id;
				$values=array($user_id);
				$user_fee=Fee::wherein('user_id',$values)->get();
				if($user_fee->count()>0)
				{
					array_push($username_array, $value->id);
				}
			}

			$policy_array=array('Tuition','Boarding','Medical','Transport','KCPE','Interview','Reportbook',
				'Scouts','PEKitsTracksuit','Show','Tour','Admission','Gown','Registration','AllianceConference','Hair','Conference1');

switch ($transaction_type) {
	case "RECEIPT":

		 
		foreach ($username_array as $key => $value) {


			$tuition_array=array();

			$boarding_array=array();

			$medical_array=array();

			$transport_array=array();

			$KCPE_array=array();

			$interview_array=array();

			$reportbook_array=array();

			$scouts_array=array();

			$PEKitsTracksuit_array=array();

			$show_array=array();

			$tour_array=array();

			$admission_array=array();

			$gown_array=array();

			$registration_array=array();

			$allianceconference_array=array();

			$hair_array=array();

			$conference1_array=array();

			//Do not get those already posted
			$post_flag = 1;
			$post_value = array($post_flag);
			


			//Get a user transactions of the specific type and date 
			$fee_trans=Fee::where('transaction_date','=',$date)
		 							->where('transaction_type', '=',$transaction_type)
		 							->where('user_id','=',$value)
		 							->whereNotIn('post_flag', $post_value)
		 							->get();

		 	//Update the post flag in the fees table
		 	foreach ($fee_trans as $key => $fee) {
		 		$fee=Fee::find($fee->id);

		 		$fee_update=array(
					'post_flag' =>'1',			 
				);

				$fee->update($fee_update);
		 	}

		 	//Get the items from the items table	 	
			 foreach ($fee_trans as $key => $fee_tran) {

			 	$item_trans=Item::where('fee_id','=',$fee_tran->id)		 							 
		 							->get();

		 		
			 		foreach ($item_trans as $key => $item_tran) {

 			 			switch ($item_tran->ledger->name) {
						     case "Tuition":
						          array_push($tuition_array, $item_tran->credit);
						         break;
						     case "Boarding":
						          array_push($boarding_array, $item_tran->credit);
						         break;
						     case "Medical":
						          array_push($medical_array, $item_tran->credit);
						         break;
						     case "Transport":
						          array_push($transport_array, $item_tran->credit);
						         break;
						     case "KCPE":
						          array_push($KCPE_array, $item_tran->credit);
						         break;
						     case "Interview":
						          array_push($interview_array, $item_tran->credit);
						         break;
						     case "PEKitsTracksuit":
						          array_push($PEKitsTracksuit_array, $item_tran->credit);
						         break;
						     case "Reportbook":
						          array_push($reportbook_array, $item_tran->credit);
						         break;
						     case "Scouts":
						          array_push($scouts_array, $item_tran->credit);
						         break;
						     case "Admission":
						          array_push($admission_array, $item_tran->credit);
						         break;
						     case "Show":
						          array_push($show_array, $item_tran->credit);
						         break;
						     case "Tour":
						          array_push($tour_array, $item_tran->credit);
						         break;
						     case "Gown":
						          array_push($gown_array, $item_tran->credit);
						         break;
						     case "Registration":
						          array_push($registration_array, $item_tran->credit);
						         break;
						     case "AllianceConference":
						          array_push($allianceconference_array, $item_tran->credit);
						         break;
						    case "Hair":
						          array_push($hair_array, $item_tran->credit);
						         break;
						    case "Conference1":
						          array_push($conference1_array, $item_tran->credit);
						         break;
						     
						}//end of swicth statement

			 		}//end of items of fees loop				 


			 }//end of user fees array



			 	 	$Tuition=array_sum($tuition_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Tuition')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Tuition,
	        		 'credit'=>'0',
	        		 'amount'=>$Tuition*-1,       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Boarding=array_sum($boarding_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Boarding')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Boarding,
	        		 'credit'=>'0',
	        		  'amount'=>$Boarding*-1,        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Medical=array_sum($medical_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Medical')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Medical,
	        		 'credit'=>'0',
	        		  'amount'=>$Medical*-1,        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Transport=array_sum($transport_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Transport')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Transport,
	        		 'credit'=>'0',
	        		  'amount'=>$Transport*-1,        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);


					$KCPE=array_sum($KCPE_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','KCPE')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$KCPE,
	        		 'credit'=>'0',
	        		  'amount'=>$KCPE*-1,        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Interview=array_sum($interview_array);
					$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Interview')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Interview,
	        		 'credit'=>'0',
	        		 'amount'=>$Interview*-1,        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$PEKitsTracksuit=array_sum($PEKitsTracksuit_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','PEKitsTracksuit')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$PEKitsTracksuit,
	        		 'credit'=>'0',
	        		  'amount'=>$PEKitsTracksuit*-1,      		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Reportbook=array_sum($reportbook_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Reportbook')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Reportbook,
	        		 'credit'=>'0',
	        		  	 'amount'=>$Reportbook*-1,       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Scouts=array_sum($scouts_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Scouts')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Scouts,
	        		 'credit'=>'0',
	        		 'amount'=>$Scouts*-1,        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Admission=array_sum($admission_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Admission')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Admission,
	        		 'credit'=>'0',
	        		'amount'=>$Admission*-1,        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Show=array_sum($show_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Show')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Show,
	        		 'credit'=>'0',
	        		  'amount'=>$Show*-1,       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Tour=array_sum($tour_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Tour')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Tour,
	        		 'credit'=>'0',
	        		  'amount'=>$Tour*-1,       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Gown=array_sum($gown_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Gown')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Gown,
	        		 'credit'=>'0',
	        		 'amount'=>$Gown*-1,       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Registration=array_sum($registration_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Registration')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Registration,
	        		 'credit'=>'0',
	        		  'amount'=>$Registration*-1,        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$AllianceConference=array_sum($allianceconference_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','allianceConference')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$AllianceConference,
	        		 'credit'=>'0',
	        		'amount'=>$AllianceConference*-1,
        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Hair=array_sum($hair_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Hair')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Hair,
	        		 'credit'=>'0',
	        		 'amount'=>$Hair*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Conference1=array_sum($conference1_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Conference1')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Conference1,
	        		 'credit'=>'0',
	        		 'amount'=>$Conference1*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);
		 	 
		 } //end of user array loop

		 
		foreach ($policy_array as $key => $value) {

			$ledger_id=Ledger::where('name',$value)->pluck('id');

			 $post_total=Secretary::where('transaction_date',$date)
			 						->where('ledger_id','=',$ledger_id)
			 						->where('post_flag','0')
			 						->sum('debit');


			$balance=Ledger::where('name',$value)->pluck('balance')+($post_total*-1);

			 $note = new Note(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>$ledger_id,
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>($post_total*-1),
	        		 'credit'=>'0',
	        		 'amount'=>($post_total*-1),
	        		 'particulars'=> 'Student Incomes',    
	        		 'term_id'=>$term_id ,  		
	        		 'balance'=> $balance       		
	        		));


			$user = User::find(Auth::user()->id);
			$user= $user->notes()->save($note);

			 $secretary_update=Secretary::where('transaction_date',$date)
			 						->where('ledger_id','=',$ledger_id)
			 						->get();

			 foreach ($secretary_update as $key => $value) {
			 	$sec=Secretary::find($value->id);
			 	$sec_update=array(
					'post_flag' =>'1',			 
				);
				$sec->update($sec_update);
			 }

		
		}

	break;


	case "INVOICE":


		foreach ($username_array as $key => $value) {


			$tuition_array=array();

			$boarding_array=array();

			$medical_array=array();

			$transport_array=array();

			$KCPE_array=array();

			$interview_array=array();

			$reportbook_array=array();

			$scouts_array=array();

			$PEKitsTracksuit_array=array();

			$show_array=array();

			$tour_array=array();

			$admission_array=array();

			$gown_array=array();

			$registration_array=array();

			$allianceconference_array=array();

			$hair_array=array();

			$conference1_array=array();

			//Do not get those already posted
			$post_flag = 1;
			$post_value = array($post_flag);


			$fee_trans=Fee::where('transaction_date','=',$date)
		 							->where('transaction_type', '=',$transaction_type)
		 							->where('user_id','=',$value)
		 							->whereNotIn('post_flag',$post_value)
		 							->get();

		 	//Update the post flag in the fees table
		 	foreach ($fee_trans as $key => $fee) {
		 		$fee=Fee::find($fee->id);

		 		$fee_update=array(
					'post_flag' =>'1',			 
				);

				$fee->update($fee_update);
		 	}

		 	//Get the items from the items table	 	
			 foreach ($fee_trans as $key => $fee_tran) {

			 	$item_trans=Item::where('fee_id','=',$fee_tran->id)		 							 
		 							->get();

		 		
			 		foreach ($item_trans as $key => $item_tran) {

 			 			switch ($item_tran->ledger->name) {
						     case "Tuition":
						          array_push($tuition_array, $item_tran->debit);
						         break;
						     case "Boarding":
						          array_push($boarding_array, $item_tran->debit);
						         break;
						     case "Medical":
						          array_push($medical_array, $item_tran->debit);
						         break;
						     case "Transport":
						          array_push($transport_array, $item_tran->debit);
						         break;
						     case "KCPE":
						          array_push($KCPE_array, $item_tran->debit);
						         break;
						     case "Interview":
						          array_push($interview_array, $item_tran->debit);
						         break;
						     case "PEKitsTracksuit":
						          array_push($PEKitsTracksuit_array, $item_tran->debit);
						         break;
						     case "Reportbook":
						          array_push($reportbook_array, $item_tran->debit);
						         break;
						     case "Scouts":
						          array_push($scouts_array, $item_tran->debit);
						         break;
						     case "Admission":
						          array_push($admission_array, $item_tran->debit);
						         break;
						     case "Show":
						          array_push($show_array, $item_tran->debit);
						         break;
						     case "Tour":
						          array_push($tour_array, $item_tran->debit);
						         break;
						     case "Gown":
						          array_push($gown_array, $item_tran->debit);
						         break;
						     case "Registration":
						          array_push($registration_array, $item_tran->debit);
						         break;
						     case "AllianceConference":
						          array_push($allianceconference_array, $item_tran->debit);
						         break;
						    case "Hair":
						          array_push($hair_array, $item_tran->debit);
						         break;
						     case "Conference1":
						          array_push($conference1_array, $item_tran->debit);
						         break;
						     
						}//end of swicth statement

			 		}//end of items of fees loop				 


			 }//end of user fees array
			 	 	$Tuition=array_sum($tuition_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Tuition')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Tuition,
	        		 'amount'=>$Tuition        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Boarding=array_sum($boarding_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Boarding')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Boarding ,
	        		  'amount'=>$Boarding        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Medical=array_sum($medical_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Medical')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Medical,
	        		  'amount'=>$Medical        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Transport=array_sum($transport_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Transport')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Transport,
	        		  'amount'=>$Transport   

	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);


					$KCPE=array_sum($KCPE_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','KCPE')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$KCPE,
	        		  'amount'=>$KCPE       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Interview=array_sum($interview_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Interview')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Interview,
	        		 'amount'=>$Interview          		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$PEKitsTracksuit=array_sum($PEKitsTracksuit_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','PEKitsTracksuit')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$PEKitsTracksuit ,
	        		  'amount'=>$PEKitsTracksuit    		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Reportbook=array_sum($reportbook_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Reportbook')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Reportbook,
	        		  'amount'=>$Reportbook          		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Scouts=array_sum($scouts_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Scouts')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Scouts,
	        		 'amount'=>$Scouts
       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Admission=array_sum($admission_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Admission')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Admission,
	        		 'amount'=>$Admission           		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Show=array_sum($show_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Show')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Show,
	        		  'amount'=>$Show        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Tour=array_sum($tour_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Tour')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Tour,
	        		 'amount'=>$Tour           		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Gown=array_sum($gown_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Gown')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Gown,
	        		 'amount'=>$Gown          		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Registration=array_sum($registration_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Registration')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Registration,
	        		 'amount'=>$Registration    		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);


					$AllianceConference=array_sum($allianceconference_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','allianceConference')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$AllianceConference,
	        		'amount'=>$AllianceConference        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Hair=array_sum($hair_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Hair')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Hair,
	        		  'amount'=>$Hair         		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);


					$Conference1=array_sum($conference1_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Conference1')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>$Conference1,
	        		  'amount'=>$Conference1         		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);
		 	 
		 } //end of user array loop

		 
		foreach ($policy_array as $key => $value) {

			$ledger_id=Ledger::where('name',$value)->pluck('id');

			 $post_total=Secretary::where('transaction_date',$date)
			 						->where('ledger_id','=',$ledger_id)
			 						->sum('credit');

			$balance=Ledger::where('name',$value)->pluck('balance')-$post_total;

			 $note = new Note(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>$ledger_id,
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>'0',
	        		 'credit'=>($post_total),
	        		 'amount'=>($post_total),
	        		 'particulars'=> 'Student Invoice',    
	        		 'term_id'=>$term_id ,  		
	        		 'balance'=> $balance       		
	        		));


			$user = User::find(Auth::user()->id);
			$user= $user->notes()->save($note);
		}
		 	 
	break;	
	case "CREDIT NOTE":
	foreach ($username_array as $key => $value) {


			$tuition_array=array();

			$boarding_array=array();

			$medical_array=array();

			$transport_array=array();

			$KCPE_array=array();

			$interview_array=array();

			$reportbook_array=array();

			$scouts_array=array();

			$PEKitsTracksuit_array=array();

			$show_array=array();

			$tour_array=array();

			$admission_array=array();

			$gown_array=array();

			$registration_array=array();

			$allianceconference_array=array();

			$hair_array=array();

			$conference1_array=array();

			//Do not get those already posted
			$post_flag = 1;
			$post_value = array($post_flag);

			$fee_trans=Fee::where('transaction_date','=',$date)
		 							->where('transaction_type', '=',$transaction_type)
		 							->where('user_id','=',$value)
		 							->whereNotIn('post_flag',$post_value)
		 							->get();

		 	//Update the post flag in the fees table
		 	foreach ($fee_trans as $key => $fee) {
		 		$fee=Fee::find($fee->id);

		 		$fee_update=array(
					'post_flag' =>'1',			 
				);

				$fee->update($fee_update);
		 	}

		 	//Get the items from the items table	 	
			 foreach ($fee_trans as $key => $fee_tran) {

			 	$item_trans=Item::where('fee_id','=',$fee_tran->id)		 							 
		 							->get();

		 		
			 		foreach ($item_trans as $key => $item_tran) {

 			 			switch ($item_tran->ledger->name) {
						     case "Tuition":
						          array_push($tuition_array, $item_tran->credit);
						         break;
						     case "Boarding":
						          array_push($boarding_array, $item_tran->credit);
						         break;
						     case "Medical":
						          array_push($medical_array, $item_tran->credit);
						         break;
						     case "Transport":
						          array_push($transport_array, $item_tran->credit);
						         break;
						     case "KCPE":
						          array_push($KCPE_array, $item_tran->credit);
						         break;
						     case "Interview":
						          array_push($interview_array, $item_tran->credit);
						         break;
						     case "PEKitsTracksuit":
						          array_push($PEKitsTracksuit_array, $item_tran->credit);
						         break;
						     case "Reportbook":
						          array_push($reportbook_array, $item_tran->credit);
						         break;
						     case "Scouts":
						          array_push($scouts_array, $item_tran->credit);
						         break;
						     case "Admission":
						          array_push($admission_array, $item_tran->credit);
						         break;
						     case "Show":
						          array_push($show_array, $item_tran->credit);
						         break;
						     case "Tour":
						          array_push($tour_array, $item_tran->credit);
						         break;
						     case "Gown":
						          array_push($gown_array, $item_tran->credit);
						         break;
						     case "Registration":
						          array_push($registration_array, $item_tran->credit);
						         break;
						     case "AllianceConference":
						          array_push($allianceconference_array, $item_tran->credit);
						         break;
						    case "Hair":
						          array_push($hair_array, $item_tran->credit);
						         break;
						  case "Conference1":
						          array_push($conference1_array, $item_tran->credit);
						         break;
						     
						}//end of swicth statement

			 		}//end of items of fees loop				 


			 }//end of user fees array

			 	 	$Tuition=array_sum($tuition_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Tuition')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Tuition,
	        		 'credit'=>'0',
	        		 'amount'=>$Tuition*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Boarding=array_sum($boarding_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Boarding')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Boarding,
	        		 'credit'=>'0',
	        		 'amount'=>$Boarding*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Medical=array_sum($medical_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Medical')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Medical,
	        		 'credit'=>'0' ,
	        		 'amount'=>$Medical*-1      		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Transport=array_sum($transport_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Transport')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Transport,
	        		 'credit'=>'0',
	        		 'amount'=>$Transport*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);


					$KCPE=array_sum($KCPE_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','KCPE')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$KCPE,
	        		 'credit'=>'0',
	        		 'amount'=>$KCPE*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Interview=array_sum($interview_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Interview')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Interview,
	        		 'credit'=>'0',
	        		 'amount'=>$Interview*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$PEKitsTracksuit=array_sum($PEKitsTracksuit_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','PEKitsTracksuit')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$PEKitsTracksuit,
	        		 'credit'=>'0',
	        		 'amount'=>$PEKitsTracksuit*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Reportbook=array_sum($reportbook_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Reportbook')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Reportbook,
	        		 'credit'=>'0',
	        		 'amount'=>$Reportbook*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Scouts=array_sum($scouts_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Scouts')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Scouts,
	        		 'credit'=>'0',
	        		 'amount'=>$Scouts*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Admission=array_sum($admission_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Admission')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Admission,
	        		 'credit'=>'0',
	        		 'amount'=>$Admission*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Show=array_sum($show_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Show')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Show,
	        		 'credit'=>'0',
	        		 'amount'=>$Show*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Tour=array_sum($tour_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Tour')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Tour,
	        		 'credit'=>'0' ,
	        		 'amount'=>$Tour*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Gown=array_sum($gown_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Gown')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Gown,
	        		 'credit'=>'0',
	        		 'amount'=>$Gown*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Registration=array_sum($registration_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Registration')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Registration,
	        		 'credit'=>'0',
	        		 'amount'=>$Registration*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$AllianceConference=array_sum($allianceconference_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','allianceConference')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$AllianceConference,
	        		 'credit'=>'0',
	        		 'amount'=>$AllianceConference*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Hair=array_sum($hair_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Hair')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Hair,
	        		 'credit'=>'0',
	        		 'amount'=>$Hair*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Conference1=array_sum($conference1_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Conference1')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Conference1,
	        		 'credit'=>'0',
	        		 'amount'=>$Conference1*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);
		 	 
		 } //end of user array loop

		 
		foreach ($policy_array as $key => $value) {

			$ledger_id=Ledger::where('name',$value)->pluck('id');

			 $post_total=Secretary::where('transaction_date',$date)
			 						->where('ledger_id','=',$ledger_id)
			 						->sum('debit');

			$balance=Ledger::where('name',$value)->pluck('balance')+$post_total;

			 $note = new Note(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>$ledger_id,
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>($post_total*-1),
	        		 'credit'=>'0',
	        		 'amount'=>($post_total*-1),
	        		 'particulars'=> 'Student Credit Notes',    
	        		 'term_id'=>$term_id ,  		
	        		 'balance'=> $balance       		
	        		));


			$user = User::find(Auth::user()->id);
			$user= $user->notes()->save($note);
		 	 
	 
		 						 
		}

	break;
	case "ADJUSTMENT":
	foreach ($username_array as $key => $value) {


			$tuition_array=array();

			$boarding_array=array();

			$medical_array=array();

			$transport_array=array();

			$KCPE_array=array();

			$interview_array=array();

			$reportbook_array=array();

			$scouts_array=array();

			$PEKitsTracksuit_array=array();

			$show_array=array();

			$tour_array=array();

			$admission_array=array();

			$gown_array=array();

			$registration_array=array();

			$allianceconference_array=array();

			$hair_array=array();

			$conference1_array=array();

			//Do not get the values already posted
			$post_flag = 1;
			$post_value = array($post_flag);



			$fee_trans=Fee::where('transaction_date','=',$date)
		 							->where('transaction_type', '=',$transaction_type)
		 							->where('user_id','=',$value)
		 							->whereNotIn('post_flag',$post_value)
		 							->get();

		 	//Update the post flag in the fees table
		 	foreach ($fee_trans as $key => $fee) {
		 		$fee=Fee::find($fee->id);

		 		$fee_update=array(
					'post_flag' =>'1',			 
				);

				$fee->update($fee_update);
		 	}

		 	//Get the items from the items table	 	
			 foreach ($fee_trans as $key => $fee_tran) {

			 	$item_trans=Item::where('fee_id','=',$fee_tran->id)		 							 
		 							->get();

		 		
			 		foreach ($item_trans as $key => $item_tran) {

 			 			switch ($item_tran->ledger->name) {
						     case "Tuition":
						          array_push($tuition_array, $item_tran->credit);
						         break;
						     case "Boarding":
						          array_push($boarding_array, $item_tran->credit);
						         break;
						     case "Medical":
						          array_push($medical_array, $item_tran->credit);
						         break;
						     case "Transport":
						          array_push($transport_array, $item_tran->credit);
						         break;
						     case "KCPE":
						          array_push($KCPE_array, $item_tran->credit);
						         break;
						     case "Interview":
						          array_push($interview_array, $item_tran->credit);
						         break;
						     case "PEKitsTracksuit":
						          array_push($PEKitsTracksuit_array, $item_tran->credit);
						         break;
						     case "Reportbook":
						          array_push($reportbook_array, $item_tran->credit);
						         break;
						     case "Scouts":
						          array_push($scouts_array, $item_tran->credit);
						         break;
						     case "Admission":
						          array_push($admission_array, $item_tran->credit);
						         break;
						     case "Show":
						          array_push($show_array, $item_tran->credit);
						         break;
						     case "Tour":
						          array_push($tour_array, $item_tran->credit);
						         break;
						     case "Gown":
						          array_push($gown_array, $item_tran->credit);
						         break;
						     case "Registration":
						          array_push($registration_array, $item_tran->credit);
						         break;
						     case "AllianceConference":
						          array_push($allianceconference_array, $item_tran->credit);
						         break;
						    case "Hair":
						          array_push($hair_array, $item_tran->credit);
						         break;
						    case "Conference1":
						          array_push($conference1_array, $item_tran->credit);
						         break;
						     
						}//end of swicth statement

			 		}//end of items of fees loop				 


			 }//end of user fees array

			 	 	$Tuition=array_sum($tuition_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Tuition')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Tuition,
	        		 'credit'=>'0',
	        		 'amount'=>$Tuition*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Boarding=array_sum($boarding_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Boarding')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Boarding,
	        		 'credit'=>'0' ,
	        		 'amount'=>$Boarding*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Medical=array_sum($medical_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Medical')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Medical,
	        		 'credit'=>'0' ,
	        		 'amount'=>$Medical*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Transport=array_sum($transport_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Transport')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Transport,
	        		 'credit'=>'0' ,
	        		 'amount'=>$Transport*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);


					$KCPE=array_sum($KCPE_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','KCPE')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$KCPE,
	        		 'credit'=>'0',
	        		 'amount'=>$KCPE*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Interview=array_sum($interview_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Interview')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Interview,
	        		 'credit'=>'0',
	        		 'amount'=>$Interview*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$PEKitsTracksuit=array_sum($PEKitsTracksuit_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','PEKitsTracksuit')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$PEKitsTracksuit,
	        		 'credit'=>'0',
	        		 'amount'=>$PEKitsTracksuit*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Reportbook=array_sum($reportbook_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Reportbook')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Reportbook,
	        		 'credit'=>'0',
	        		 'amount'=>$Reportbook*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Scouts=array_sum($scouts_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Scouts')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Scouts,
	        		 'credit'=>'0',
	        		 'amount'=>$Scouts*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Admission=array_sum($admission_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Admission')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Admission,
	        		 'credit'=>'0',
	        		 'amount'=>$Admission*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Show=array_sum($show_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Show')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Show,
	        		 'credit'=>'0',
	        		 'amount'=>$Show*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Tour=array_sum($tour_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Tour')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Tour,
	        		 'credit'=>'0',
	        		 'amount'=>$Tour*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Gown=array_sum($gown_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Gown')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Gown,
	        		 'credit'=>'0',
	        		 'amount'=>$Gown*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Registration=array_sum($registration_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Registration')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Registration,
	        		 'credit'=>'0',
	        		 'amount'=>$Registration*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$AllianceConference=array_sum($allianceconference_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','allianceConference')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$AllianceConference,
	        		 'credit'=>'0',
	        		 'amount'=>$AllianceConference*-1        		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Hair=array_sum($hair_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Hair')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Hair,
	        		 'credit'=>'0' ,
	        		 'amount'=>$Hair*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);

					$Conference1=array_sum($conference1_array);
				 	$secretary = new Secretary(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>ledger::where('name','Conference1')->pluck('id'),
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>$Conference1,
	        		 'credit'=>'0' ,
	        		 'amount'=>$Conference1*-1       		
	        		 //'balance'=> $balance       		
	        		));
					$user = User::find($value);
					$user= $user->secretaries()->save($secretary);
		 	 
		 } //end of user array loop

		 
		foreach ($policy_array as $key => $value) {

			$ledger_id=Ledger::where('name',$value)->pluck('id');

			 $post_total=Secretary::where('transaction_date',$date)
			 						->where('ledger_id','=',$ledger_id)
			 						->sum('debit');

			$balance=Ledger::where('name',$value)->pluck('balance')+$post_total;

			 $note = new Note(array(
	        		 'posting_date'=> Carbon::now('EAT'),
	        		 'transaction_date'=>$date,
	        		 'ledger_id'=>$ledger_id,
	        		 'transaction_type'=>$transaction_type,	        		
	        		 'debit'=>($post_total*-1),
	        		 'credit'=>'0',
	        		 'amount'=>($post_total*-1),
	        		 'particulars'=> 'Student Adjustments',    
	        		 'term_id'=>$term_id ,  		
	        		 'balance'=> $balance       		
	        		));


			$user = User::find(Auth::user()->id);
			$user= $user->notes()->save($note);
		 	 
	 		 						 
		}
	break;				     
	}

	 DB::commit();		 	
		 

		Session::flash('message', 'Successful posting of transactions');
		return Redirect::to('post/incomes');
 
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
