<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Type;
use App\Account;
use Auth;
use Carbon;
use Session;
use Redirect;

class TypController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('role');
	}
protected  $rules = ['name' => ['required']];
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('account_types.search_type');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, $this->rules);

		$account_name=$request->input('name');

		
		$types=Type::where('name', 'LIKE', '%'.$account_name.'%')
						 
        				->orderBy('created_at', 'desc')->simplePaginate(10);

       if($types->count()>0)
		{
		
		$types->setPath('types');
		 return view('account_types.index',compact('types'));
		}
		else
		{
		Session::flash('message', 'Results Not Available');
		 
        return view ('account_types.search_type');
		}

		}


	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
