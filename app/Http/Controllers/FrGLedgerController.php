<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Carbon;
use Session;
use Redirect;
use App\Provisional;
use App\Brief;
use App\Ledger;
use App\History;
use App\Book;
use App\Archive;
use App\Sheet;
use App\Chronicle;
use App\Register;
use App\Occasion;

class FrGLedgerController extends Controller {

	//Request to get the general ledger of the fathers cash book
	public function getFathers(){

		return view('gledger.fr_search');
	}

	//Receive the post of the father's request
	public function postFathers(Request $request){

		$deletion_flag; 	//A deletion flag of the ledgers table
		$values;			//An array for the eloquent deletion
		$accounts;			//Holds all the accounts of the cash book
		$debit_trans;		//A Particular account debit transaction
		$credit_trans;		//A particular account credit transactions
		$first_date;		//Input first date
		$second_date;       //Input second date
		$debit_trans_array=array();		//An array that holds the list of debit trans of all accounts
		$credit_trans_array=array();		//An array that holds the list of credit transactions of all accounts

		//Get the user input
		$first_date=$request->input('first_date');
		$second_date=$request->input('second_date');

			//get all accounts in the ledger
			$deletion_flag=1;
			$values=array($deletion_flag);

				//Query the ledgers table for all accounts
				$accounts=Ledger:: get();


					//Pull the transactions of the account in the briefs table
					foreach ($accounts as $key => $value) {

						$debit_trans=History::where('ledger_id',$value->id)
											->where('transaction_type','debit')
										 	->whereBetween('transaction_date', array($first_date, $second_date))
		        				   		 	 ->sum('amount');

		        							//Push into the debit trans array	
		        							array_push($debit_trans_array, $debit_trans);

		        		$credit_trans=History::where('ledger_id',$value->id)
											->where('transaction_type','credit')
										 	->whereBetween('transaction_date', array($first_date, $second_date))
		        				   		 	 ->sum('amount');

		        				   		 	 //Push into the credit trans array
		        				   		 	 array_push($credit_trans_array, $credit_trans);

					}//end of foreach	

					//retun the values for display	
					return view('gledger.fr_result',compact('accounts','debit_trans_array','credit_trans_array','first_date','second_date'));
	}

	//Request to get the general ledger of the parish council 
	public function getCwa(){

		return view('gledger.cwa_search');
	}

	//Receive the post of the general ledger of the parish council
	public function postCwa(Request $request){

		$deletion_flag; 	//A deletion flag of the ledgers table
		$values;			//An array for the eloquent deletion
		$accounts;			//Holds all the accounts of the cash book
		$debit_trans;		//A Particular account debit transaction
		$credit_trans;		//A particular account credit transactions
		$first_date;		//Input first date
		$second_date;       //Input second date
		$debit_trans_array=array();		//An array that holds the list of debit trans of all accounts
		$credit_trans_array=array();		//An array that holds the list of credit transactions of all accounts

		//Get the user input
		$first_date=$request->input('first_date');
		$second_date=$request->input('second_date');

			//get all accounts in the ledger
			$deletion_flag=1;
			$values=array($deletion_flag);

				//Query the ledgers table for all accounts
				$accounts=Register::get();


					//Pull the transactions of the account in the briefs table
					foreach ($accounts as $key => $value) {

						$debit_trans=Occasion::where('register_id',$value->id)
											->where('transaction_type','debit')
										 	->whereBetween('transaction_date', array($first_date, $second_date))
		        				   		 	 ->sum('amount');

		        							//Push into the debit trans array	
		        							array_push($debit_trans_array, $debit_trans);

		        		$credit_trans=Occasion::where('register_id',$value->id)
											->where('transaction_type','credit')
										 	->whereBetween('transaction_date', array($first_date, $second_date))
		        				   		 	 ->sum('amount');

		        				   		 	 //Push into the credit trans array
		        				   		 	 array_push($credit_trans_array, $credit_trans);

					}//end of foreach	

					//retun the values for display	
					return view('gledger.cwa_result',compact('accounts','debit_trans_array','credit_trans_array','first_date','second_date'));
	}

	//Request to get the general ledger of the parish council 
	public function getPc(){

		return view('gledger.pc_search');
	}

	//Receive the post of the general ledger of the parish council
	public function postPc(Request $request){

		$deletion_flag; 	//A deletion flag of the ledgers table
		$values;			//An array for the eloquent deletion
		$accounts;			//Holds all the accounts of the cash book
		$debit_trans;		//A Particular account debit transaction
		$credit_trans;		//A particular account credit transactions
		$first_date;		//Input first date
		$second_date;       //Input second date
		$debit_trans_array=array();		//An array that holds the list of debit trans of all accounts
		$credit_trans_array=array();		//An array that holds the list of credit transactions of all accounts

		//Get the user input
		$first_date=$request->input('first_date');
		$second_date=$request->input('second_date');

			//get all accounts in the ledger
			$deletion_flag=1;
			$values=array($deletion_flag);

				//Query the ledgers table for all accounts
				$accounts=Book::get();


					//Pull the transactions of the account in the briefs table
					foreach ($accounts as $key => $value) {

						$debit_trans=Archive::where('book_id',$value->id)
											->where('transaction_type','debit')
										 	->whereBetween('transaction_date', array($first_date, $second_date))
		        				   		 	 ->sum('amount');

		        							//Push into the debit trans array	
		        							array_push($debit_trans_array, $debit_trans);

		        		$credit_trans=Archive::where('book_id',$value->id)
											->where('transaction_type','credit')
										 	->whereBetween('transaction_date', array($first_date, $second_date))
		        				   		 	 ->sum('amount');

		        				   		 	 //Push into the credit trans array
		        				   		 	 array_push($credit_trans_array, $credit_trans);

					}//end of foreach	

					//retun the values for display	
					return view('gledger.pc_result',compact('accounts','debit_trans_array','credit_trans_array','first_date','second_date'));
	}

	//Request to get the general ledger of the parish council 
	public function getLanda(){

		return view('gledger.landa_search');
	}

	//Receive the post of the general ledger of the parish council
	public function postLanda(Request $request){

		$deletion_flag; 	//A deletion flag of the ledgers table
		$values;			//An array for the eloquent deletion
		$accounts;			//Holds all the accounts of the cash book
		$debit_trans;		//A Particular account debit transaction
		$credit_trans;		//A particular account credit transactions
		$first_date;		//Input first date
		$second_date;       //Input second date
		$debit_trans_array=array();		//An array that holds the list of debit trans of all accounts
		$credit_trans_array=array();		//An array that holds the list of credit transactions of all accounts

		//Get the user input
		$first_date=$request->input('first_date');
		$second_date=$request->input('second_date');

			//get all accounts in the ledger
			$deletion_flag=1;
			$values=array($deletion_flag);

				//Query the ledgers table for all accounts
				$accounts=Sheet:: get();


					//Pull the transactions of the account in the briefs table
					foreach ($accounts as $key => $value) {

						$debit_trans=Chronicle::where('sheet_id',$value->id)
											->where('transaction_type','debit')
										 	->whereBetween('transaction_date', array($first_date, $second_date))
		        				   		 	 ->sum('amount');

		        							//Push into the debit trans array	
		        							array_push($debit_trans_array, $debit_trans);

		        		$credit_trans=Chronicle::where('sheet_id',$value->id)
											->where('transaction_type','credit')
										 	->whereBetween('transaction_date', array($first_date, $second_date))
		        				   		 	 ->sum('amount');

		        				   		 	 //Push into the credit trans array
		        				   		 	 array_push($credit_trans_array, $credit_trans);

					}//end of foreach	

					//retun the values for display	
					return view('gledger.landa_result',compact('accounts','debit_trans_array','credit_trans_array','first_date','second_date'));
	}
}
