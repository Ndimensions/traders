<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Expense;
use App\Account;
use App\Invoice;
use App\Transaction;
use App\Journal;
use App\Tier;
use App\Ledger;
use App\Brief;
use App\Balance;
use App\Entry;
use App\Voucher;
use App\Periodical;
use App\Receipt;
use App\Type;
use Carbon;
use Auth;
use DB;

class FathersIncomeController extends Controller {


   public function __construct()
    {
        $this->middleware('auth');
        
    }


protected  $rules = [
					 'amount' =>	 ['required','numeric'],
                    'payee' => ['required','regex:/^[\pL\s\-]+$/u'],
					 'description' => ['required'],
					 'account_id'=>['required']
					  						];
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

    $posted_flag = 1;

    $values = array($posted_flag);

		$vouchers = Voucher::whereIn('posted_flag', $values)
                        ->orderBy('created_at', 'desc')->simplePaginate(50);

		$vouchers->setPath('vouchers');

		//$vouchers->load('user','account');

		 
       return View('vouchers.index',compact('vouchers'));
	
 	}

	/**
  	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('vouchers.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, $this->rules);

DB::beginTransaction();

        $ledger_id=$request->input('account_id');

        // $values = array($ledger_id);
        // $account_id= Account::wherein('ledger_id',$values)->pluck('id');
        // $account_type=Account::TypeId($account_id)->pluck('type_id');


        $values = array($ledger_id);

        //Check if the account exists
        $check_account=Ledger::wherein('id',$values)->pluck('id');


        if(!$check_account)
        {
        	Session::flash('message', ' The Account ID does not exist/Please search account to confirm');
        	return view ('vouchers.create');

        }

        //The account id is equal to the ledger id
        $account_id= Account::wherein('ledger_id',$values)->pluck('id');

        $account=Account::find($account_id);


        $ledger=Ledger::find($ledger_id);
        $account_divison=$ledger->division->name;

        // $votehead=Type::find($account_type)->name;
 
        if($account_divison !='EXPENSE')
        {
          Session::flash('message', ' The Account you selected is not an expense account.It is in the '.$account_divison.'\t'.'division');
          return view ('vouchers.create');
        }

        if($request->has('transaction_date')){
             $transaction_date=$request->input('transaction_date');
            
        }elseif (!$request->has('transaction_date')) {
            $transaction_date=Carbon::now('EAT');
        }
        
      

        $last=Expense::orderBy('created_at','desc')->first();

        $receipt=Receipt::where('id','=','1')->pluck('payments');

        $receipt_id=$receipt+1;
        
          $receipt_update=array(
            'payments'=>$receipt_id
            );
        
        $receipts=Receipt::find(1);
          
        $receipts->update($receipt_update);

        $receipt_id=$receipt_id;

        $transaction=Transaction::create([
            'transaction_no'=>$receipt_id,
            ]);

        //Get the transaction of the inserted id
        $transaction_id=$transaction->id;


        //Creating the corresponding journal entry
        $journal_update=Periodical::create([
            'transaction_id'=>$transaction_id
        	]);

        //Get the id of the inserted journal entry
        $periodical_id = $journal_update->id;
       // $transaction_id=Periodical::find($periodical_id)->transaction_id;
    	 
        	$voucher = new Voucher(array(
                'amount'=>$request->input('amount'),
        		'transaction_no'=>$receipt_id,
                'transaction_id'=>$transaction_id,
                'transaction_type'=>'CASH VOUCHER EXPENSE',
        		'transaction_date'=>$transaction_date,
        		'description'=>$request->input('description'),
                'payees_name'=>$request->input('payee'),
        		'user_id'=>Auth::user()->id,
                'credit'=>'1',
                'debit'=>$ledger_id,
                'posted_flag'=>'0',
                'financial_id'=>"",
                'deletion_flag'=>""
         		));


		  $account = Account::find($account_id);

		  $account = $account->vouchers()->save($voucher);


        //Increase the balance of the expense account in the balances to the debit side
        $ledger_id=$ledger->id;
        $ledger=Balance::find($ledger_id);	
        $acc_bal=$ledger->debit;
        $votehead=Ledger::find($ledger_id)->type->name;
        $new_bal=$acc_bal+$request->input('amount'); 
        $account_ledger=$ledger;    	
        $ledger_update=array(
        	'debit'=>$new_bal
        	);

        $ledger->update($ledger_update);

        //Increase the cash/bank account balance in the credit side
        $ledger_id=Balance::where('ledger_id','=','1')->pluck('id');
        $ledger=Balance::find($ledger_id);
        $current_balance=$ledger->credit;
        $new_balance=$current_balance+$request->input('amount');

        $ledger_update=array(
            'credit'=>$new_balance
            );
        $ledger->update($ledger_update);
 
        $trans="Cash";

DB::commit();
		if($account)
		{
		
        Session::flash('message', 'Successfully added voucher');

         $print='voucher'; 

        return view ('vouchers.print_vouchers',compact('trans','print','voucher','receipt_id','votehead'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 
       

	}

}
