<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
 
use App\Brief;
use App\Ledger;
use App\Division;
use App\Type;
use Session;
use Redirect;
use Carbon;
use Auth;
use DB;
class IncomesAccountRptController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected  $rules = ['first_date' => ['required','date_format:"Y-m-d'],
					 'second_date'=>['required','date_format:"Y-m-d"'],


					];
	public function index()
	{
		//
		dd('IncomesAccountRptController');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$income_flag = 1;

		$values = array($income_flag);
		$types=Type::select('id')->whereIn('division_id',$values)->get()->toArray();

		$account=Ledger::whereIn('type_id',$types)->pluck('name','id')->all();
		  		 
		return view('cover.search_income_acc_trans',compact('account'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

	$this->validate($request, $this->rules);

		$first_date = $request->input('first_date');
		
		$second_date = $request->input('second_date');

 
		$ledger_id=$request->input('ledger_id');

  
		
		//If the account is not selected
		if(!$request->has('ledger_id')){
			Session::flash('message', 'Please select the account name ');
        return Redirect::to('incomes/rpt/create')->withInput();
		}

		 

		$account_name=Ledger::where('id',$ledger_id)->pluck('name');
		//Star of the query
			 
			$trans=Brief::whereBetween('transaction_date', array($first_date, $second_date))
					->where('ledger_id',$ledger_id)
					->where('transaction_type','credit')
					->where('description','Income')
						->get();
	 
		return view('cover.income_acc_res',compact('account_name','first_date','second_date','trans'));
			 
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
