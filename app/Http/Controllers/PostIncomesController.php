<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Fee;
use Session;
use Redirect;
use Carbon;
use Auth;
use DB;

class PostIncomesController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('role');
	}


protected  $rules = [ 
 					 'date'=>['required','date_format:"Y-m-d"'],	
					 ];


	public function index()
	{
		return view('fees.search_incomes_to_post');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, $this->rules);

		$date = $request->input('date');
		
 		$transaction_type = $request->input('transaction_type');


		if($date ===""  && $transaction_type ==="")
			{
				
			Session::flash('message', 'At Least one Field is required');
	        return Redirect::to('post/incomes');
			
			}
			
		if($transaction_type==="")
			{
					Session::flash('message', 'PLease select the transaction type.');
	        return Redirect::to('post/incomes');
			}

	
		//if the first date is the only one set
		if($request->has('date')  && $request->has('transaction_type')){

		 	$today_fee=Fee::where('transaction_date','=',$date)
		 							->where('transaction_type', '=',$transaction_type)
		 							->where('post_flag','=','0')
		 							->get();
		 	
			$receipts=$today_fee->sum('amount');
        
      	 }		 

	
        //return ('students.search_res',compact('student'));
		if($today_fee->count()>0)
		{
		 return view('cover.transaction_to_post_res',compact('today_fee','receipts','date','second_date','transaction_type'));
		 $today_fee->load('student');
		 
		}
		else
		{
		Session::flash('message', 'Results are not available.');
        return Redirect::to('post/incomes')->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
