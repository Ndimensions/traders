<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Financial;
use View;
use Auth;
use Entrust;
use Session;
use Redirect;
 
use Illuminate\Http\Request;

class FinancialsController extends Controller {


    public function __construct()
    {
        $this->middleware('auth');
        //  $this->middleware('role');
    }


protected  $rules = ['name' =>   ['required'],
                     'begin_date' => ['required','date_format:"Y-m-d','before:end_date','unique:financials'],
                     'end_date'=>['required','date_format:"Y-m-d"','unique:financials'], 
                     'active'=>['unique:financials']];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    { 

       $financials = Financial::get();

        // show the edit form and pass the nerd
       return View::make('financials.index',compact('financials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
        // show the edit form and pass the nerd
       return View::make('financials.create');
           
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request )
    {

          $messages = [
                'unique' => 'An active term exists',
                         ];
        $active=$request->input('active');

        if($active== 'on'){
            $active=1;
        }
        else{
            $active=0;
        }

        //Check if an active financial period exists
        $active_period=Financial::where('active','1')->get();
        if(count($active_period) >0){
            $active=0;
         }
 
     
        $this->validate($request, $this->rules);

        //dd($request->all());
        $financials =new Financial;
        $financials ::create(array(
            'name'=>$request->input('name'),
            'begin_date'=>$request->input('begin_date'),
            'end_date'=>$request->input('end_date'),
            'active'=>$active

            ));

        if($financials)
        {

        Session::flash('message', 'Successfully created the financial period.');
        return Redirect::to('financials');
        }

        Session::flash('message','Failed to create record');
         return Redirect:: to('financials');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $financial=Financial::find($id);
        
        // $terms=$subjects->toArray();
        return View ('financials.show',compact('term'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        
        // get the term
        $financial = financial::find($id);

        // show the edit form and pass the nerd
        return View('financials.edit')
           ->with('financial', $financial);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {

  $rules = ['name' =>    ['required','Unique:financials,id,{$id}'],
                     'begin_date' => ['required','date_format:"Y-m-d','before:end_date','unique:financials,id,{$id}'],
                     'end_date'=>['required','date_format:"Y-m-d"','unique:financials,id,{$id}'],    
                     'active'=>['unique:financials']];

        $this->validate($request, $rules);
        //$term = new Subject();
        $financial = financial::find($id);


        $active_period=$financial->active;
        if($active_period==1){
             Session::flash('message',' Update failed, this is the active financial period.');
               return Redirect:: to('financials');
        }
        else
        {
            $financial->update($request->all());

        }
        
        // redirect
        Session::flash('message', 'Successfully updated financial period.');
        return Redirect::to('financials');

        Session::flash('message','Failed to update record');
         return Redirect:: to('financials/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $financial = financial::find($id);
       
        $active_period=$financial->active;
        if($active_period==1){
             Session::flash('message',' Deletion failed, this is the active financial period.');
               return Redirect:: to('financials');
        }
        // if(count($financial->))
        // {
        //     Session::flash('message',' Deletion failed. Term is associated with other parts.</br>Delete Others First');
        //     return Redirect:: to('financials');
            
        // }

        // else  
        // {
                
            $financial->delete();
            Session::flash('message',' Financial period deleted.');
            return Redirect:: to('financials');
                    
         
        

       
    }

}
