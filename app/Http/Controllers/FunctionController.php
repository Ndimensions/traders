<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Type;
use App\Category;
use App\User;
use App\Routine;
use Redirect;
use Session;
use Auth;
use DB;
use App\Division;

class FunctionController extends Controller {

protected  $rules = ['name' => ['required', 'min:3','unique:Types'],
					'description' => ['required'],
					'code'=>['required'],	];
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		 $routines=Routine::get();
		 return view('functions.index',compact('routines'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$divisions=Division::lists('name','id');
		return view('functions.create',compact('divisions'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, $this->rules);
		$fund_id=1;
		$division_id=$request->input('division_id');
		$user_id=Auth::user()->id;
		$code=$request->input('code');

		DB::beginTransaction();

		$routine =new Routine;
        $routine ::create(array(
        	'name' =>$request->input('name'),
        	'description'=>$request->input('description'),
         	'user_id'=>$user_id,
         	'fund_id'=>$fund_id,
         	'division_id'=>$division_id,
         	'function_code'=>$fund_id.$division_id.$code
        	));

		DB::commit();

		 if($routine)
        {

       	Session::flash('message', 'Successfully created function');
        return Redirect::to('functions');
        }

        Session::flash('message','Failed to create record');
         return Redirect:: to('fucntions/create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
 		$routine = Routine::find($id);

        // show the edit form and pass the nerd
        return View('functions.edit',compact('routine'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$routine= Routine::find($id);

		 if(count($routine->types))
		{
			
	        Session::flash('message',' Deletion Failed. Function Has Vote heads');
	        return Redirect:: to('functions');
			
		}

	 	else  
	 	{
		 		
	 	$routine->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the function');
        return Redirect::to('functions');
	 				
		 
	 	}
	}

}
