<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Expense;
use App\Account;
use App\Transfer;
use App\Invoice;
use App\Journal;
use App\Ledger;
use App\Entry;
use App\Voucher;
use App\Periodical;
use App\Receipt;
use App\Bank;
use App\Division;
use App\Type;
use App\Transaction;
use App\Financial;
use App\Balance;
use App\Cheque;
use Carbon;
use Auth;
use DB;

class FrBankTransfersController extends Controller {

	  public function __construct()
    {
        $this->middleware('auth');
        
    }


protected  $rules = [
					 'amount' =>	 ['required','numeric'],
					 'description' => ['required'],
					 'payee' => ['required'],
					 'account_id'=>['required']
					 

					  
					  						];
	public function index()
	{


		 $posted_flag = 1;

   		 $values = array($posted_flag);

		$banks = Bank::whereIn('posted_flag', $values)
                        ->orderBy('created_at', 'desc')->simplePaginate(10);

		$banks->setPath('banks');

		//$vouchers->load('user','account');

 
		 
       return View('banks.index',compact('banks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		 
        $expense_votehead=2;
        $votehead=Division::find($expense_votehead);

        $expense_accounts=$votehead->ledgers->pluck('name')->all();

        $cash_accounts=Ledger::where('type_id',21)
                                        ->pluck('name')->all();

		return view('banks.transfers',compact('expense_accounts','cash_accounts'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, $this->rules);

DB::beginTransaction();

        $ledger_id=$request->input('account_id');
        $cash_account=$request->input('cash_account');

    
        $values = array($ledger_id);

        //Check if the account exists
        $check_account=Ledger::wherein('id',$values)->pluck('id');


        if(!$check_account)
        {
        	Session::flash('message', ' The Account ID does not exist/Please search account to confirm');
        	return view ('banks.create');

        }
    

        // 
        $ledger=Ledger::find($ledger_id);
        $ledger_name=$ledger->name;
        $votehead=$ledger->type->name;

        // $votehead=Type::find($account_type)->name;


        if($request->has('transaction_date')){
             $transaction_date=$request->input('transaction_date');
            
        }elseif (!$request->has('transaction_date')) {
            $transaction_date=Carbon::now('EAT');
        }
        
             //Get the financial id
        $financial_id=Financial::CurrentFinancial()->first()->id;
      

          //Creating the corresponding journal entry
        $journal_update=Periodical::create([
            'transaction_id'=>''
            ]);

        $periodial_id=$journal_update->id;
        $periodical=Periodical::find($periodial_id);

        $transaction=Transaction::create([
            'transaction_no'=>$periodial_id,
            ]);

        //Get the transaction of the inserted id
        $transaction_id=$transaction->id;

        //Update the periodical id withe transaction number
        $periodical_update=array(
            'transaction_id'=>$transaction_id
            );
        $periodical->update($periodical_update);
    
   
        $receipt_id=$periodial_id;

        $receipt_id=sprintf("%07d", $receipt_id);

    	 
        $voucher = Voucher::create([
                'amount'=>$request->input('amount'),
                'transaction_no'=>$receipt_id,
                'transaction_id'=>$transaction_id,
                'transaction_type'=>'Expense',
                'transaction_date'=>$transaction_date,
                'description'=>$request->input('description'),
                'ref'=>$request->input('ref'),
                'payees_name'=>$request->input('payee'),
                'user_id'=>Auth::user()->id,
                'particulars'=>$ledger_name,
                'category'=>'Drawing',
                'credit'=>$cash_account,
                'debit'=>$ledger_id,
                'posted_flag'=>'0',
                'financial_id'=>$financial_id,
                'deletion_flag'=>""
               
                ]);

 
 
        $trans="Bank Transfer:&emsp;";

DB::commit();
		if($voucher)
		{
		
        Session::flash('message', 'Successfully added bank transfer');

         $print='voucher'; 
         $bank=$voucher;

        return view ('banks.print_transfers',compact('trans','print','bank','voucher','receipt_id','votehead'));

		}



	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
