<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
 
use Session;
use Redirect;
use App\Invest;
use App\Payment;
use App\Interest;
use App\Voucher;
use Carbon;
use Auth;
use DB;

class InvestController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$deletion_flag = '0';

		$np_posted='0';
		$np_values=array($np_posted);

		$values = array($deletion_flag);
		 
		$journals = Invest::where('type','I')
							->orderBy('created_at','Asc')
							->get();
	 
         return View('invest.index',compact('journals'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		 
		$journal_id=$id;
		$transaction_id=Invest::find($journal_id)->transaction_id;
		
		$voucher_id=Voucher::where('transaction_id',$transaction_id)->first()->id;
		$voucher=Voucher::find($voucher_id);
		$code=$voucher->code;

		DB::beginTransaction();

			//Reject the payment detials earirl
			$payment_id=Payment::where('code',$code)->first()->id;
			$update=array(
				'reject_flag'=>1
			);
			$payment=Payment::Find($payment_id);
			$payment->update($update);

			//Delete the interest record
			$interest_id=Interest::where('transaction_id',$transaction_id)->first()->id;
			$interest=Interest::find($interest_id);
			$interest->delete();

			//Delete the voucher
			$voucher->delete();

 		DB::commit();	

 	   $invest=Invest::find($id);
 	   $invest->delete();

		 
		Session::flash('message', ' Successfully rejected transaction.');

		return redirect('invest');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 //Update the deletion flag  in the periodicals table
	//     $journal_id=$id;
	//     $transaction_id=Invest::find($journal_id)->transaction_id;
	//     $voucher_id=Voucher::where('transaction_id',$transaction_id)->first()->id;
	//     $journal=Voucher::find($voucher_id);
	// 	DB::beginTransaction();
	 
	    
	// 	$deletion_update=array(
    //     	'deletion_flag'=>'1',
    //     	'deletion_date'=> Carbon::now('EAT'),
    //     	'deleted_by'=>Auth::user()->first_name.Auth::user()->last_name
    //     	);

    //     $journal->update($deletion_update);

		 
 	// 	DB::commit();	

 	//    $invest=Invest::find($id);
 	//    $invest->delete();

		 
	// 	Session::flash('message', ' Successfully rejected transaction.');

	// 	return redirect('invest');

	}

}
