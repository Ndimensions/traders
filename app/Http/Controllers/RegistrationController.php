<?php namespace App\Http\Controllers;

use App\Commission;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use App\Voucher;
use App\Transaction;
use App\Financial;
use App\Invest;
use App\Periodical;
use App\Ledger;
use App\User;
use App\Promote;
use App\Income;
use App\Payment;
use App\Statement;
use Redirect;
use Auth;
use Session;
use Carbon;

class RegistrationController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('adminroutes');	
      
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user=Auth::user();
		$is_admin=$user->is_admin;
		if($is_admin == 0){
			return redirect('home');
		}
		
		//Get all payments
		$payments=Payment::where('verify_flag',0)
							->where('type','R')
							->where('reject_flag',0)
							->orderBy('created_at','Asc')->get();
		
		return view('registration.verify_payments',compact('payments'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('registration.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		 
		$code=$request->input('code');
		$user_id=Auth::user()->id;

		if(Payment::where('code',$code)
					->where('reject_flag',0)->exists()){
			Session::flash('message', 'Invalid mpesa code');
			return Redirect::back()->withInput();
		}

		DB::beginTransaction();

		$payments=Payment::create([
			'code'=>$code,
			'user_id'=>$user_id,
			'cust_id'=>0,
			'type'=>'R'
		]);

		DB::commit();
		
		if($payments){

			$user_update=array(
				'active'=>1
			);
			$user=Auth::user();
			$user->update($user_update);
		}

		return redirect('home')->with($code);
	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//Update the payment flag
		DB::beginTransaction();

		$payment_id=$id;
		$payment=Payment::Find($payment_id);
		
		$update=array(
			'verify_flag'=>1
		);
		$payment->update($update);
		$user_id=$payment->user_id;

		
		$principal=1100;
	
		$ledger_id=6;
		$ledger=Ledger::find($ledger_id);
		$ledger_name=$ledger->name;
		
		
		$user=User::Find($user_id);
		$user_id=$user->id;
		$user_name=$user->name;
		$promo_code=$user->code;

		

		//Create the customer ledger_id
		//Create the user ledger record in the ledgers
		$subscriber_type=7;
		$ledger=Ledger::create([
			'type_id'=>$subscriber_type,
			'user_id'=>$user_id,
			'name'=>$user_name,
			'description'=>'Subscriber Account'
		]);
		$refering_id=Promote::where('code',$promo_code)->first()->user_id;
		$cust_id=Ledger::where('user_id',$refering_id)->first()->id;
 
		 
        $transaction_date=Carbon::now('EAT');
		
       
         //Get the financial id
        $financial_id=Financial::CurrentFinancial()->first()->id;
        
        
		 
		//if there is a commision code
		//Create the interests table
		if($promo_code!=null){

			 //Creating the corresponding journal entry
			 $periodical=Periodical::create([
				'transaction_id'=>1
				]);
	
			$periodial_id=$periodical->id;
			$periodical=Periodical::find($periodial_id);
	
			$transaction=Transaction::create([
				'transaction_no'=>$periodial_id,
				]);
	
			//Get the transaction of the inserted id
			$transaction_id=$transaction->id;
	
			//Update the periodical id withe transaction number
			$periodical_update=array(
				'transaction_id'=>$transaction_id
				);
			$periodical->update($periodical_update);
		
		   
			$receipt_id=$periodial_id;
			$receipt_id=sprintf("%07d", $receipt_id);
			
			$amount=500;
			$code=$promo_code;
			$promote=Promote::where('code',$code)->first();
			$promote_id=$promote->id;

			$commision=Commission::create([
				'amount'=>$amount,
				'code'=>$code,
				'transaction_date'=>$transaction_date,
				'maturity_date'=>$transaction_date,
				'user_id'=>$user_id,
				'transaction_id'=>$transaction_id,
				'promote_id'=>$promote_id,
				'paid_flag'=>'0',
				'mature_flag'=>'1',
				'cust_id'=>$cust_id
			]);

			 

			//Get the invest id for the journal transaction
			 $invest=Invest::create([
				'transaction_id'=>1,
				'type'=>'C'
				]);
	
			$invest_id=$invest->id;

			//Transfer the commission directly into the referral account
			$statement=Statement::create([
				'amount'=>$amount,
				'transaction_date'=>$transaction_date,
				'transaction_type'=>'Credit',
				'description'=>'Journal',
				'particulars'=>'Commission',
				'category'=>'Deposit',
				'invest_id'=>$invest_id,
				'cust_id'=>$cust_id,
				'ledger_id'=>5,
				'user_id'=>$user_id
			]);
			
			$commission_ledger=Ledger::find(5)->name;

			//The transactions table
			$voucher = Voucher::create([
				'amount'=>$amount,
				'transaction_no'=>$receipt_id,
				'transaction_id'=>$transaction_id,
				'transaction_type'=>'Expense',
				'transaction_date'=>$transaction_date,
				'description'=>'Commission',
				'ref'=>$transaction_id,
				'payees_name'=>Auth::user()->name,
				'user_id'=>Auth::user()->id,
				'particulars'=>$commission_ledger,
				'category'=>'Drawing',
				'credit'=>'1',
				'debit'=>'5',
				'posted_flag'=>'0',
				'financial_id'=>$financial_id,
				'deletion_flag'=>"",
				'cust_id'=>$cust_id,
				'code'=>''
				 ]);

				  //Creating the corresponding journal entry
				  $journal_update=Income::create([
					'transaction_id'=>1
					]);
		
				$periodial_id=$journal_update->id;
				$periodical=Income::find($periodial_id);
		
				$transaction=Transaction::create([
					'transaction_no'=>$periodial_id,
					]);
		
				//Get the transaction of the inserted id
				$transaction_id=$transaction->id;
		
				//Update the periodical id withe transaction number
				$periodical_update=array(
					'transaction_id'=>$transaction_id
					);
				$periodical->update($periodical_update);
			
			  
				$receipt_id=$periodial_id;
		
				$receipt_id=sprintf("%07d", $receipt_id);
		

				 $voucher = Voucher::create([
					'amount'=>1000,
					'transaction_no'=>$receipt_id,
					'transaction_id'=>$transaction_id,
					'transaction_type'=>'Income',
					'transaction_date'=>$transaction_date,
					'description'=>'Registration',
					'ref'=>$transaction_id,
					'payees_name'=>Auth::user()->name,
					'user_id'=>Auth::user()->id,
					'particulars'=>$ledger_name,
					'category'=>'Deposit',
					'credit'=>'6',
					'debit'=>'1',
					'posted_flag'=>'0',
					'financial_id'=>$financial_id,
					'deletion_flag'=>"",
					'cust_id'=>$cust_id,
					'code'=>''
					 ]);
	
			 
		}//end of the promo code funtion
		// else {

		// 		  //Creating the corresponding journal entry
		// 		$journal_update=Income::create([
		// 			'transaction_id'=>1
		// 			]);
		
		// 		$periodial_id=$journal_update->id;
		// 		$periodical=Income::find($periodial_id);
		
		// 		$transaction=Transaction::create([
		// 			'transaction_no'=>$periodial_id,
		// 			]);
		
		// 		//Get the transaction of the inserted id
		// 		$transaction_id=$transaction->id;
		
		// 		//Update the periodical id withe transaction number
		// 		$periodical_update=array(
		// 			'transaction_id'=>$transaction_id
		// 			);
		// 		$periodical->update($periodical_update);
			
			  
		// 		$receipt_id=$periodial_id;
		
		// 		$receipt_id=sprintf("%07d", $receipt_id);
		
		// 		//The transactions table
		// 		$voucher = Voucher::create([
		// 			'amount'=>$principal,
		// 			'transaction_no'=>$receipt_id,
		// 			'transaction_id'=>$transaction_id,
		// 			'transaction_type'=>'Income',
		// 			'transaction_date'=>$transaction_date,
		// 			'description'=>'Registration',
		// 			'ref'=>$transaction_id,
		// 			'payees_name'=>Auth::user()->name,
		// 			'user_id'=>Auth::user()->id,
		// 			'particulars'=>$ledger_name,
		// 			'category'=>'Deposit',
		// 			'credit'=>'6',
		// 			'debit'=>'1',
		// 			'posted_flag'=>'0',
		// 			'financial_id'=>$financial_id,
		// 			'deletion_flag'=>"",
		// 			'cust_id'=>$cust_id,
		// 			'code'=>''
		// 			 ]);
	
		// }         
		

	//After the payment, creat the user investment pacakge
		$dozen_id=1;
		$user_update=array(
			'dozen_id'=>$dozen_id,
			'active'=>1,
			 'verify'=>1
		);
		$user->update($user_update);

	DB::commit();
		if($voucher)
		{

		//Create the user invite code for 
		$code=rand(100000,1000000);
		$transaction_date=Carbon::now('EAT');
		$maturity_date=$transaction_date;
		 
		//dd($maturity_date);
		$amount=Financial::CurrentFinancial()->first()->commission;

		//Check if the user has an active code
		if(Promote::where('user_id',$user_id)
					->where('active_flag',1)->exists()){
			
			$code=Promote::where('user_id',$user_id)
							->where('active_flag',1)->first()->code;
				
		}else{
			
			//Create the promotes record
			$promote=Promote::create([
				'amount'=>$amount,
				'user_id'=>$user_id,
				'code'=>$code,
				'transaction_date'=>$transaction_date,
				'expiry_date'=>$maturity_date
				]);
					
		}
		
        Session::flash('message', 'Successfully activated account');

		 // $print='voucher'; 
		 // $votehead='Investmetn';
		 // $trans='Buy';

		 return Redirect::to('registration');

       // return view ('registration.print_registration',compact('trans','print','voucher','receipt_id','votehead'));

		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$payment_id=$id;
		DB::beginTransaction();
		$update=array(
			'reject_flag'=>1
		);
		$payment=Payment::find($payment_id);
		$payment->update($update);

		$user_id=$payment->user_id;
		$user_update=array(
			'active'=>0
		);
		$user=User::Find($user_id);
		$user->update($user_update);
		DB::commit();


		Session::flash('message', 'M-PEsa Code rejected');
		return Redirect::back();

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
