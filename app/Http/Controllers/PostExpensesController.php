<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Expense;
use App\Account;
use App\Invoice;
use App\Journal;
use App\Ledger;
use App\Entry;
use App\Periodical;
use App\Bank;
use App\Voucher;
use App\History;
use App\Post;
use App\User;
use App\Record;
use Carbon;
use Auth;
use DB;

class PostExpensesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $Request)
	{
		$deletion_flag = 1;

		$values = array($deletion_flag);
		$transactions_array=array();

DB::beginTransaction();
	 
		$bank_journals = Periodical::whereNotIn('deletion_flag', $values)
									->where('credit','8')
								->get();
		 

		foreach ($bank_journals as $key => $value)
		{

				$transaction_type=$value->transaction_type;
				$transaction_no=$value->transaction_no;

				array_push($transactions_array, $transaction_type);


			 	 $bank_id=Bank::where('transaction_no','=',$transaction_no)->pluck('id');
				 $bank=Bank::find($bank_id);
		   			 	  
					$posted_update=array(
			        	'posted_flag'=>'1'
			        	);

		        $bank->update($posted_update);


				//Get the account balance in  the ledger table
				$ledger=Ledger::where('id','=',$value->debit)->first();
		        $account_current_balance=$ledger->balance;
		        $balance=$account_current_balance+$value->amount;

		        $ledger_id=$value->debit;

				 //Create this record in the Record table
				$record = new Record(array(
        		 'posting_date'=> Carbon::now('EAT'),
        		 'ledger_id'=>$ledger_id,
        		 'transaction_type'=>'Debit',
        		 'debit'=>$value->amount,
        		 'credit'=>'0',
        		 'particulars'=>$value->particulars,
        		 'transaction_no'=>$value->transaction_no,
        		 'balance'=> $balance,
        		 'transaction_date'=>$value->transaction_date       		
        		));

				$user = User::find(Auth::user()->id);

				$user= $user->records()->save($record);	

				//Increase the balance of the account in the ledger
                $ledger_id=$ledger->id;
                $ledger=Ledger::find($ledger_id);           
                $ledger_update=array(
                    'balance'=>$balance
                    );
                $ledger->update($ledger_update);						

			}

		//Update the bank in the general ledger/records table
			$bank_credit_transaction=Periodical::whereNotIn('deletion_flag', $values)
											->where('transaction_type','=','CHEQUE PAYMENT')
											->where('credit','=','8')
											->sum('amount');

			//Update the bank balance in the ledger
			$ledger=Ledger::where('id','=','8')->first();
		    $bank_account_balance=$ledger->balance;
		    $bank_balance=$bank_account_balance-$bank_credit_transaction;
		 

			//Post this record in the Record table
			$record = new Record(array(
			 'posting_date'=> Carbon::now('EAT'),
			 'ledger_id'=>'8',
			 'transaction_type'=>'Credit',
			 'debit'=>'0',
			 'credit'=>$bank_credit_transaction,
			 'particulars'=>Carbon::now('EAT').'Bank Expenses',
			 'transaction_no'=>'CB',
			 'balance'=> $bank_balance,
			 'transaction_date'=>Carbon::now('EAT')
			   		
			));

			$user = User::find(Auth::user()->id);

			$user= $user->records()->save($record);	


			//Reduce and update the balance in the bank account in the ledger
		    $bank_ledger_id=Ledger::where('id','=','8')->pluck('id');
		    $bank_ledger=Ledger::find($bank_ledger_id);
		    

		    $ledger_update=array(
		        'balance'=>$bank_balance
		        );

		    $bank_ledger->update($ledger_update);


	//Cash Transactons
		$cash_journals = Periodical::whereNotIn('deletion_flag', $values)
								        ->where('credit','1')
							            ->get();

		foreach ($cash_journals as $key => $value)
			{
				$transaction_type=$value->transaction_type;
				$transaction_no=$value->transaction_no;
				  
				$voucher_id=Voucher::where('transaction_no','=',$transaction_no)->pluck('id');
				$voucher=Voucher::find($voucher_id);
	   			 	    
				$posted_update=array(
		        	'posted_flag'=>'1'
		        	);

		        $voucher->update($posted_update);

		        //Get the account balance in  the ledger table
				$ledger=Ledger::where('id','=',$value->debit)->first();

		        $account_current_balance=$ledger->balance;
		        $balance=$account_current_balance+$value->amount;

		        $ledger_id=$value->debit;

		         
			  //Create this record in the Record table
				$record = new Record(array(
        		 'posting_date'=> Carbon::now('EAT'),
        		 'ledger_id'=>$ledger_id,
        		 'transaction_type'=>'Debit',
        		 'debit'=>$value->amount,
        		 'credit'=>'0',
        		 'particulars'=>$value->particulars,
        		 'transaction_no'=>$value->transaction_no,
        		 'balance'=>$balance,
        		 'transaction_date'=>$value->transaction_date    		
        		));

				$user = User::find(Auth::user()->id);

				$user= $user->records()->save($record);	

			 //Increase the balance of the account in the ledger
                $ledger_id=$ledger->id;
                $ledger=Ledger::find($ledger_id);           
                $ledger_update=array(
                    'balance'=>$balance
                    );
                $ledger->update($ledger_update);
				
			}

	
		//Add into the posts table to show who carried out a particular end of day posting
			$post = new Post(array(
        		 'posting_date'=> Carbon::now('EAT'),
        		'transaction_category'=>"EXPENSE POSTING",
        		
        		));

			$user = User::find(Auth::user()->id);

			$user= $user->posts()->save($post);



		$cash_credit_transaction=Periodical::whereNotIn('deletion_flag', $values)
										->where('transaction_type','=','CASH VOUCHER')
										->where('credit','=','1')
										->sum('amount');
	 
		  

		//Update the bank balance in the ledger
		$ledger=Ledger::where('id','=','1')->first();
        $cash_account_balance=$ledger->balance;
        $cash_balance=$cash_account_balance-$cash_credit_transaction;


		 //Create this record in the Record table
		$record = new Record(array(
		 'posting_date'=> Carbon::now('EAT'),
		 'ledger_id'=>'1',
		 'transaction_type'=>'Credit',
		 'debit'=>'0',
		 'credit'=>$cash_credit_transaction,
		 'particulars'=>Carbon::now('EAT').'Cash Expenses',
		 'transaction_no'=>'CB',
		 'balance'=> $cash_balance,
		 'transaction_date'=> Carbon::now('EAT')     		
		));

		$user = User::find(Auth::user()->id);

		$user= $user->records()->save($record);	

		//Reduce and update the balance in the bank account in the ledger
	    $cash_ledger_id=Ledger::where('id','=','1')->pluck('id');
	    $cash_ledger=Ledger::find($cash_ledger_id);
	    
	    $ledger_update=array(
	        'balance'=>$cash_balance
	        );

	    $cash_ledger->update($ledger_update);	
	    				 
			
 
	 DB::commit();

	 DB::beginTransaction();

	 	//Transfer the records from the periodicals table to the histories table
			$periodicals=Periodical::all();

			foreach ($periodicals as $key => $value) {
			$user = User::find(Auth::user()->id);

			$history_record=History::create([
        	'transaction_no'=>$value->transaction_no,
        	'transaction_type'=>$value->transaction_type,
           	'transaction_remarks'=>$value->transaction_remarks,
          	'transaction_date'=> $value->transaction_date,
        	'particulars'=>$value->particulars,
        	'debit'=>$value->debit,
        	'credit'=>$value->credit,
        	'amount'=>$value->amount,
        	'deletion_flag'=>$value->deletion_flag,
        	'created_date'=>$value->created_at,
        	'updated_date'=>$value->updated_at,
        	'periodical_id'=>$value->id,
        	'deletion_date'=>$value->deletion_date,
        	'deleted_by'=>$value->deleted_by
        	]); 

			$user= $user->histories()->save($history_record);

			//Delete each of the records in the periodicals table
			$periodical_delete=Periodical::find($value->id);
			$periodical_delete->delete();
			}
	DB::commit();

			$deletion_flag = 1;

			$values = array($deletion_flag);
			 
			$journals = Periodical::whereNotIn('deletion_flag', $values)
									->orderBy('created_at', 'desc')->simplePaginate(10);

			$journals->setPath('journal');

			//$journals->load('user','account');

		 
      		 return View('periodicals.index',compact('journals'));
	 

}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
