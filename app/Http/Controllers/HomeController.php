<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Commission;
use App\Payment;
use App\Interest;
use App\Statement;
use App\Withdraw;
use Illuminate\Support\Facades\Auth;
use App\Ledger;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    

    $user=Auth::user();
    $user_id=$user->id;
    $news=Article::orderBy('created_at','Asc')->get();
    if($user->active == 0){
        
        if(Payment::where('user_id',$user_id)
                        ->where('reject_flag',1)->exists()){
             
        return view('registration.create_reject');                 
        }else{
            //Select option to activate mpesa/paypal or either
            return view('registration.choose_payment');
            //return  redirect('registration/create');
        }
    }else if($user->verify == 0){
        
        return view('registration.verify_wait');
    } 
    else{
        $user=Auth::user();
        
        $user_id=$user->id;

        $cust_id=Ledger::where('user_id',$user_id)->first()->id;
        
        
        $commissions=Commission::where('cust_id',$cust_id)
                                ->where('mature_flag',1)
                                ->where('paid_flag',1)->get()->sum('amount');

        $investments=Interest::where('cust_id',$cust_id)
                                ->where('mature_flag',1)
                                ->where('paid_flag',1)->get()->sum('amount');
        
        $account_credits=Statement::where('cust_id',$cust_id)
                                    ->where('transaction_type','Credit')->get()->sum('amount');
        
        $account_debits=Statement::where('cust_id',$cust_id)
                                    ->where('transaction_type','Debit')->get()->sum('amount');

        $withdraw_requests=Withdraw::where('cust_id',$cust_id)
                                    ->where('paid_flag',0)
                                    ->orderBy('created_at','Asc')
                                    ->get()->sum('amount');
        
        $balance=$account_credits-$account_debits;
    
        return view('home',compact('commissions','investments','balance','news'));
    }
	
}
}
