<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
 
use Session;
use Redirect;
use App\Expense;
use App\Account;
use App\Bank;
use App\Voucher;
use App\Type;
use App\Ledger;
use Carbon;
use Auth;
use DB;

class ExpController extends Controller {

protected  $rules = ['transaction_no' =>	 ['required','number'],
					  
					  						];

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return view('expenses.search_exp');

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		 
		
		$transaction_no = $request->input('transaction_no');

		$deletion_flag = 1;
		$values = array($deletion_flag);
		$posted_flag='1';
		$posted_values=array($posted_flag);
		 
		
	   $trans=Voucher::where('transaction_no',$transaction_no)
	   					->where('credit','1')
	   					->orwhere('credit','2')
						->whereNotIn('deletion_flag', $values)
						->whereIn('posted_flag',$posted_values)
						->first();


		if($trans !== null){

			 $ledger_id=$trans->debit;
	         $type_id=Ledger::find($ledger_id)->type_id;
			 $votehead=Type::find($type_id)->name;
		 
	         $print='voucher'; 
	         $voucher=$trans;
	         $receipt_id=$trans->$transaction_no;
	         $trans="Cash";
	        

			return view ('vouchers.print_vouchers_copy',compact('trans','print','voucher','receipt_id','votehead'));

				
		}

		

        
		Session::flash('message', 'Transaction not found.');
		 
        return view ('expenses.search_exp');

		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
