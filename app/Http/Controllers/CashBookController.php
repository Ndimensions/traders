<?php namespace App\Http\Controllers;

use App\Brief;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Interest;
use App\Ledger;
use App\Voucher;
use App\Financial;
use App\Transaction;
use App\Statement;
use App\Invest;
use App\Withdraw;
use App\Payment;
use Session;
use Redirect;
use Auth;
use Carbon;
use Carbon\Carbon as CarbonCarbon;
use Illuminate\Http\Request;

class CashBookController extends Controller {

	//Function to request mpesa codes
	public  function Mpesacodes(){

		return view('invest.mpesa_search' );

	}//end fo the getform function
	 
	//Function to handle post of mpesa codes
	public function postmpesacodes(Request $request){

		$first_date = $request->input('first_date');
		$second_date = $request->input('second_date');

		$codes=Payment::whereBetween('created_at', array($first_date, $second_date))
						->get();

		return view('invest.mpesa_codes_report',compact('codes'));
	}//end of postmpesacodes

	public static function getForm(){

		return view('invest.cash_form' );

	}//end fo the getform function
	 
	//Post form to receive the form post
	public static function postForm(Request $request){

		$first_date = $request->input('first_date');
		$second_date = $request->input('second_date');

		//
		$cash_type=1;
		$cash_ledgers=Ledger::select('id')
						->where('type_id',$cash_type)->get()->toArray();

		$liabilities_types=4;
		$liabilities_ledgers=Ledger::select('id')
							->where('type_id',$liabilities_types)->get()->toArray();

		//Get the cash records
		$cash_records=Brief::whereIn('ledger_id',$cash_ledgers)
							->whereBetween('transaction_date',array($first_date,$second_date))
							->where('description','Journal')
							->orwhere('description','Income')
							->where('transaction_type','Debit')
							->where('category','Deposit')
							->get();
		
		//Get the credit tranasctions
		$liabilities=Brief::whereIn('ledger_id',$liabilities_ledgers)
							->whereBetween('transaction_date',array($first_date,$second_date))
							->where('description','Journal')
							->where('transaction_type','Credit')
							->where('category','Deposit')
							->get();

		 
		//Display the form
		return view('invest.cash_form_res',compact('cash_records','liabilities','first_date','second_date'));
	}//end of the postform

	//Request to pay the matured investments
	public  function interests(){

		$date=Carbon::now('EAT');
		$date=$date->toDateTimeString();
		 

		$interests=Interest::where('paid_flag',0)
							->where('maturity_date','<',$date)
							->where('mature_flag',1)
							->orderBy('created_at')->get();

		return view('invest.interest_res',compact('interests'));

	}//end fo the getinterests table

	//Post function to receive the interest 
	public function postinterests(Request $request){
	 
		$interests=$request->get('interests');
		$transaction_date=Carbon::now('EAT');
		$financial_id=Financial::CurrentFinancial()->first()->id;
		$user_id=Auth::user()->id;
		$ledger_name=Ledger::Find(7)->name;
		
		foreach ($interests as $key => $value) {
			
			$interest=Interest::find($value);
			$amount=$interest->amount;
			$cust_id=$interest->cust_id;
			 
			$update=array(
				'paid_flag'=>1,
				'mature_flag'=>1
			);
			$interest->update($update);

			//Creating the corresponding journal entry
			$invest=Invest::create([
				'transaction_id'=>1
				]);
	
			$invest_id=$invest->id;
			$invest=invest::find($invest_id);
	
			$transaction=Transaction::create([
				'transaction_no'=>$invest_id,
				]);
	
			//Get the transaction of the inserted id
			$transaction_id=$transaction->id;
	
			//Update the invest id withe transaction number
			$periodical_update=array(
				'transaction_id'=>$transaction_id
				);
			$invest->update($periodical_update);
		
		   
			$receipt_id=$invest_id;
			$receipt_id=sprintf("%07d", $receipt_id);

			 //The transactions table
			Voucher::create([
				'amount'=>$amount,
				'transaction_no'=>$receipt_id,
				'transaction_id'=>$transaction_id,
				'transaction_type'=>'Journal',
				'transaction_date'=>$transaction_date,
				'description'=>'Investment',
				'ref'=>$transaction_id,
				'payees_name'=>'',
				'user_id'=>$user_id,
				'cust_id'=>$cust_id,
				'particulars'=>$ledger_name,
				'category'=>'Deposit',
				'credit'=>'1',
				'debit'=>'7',
				'posted_flag'=>'0',
				'financial_id'=>$financial_id,
				'deletion_flag'=>"",
				'code'=>''
				 ]);

			//Transfer the commission directly into the referral account
			Statement::create([
				'amount'=>$amount,
				'transaction_date'=>$transaction_date,
				'transaction_type'=>'Credit',
				'description'=>'Journal',
				'particulars'=>'Investment pays',
				'category'=>'Deposit',
				'invest_id'=>$invest_id,
				'ledger_id'=>7,
				'cust_id'=>$cust_id,
				'user_id'=>$user_id
			]);
	 
		}//end of the foreach statement

		Session::flash('message', 'Successfully posted interest earnings');
        return Redirect::back();
	}//end of thepostinterest table

	 		//Request to pay the matured investments
		public  function withdraws(){

			$withdraws=Withdraw::where('paid_flag',0)
								->orderBy('created_at')->get();
	
			return view('invest.withdraw_res',compact('withdraws'));
	
		}//end fo the getinterests table
	
		//Post function to receive the interest 
		public function postwithdraws(Request $request){
			
		$withdraw=$request->input('request');
		$transaction_date=Carbon::now('EAT');
		$financial_id=Financial::CurrentFinancial()->first()->id;
		$user_id=Auth::user()->id;
		$ledger_name=Ledger::Find(7)->name;
		
		
			$interest=Withdraw::find($withdraw);
			$amount=$interest->amount;
			$cust_id=$interest->cust_id;

			$update=array(
				'paid_flag'=>1
			);
			$interest->update($update);


			//Creating the corresponding journal entry
			$invest=Invest::create([
				'transaction_id'=>1,
				'type'=>'W'
				]);
	
			$invest_id=$invest->id;
			$invest=invest::find($invest_id);
	
			$transaction=Transaction::create([
				'transaction_no'=>$invest_id,
				]);
	
			//Get the transaction of the inserted id
			$transaction_id=$transaction->id;
	
			//Update the invest id withe transaction number
			$periodical_update=array(
				'transaction_id'=>$transaction_id
				);
			$invest->update($periodical_update);
		
			
			$receipt_id=$invest_id;
			$receipt_id=sprintf("%07d", $receipt_id);

				//The transactions table
			Voucher::create([
				'amount'=>$amount,
				'transaction_no'=>$receipt_id,
				'transaction_id'=>$transaction_id,
				'transaction_type'=>'Journal',
				'transaction_date'=>$transaction_date,
				'description'=>'Investment',
				'ref'=>$transaction_id,
				'payees_name'=>'',
				'user_id'=>$user_id,
				'cust_id'=>$cust_id,
				'particulars'=>$ledger_name,
				'category'=>'Deposit',
				'credit'=>'1',
				'debit'=>'7',
				'posted_flag'=>'0',
				'financial_id'=>$financial_id,
				'deletion_flag'=>"",
				'code'=>''
					]);

			//Transfer the commission directly into the referral account
			Statement::create([
				'amount'=>$amount,
				'transaction_date'=>$transaction_date,
				'transaction_type'=>'Debit',
				'description'=>'Journal',
				'particulars'=>'Withdraw',
				'category'=>'Withdraw',
				'invest_id'=>$invest_id,
				'ledger_id'=>7,
				'cust_id'=>$cust_id,
				'user_id'=>$user_id
			]);

			// //Debit the investment account
			// Brief::create([
			// 	'transaction_id'=>$transaction_id,
			// 	'posting_date'=>Carbon::now('EAT'),
			// 	'transaction_date'=>$transaction_date,
			// 	'transaction_type'=>'Debit',
			// 	'ledger_id'=>7,
			// 	'amount'=>$amount,
			// 	'category'=>'Withdraw',
			// 	'particulars'=>$ledger_name,
			// 	'description'=>'Journal'
			// ]);
			
			// $investment_ledger=Ledger::Find(7)->name;
			// //Credit the cash account
			// Brief::create([
			// 	'transaction_id'=>$transaction_id,
			// 	'posting_date'=>Carbon::now('EAT'),
			// 	'transaction_date'=>$transaction_date,
			// 	'transaction_type'=>'Credit',
			// 	'ledger_id'=>1,
			// 	'amount'=>$amount,
			// 	'category'=>'Withdraw',
			// 	'particulars'=>$investment_ledger,
			// 	'description'=>'Journal'
			// ]);
		
			
		Session::flash('message', 'Successfully posted withdraw amount');
		return Redirect::back();
	}//end of thepostinterest table

}
