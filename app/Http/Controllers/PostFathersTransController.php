<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Expense;
use App\Account;
use App\Invoice;
use App\Journal;
use App\Ledger;
use App\Tier;
use App\Brief;
use App\Entry;
use App\Financial;
use App\Sole;
use App\Periodical;
use App\Bank;
use App\Voucher;
use App\History;
use App\Post;
use App\User;
use App\Record;
use Carbon;
use Auth;
use DB;

class PostFathersExpTransController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		dd('found');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$deletion_flag = 1;
		$values = array($deletion_flag);
		$transactions_array=array();
		$not_posted_flag=0;
		$np_values=array($not_posted_flag);

		//Declare an array to hold the ledgers that are being posted
		$ledger_array=array();

		$journals=Periodical::all();

		$financial_id=Financial::CurrentFinancial()->first()->id;


DB::beginTransaction();
	 
		foreach ($journals as $key => $value)
			{
				   
				$voucher_id=Voucher::where('transaction_id','=',$value->transaction_id)->pluck('id');
				$voucher=Voucher::find($voucher_id);    
				$posted_update=array(
		        	'posted_flag'=>'1'
		        	);
		        $voucher->update($posted_update);



		        //Store the user records in the soles table for querying
		        //Create the record in the briefs table for the other accounts entry
		       	$soles=Sole::create([
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
 		            'amount'=>$value->transaction->voucher->amount,
 		            'user_id'=>$value->transaction->voucher->user_id,
 		            'financial_id'=>$value->transaction->voucher->financial_id
            	]);



		        $trans=Voucher::find($value->transaction->voucher->id);

		        $debit_acc=$trans->debit;

		        //Get the record with the debit account
		        $vouch_debit=Voucher::where('debit','=',$debit_acc)
		        						->where('transaction_id',$value->transaction_id)
		        						->first();


		       	//Create the record in the briefs table for the other accounts entry
		       	$briefs=Brief::create([
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Debit',
		            'ledger_id'=>$debit_acc,
		            'amount'=>$vouch_debit->amount
            	]);


            	$credit_acc=$trans->credit;

		        //Get the record with the debit account
		        $voucher_credit=Voucher::where('credit','=',$credit_acc)
		        						->where('transaction_id',$value->transaction_id)
		        						->first();


		       	//Create the record in the briefs table for the credit bank and cash
		       	$banks=Bank::create([ 
		            'transaction_id'=>$value->transaction_id,
		            'posting_date'=>Carbon::now('EAT'),
		            'transaction_date'=>$value->transaction->voucher->transaction_date,
		            'transaction_type'=>'Credit',
		            'ledger_id'=>$credit_acc,
		            'amount'=>$voucher_credit->amount
            	]);
		  			
 }
	 DB::commit();

	 DB::beginTransaction();
 			
 			//Get the transactions in the briefs table and do the totals
			$periodicals=Periodical::all();

			foreach ($journals as $key => $value) {
			

			 $transaction=Voucher::find($value->transaction->voucher->id);

		     $debit_acc=$trans->debit;

		     $transaction_date=$value->transaction->voucher->transaction_date;

		     //Get the record with the debit account
		     $trans=Brief::where('ledger_id','=',$debit_acc)
		        						->where('transaction_date','=',$transaction_date)
		        						->where('transaction_type','=','Debit')
		        						->sum('amount');
		     //Post to the histories table the summarized amounts   						
		     if($trans>0){
			     $history_record=History::create([
	        	'ledger_id'=>$debit_acc,
	           	'transaction_type'=> 'Debit',
	        	'amount'=>$trans,
	        	'transaction_date'=>$transaction_date,
	        	'posting_date'=>Carbon::now('EAT')
	        	]); 

			     $user = User::find(Auth::user()->id);
				$user= $user->histories()->save($history_record);
		     
		     }
		     $trans=Brief::where('ledger_id','=',$debit_acc)
		     				//->where('posting_date','=',Carbon::now('EAT'))
		        						->where('transaction_type','=','Credit')
		        						->sum('amount');
		     
		      //Post to the histories table the summarized amounts
		      if($trans>0){
		      	   $history_record=History::create([
	        	'ledger_id'=>$debit_acc,
	           	'transaction_type'=> 'Credit',
	        	'amount'=>$trans,
	        	'transaction_date'=>$transaction_date,
	        	'posting_date'=>Carbon::now('EAT')
	        	]); 

		      	  $user = User::find(Auth::user()->id);
				$user= $user->histories()->save($history_record);
		      }						


		      //Other account transaction 
		      $credit_acc=$transaction->credit;
		      $transaction_date=$value->transaction->voucher->transaction_date;

		     //Get the record with the debit account
		     $trans=Brief::where('ledger_id','=',$credit_acc)
		        						->where('transaction_date','=',$transaction_date)
		        						->where('transaction_type','=','Debit')
		        						->sum('amount');
		     //Post to the histories table the summarized amounts   						
		     if($trans>0){
			     $history_record=History::create([
	        	'ledger_id'=>$credit_acc,
	           	'transaction_type'=> 'Debit',
	        	'amount'=>$trans,
	        	'posting_date'=>Carbon::now('EAT'),
	        	'transaction_date'=>$transaction_date
	        	]); 
			      $user = User::find(Auth::user()->id);
				$user= $user->histories()->save($history_record);
		     
		     }
		     $trans=Brief::where('ledger_id','=',$credit_acc)
		        						->where('transaction_date','=',$transaction_date)
		        						->where('transaction_type','=','Credit')
		        						->sum('amount');
		     
		      //Post to the histories table the summarized amounts
		      if($trans>0){
		      	   $history_record=History::create([
	        	'ledger_id'=>$credit_acc,
	           	'transaction_type'=> 'Credit',
	        	'amount'=>$trans,
	        	'posting_date'=>Carbon::now('EAT'),
	        	'transaction_date'=>$transaction_date
	        	]); 
		      	    $user = User::find(Auth::user()->id);
				$user= $user->histories()->save($history_record);
		      }						

			//Delete each of the records in the periodicals table
			$periodical_delete=Periodical::find($value->id);
			$periodical_delete->delete();
			}
	DB::commit();

			// $deletion_flag = 1;

			// $values = array($deletion_flag);
			 
			$journals = Periodical::all();

			// $journals->setPath('journal');

			// //$journals->load('user','account');
		 
      		 return View('periodicals.index',compact('journals'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
