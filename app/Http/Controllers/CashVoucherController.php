<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Transaction;
use App\Ledger;
use App\Voucher;
use App\Periodical;
use App\Dozen;
use App\Financial;
use App\Interest;
use App\Invest;
use App\Payment;
use Carbon;
use Auth;
use DB;

class CashVoucherController extends Controller {


   public function __construct()
    {
        $this->middleware('auth');
        
    }


protected  $rules = [ 'amount' =>	 ['required','numeric'],];
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

    $posted_flag = 1;
    $deletion_flag=1;

    $values = array($posted_flag);
    $values2=array($deletion_flag);

		$vouchers = Voucher::whereIn('posted_flag', $values)
                            ->where('transaction_type','Expense')
                            ->whereNotin('deletion_flag',$values2)
                         ->orderBy('created_at', 'desc')->simplePaginate(50);

		$vouchers->setPath('vouchers');

		//$vouchers->load('user','account');

		 
       return View('vouchers.index',compact('vouchers'));
	
 	}

	/**
  	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		  //Get the accounts that have the income divison and sadaka votehead so that the user can select during entry
		$dozen_id=Auth::user()->dozen_id;
		$user_id=Auth::user()->id;
		$dozen=Dozen::find($dozen_id);
		$dozen_min=$dozen->min;
		$dozen_max=$dozen->max;
		$rate=$dozen->rate;
		$cust_id=Ledger::where('user_id',$user_id)->first()->id;
		//Check if the user has an active investment to reject
		if(Interest::where('cust_id',$cust_id)
					->where('paid_flag',0)->exists()){

			return view('vouchers.invest_exists');
		}

		return view('vouchers.create',compact('dozen_min','dozen_max','rate','dozen_id'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, $this->rules);

		DB::beginTransaction();

        
		$principal=$request->input('amount');
		$principal=$principal=(97/100) * $principal;
		$code=$request->input('code');

		if(Payment::where('code',$code)
					->where('reject_flag',0)
					->exists()){
			Session::flash('message', 'Invalid mpesa code');
			return Redirect::back()->withInput();
		}

		
		$ledger_id=7;
		$ledger=Ledger::find($ledger_id);
		$ledger_name=$ledger->name;

		//Get the user package
		$dozen_id=Auth::user()->dozen_id;
		$dozen=Dozen::find($dozen_id);
		$user_id=Auth::user()->id;
		$cust_id=Ledger::where('user_id',$user_id)->first()->id;
		$dozen_min=$dozen->min;
		$dozen_max=$dozen->max;
		$rate=$dozen->rate;
		$amount=(($rate/100)*$principal) + $principal;
		$interest=($rate/100)*$principal;

	
		//Create the payment record
		Payment::create([
			'code'=>$code,
			'type'=>'I',
			'user_id'=>$user_id,
			'cust_id'=>$cust_id
		]);

		
    	$transaction_date=Carbon::now('EAT');
				
         //Get the financial id
        $financial_id=Financial::CurrentFinancial()->first()->id;
        
        //Creating the corresponding journal entry
			 $periodical=Periodical::create([
				'transaction_id'=>1
				]);
	
			$periodial_id=$periodical->id;
			$periodical=Periodical::find($periodial_id);
	
			$transaction=Transaction::create([
				'transaction_no'=>$periodial_id,
				]);
	
			//Get the transaction of the inserted id
			$transaction_id=$transaction->id;
	
			//Update the periodical id withe transaction number
			$periodical_update=array(
				'transaction_id'=>$transaction_id
				);
			$periodical->update($periodical_update);
		
		   
			$receipt_id=$periodial_id;
			$receipt_id=sprintf("%07d", $receipt_id);
		 
		

		//The transactions table
        $voucher = Voucher::create([
			'amount'=>$interest,
			'transaction_no'=>$receipt_id,
			'transaction_id'=>$transaction_id,
			'transaction_type'=>'Expense',
			'transaction_date'=>$transaction_date,
			'description'=>'Interest',
			'ref'=>$transaction_id,
			'payees_name'=>Auth::user()->name,
			'user_id'=>Auth::user()->id,
			'cust_id'=>$cust_id,
			'particulars'=>$ledger_name,
			'category'=>'Drawing',
			'credit'=>'1',
			'debit'=>'4',
			'posted_flag'=>'0',
			'financial_id'=>$financial_id,
			'deletion_flag'=>"",
			'code'=>$code
			 ]);
			
			 //Creating the corresponding journal entry
			 $invest=Invest::create([
				'transaction_id'=>1,
				'type'=>'I'
				]);
	
			$invest_id=$invest->id;
			$invest=invest::find($invest_id);
	
			$transaction=Transaction::create([
				'transaction_no'=>$invest_id,
				]);
	
			//Get the transaction of the inserted id
			$transaction_id=$transaction->id;
	
			//Update the invest id withe transaction number
			$periodical_update=array(
				'transaction_id'=>$transaction_id
				);
			$invest->update($periodical_update);
		
		   
			$receipt_id=$periodial_id;
			$receipt_id=sprintf("%07d", $receipt_id);

			$maturity_date=Carbon::now('EAT')->addDays(1);
			 

			//Create the interests table
			$interests=Interest::create([
				'principal'=>$principal,
				'amount'=>$amount,
				'interest'=>$interest,
				'rate'=>$rate,
				'transaction_date'=>$transaction_date,
				'user_id'=>Auth::user()->id,
				'transaction_id'=>$transaction_id,
				'cust_id'=>$cust_id,
				'maturity_date'=>$maturity_date,
				'mature_flag'=>'0'
			]);

			 //The transactions table
			 $voucher = Voucher::create([
				'amount'=>$principal,
				'transaction_no'=>$receipt_id,
				'transaction_id'=>$transaction_id,
				'transaction_type'=>'Journal',
				'transaction_date'=>$transaction_date,
				'description'=>'Investment',
				'ref'=>$transaction_id,
				'payees_name'=>Auth::user()->name,
				'cust_id'=>$cust_id,
				'user_id'=>Auth::user()->id,
				'particulars'=>$ledger_name,
				'category'=>'Deposit',
				'credit'=>'7',
				'debit'=>'1',
				'posted_flag'=>'0',
				'financial_id'=>$financial_id,
				'deletion_flag'=>"",
				'code'=>$code
				 ]);
	 
 
        //$trans=Ledger::find($cash_account)->name;

	DB::commit();
		if($voucher)
		{
		
        Session::flash('message', 'Successfully investment, check your wallet');

		 $print='voucher'; 
		 $votehead='Investment';
		 $trans='Buy';

        return view ('vouchers.print_vouchers',compact('trans','print','voucher','receipt_id','votehead'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 
       

	}

}
