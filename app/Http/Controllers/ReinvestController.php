<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Invest;
use App\Interest;
use App\Dozen;
use App\Ledger;
use App\User;
use App\Payment;
use App\Voucher;
use App\Withdraw;
use App\Transaction;
use App\Statement;
use Carbon;
use Auth;
use DB;

class ReinvestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $cust_id=$request->input('record');
        $user_id=Ledger::where('id',$cust_id)->first()->user_id;
        DB::beginTransaction();
         
        //Check to ensure that they do have an active investment
        if(Interest::where('cust_id',$cust_id)
					->where('paid_flag',0)->exists()){
             Session::flash('message', 'Reinvestment failed. You have an active investment.');
			return redirect::back();
		}

        $credit=Statement::where('transaction_type','Credit')
							->where('cust_id',$cust_id)->get()->sum('amount');
		$debits=Statement::where('transaction_type','Debit')
                            ->where('cust_id',$cust_id)->get()->sum('amount');

        $withdraw_requests=Withdraw::where('cust_id',$cust_id)
							->where('paid_flag',0)
							->orderBy('created_at','Asc')
                            ->get()->sum('amount');
                            
		$balance=$credit-$debits-$withdraw_requests;
         
        if($balance < 0){
            Session::flash('message', 'Reinvestment failed. Your balance is less than 0.');
			return redirect::back();
        }
    
        $user=User::Find($user_id);
        $principal=$balance;
        $principal=(97/100)*$principal;
		$transaction_date=Carbon::now('EAT');
		$maturity_date=Carbon::now('EAT')->addDays(1);
		 

		//Get the user package
		$dozen_id=$user->dozen_id;
		$dozen=Dozen::find($dozen_id);
		$cust_id=Ledger::where('user_id',$user_id)->first()->id;
		$rate=$dozen->rate;
		$amount=(($rate/100)*$principal) + $principal;
		$interest=($rate/100)*$principal;

		$invest=Invest::create([
			'transaction_id'=>1,
			'type'=>'RIV'
			]);

		$invest_id=$invest->id;
		$invest=invest::find($invest_id);

		$transaction=Transaction::create([
			'transaction_no'=>$invest_id,
			]);

		//Get the transaction of the inserted id
		$transaction_id=$transaction->id;

		//Update the invest id withe transaction number
		$periodical_update=array(
			'transaction_id'=>$transaction_id
			);
		$invest->update($periodical_update);

		//Create the interests table
		$interests=Interest::create([
			'principal'=>$principal,
			'amount'=>$amount,
			'interest'=>$interest,
			'rate'=>$rate,
			'transaction_date'=>$transaction_date,
			'user_id'=>$user_id,
			'transaction_id'=>$transaction_id,
			'cust_id'=>$cust_id,
			'maturity_date'=>$maturity_date,
			'mature_flag'=>'1',
            'paid_flag'=>'0',
            'status'=>0
        ]);
        
        //Update the user statement
        //Transfer the commission directly into the referral account
		Statement::create([
            'amount'=>$balance,
            'transaction_date'=>$transaction_date,
            'transaction_type'=>'Debit',
            'description'=>'Journal',
            'particulars'=>'Reinvestment',
            'category'=>'Deposit',
            'invest_id'=>$invest_id,
            'ledger_id'=>7,
            'cust_id'=>$cust_id,
            'user_id'=>$user_id
			]);

		DB::commit();

		Session::flash('message', ' Successfully reinvested amount.');
		return redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         
        // DB::beginTransaction();
        // $interest_id=$id;
       
        // $interest=Interest::find($interest_id);   
        
        // $cust_id=$interest->cust_id;
        // //Check to ensure that they do have an active investment
        // if(Interest::where('cust_id',$cust_id)
		// 			->where('paid_flag',0)->exists()){
        //      Session::flash('message', 'Reinvestment failed. You have an active investment.');
		// 	return redirect::back();
		// }

        // $credit=Statement::where('transaction_type','Credit')
		// 					->where('cust_id',$cust_id)->get()->sum('amount');
		// $debits=Statement::where('transaction_type','Debit')
		// 					->where('cust_id',$cust_id)->get()->sum('amount');
		// $balance=$credit-$debits;
        // $interest_record=$interest;
        
        // $interst_update=array(
        //     'status'=>1
        // );
        // $interest->update($interst_update);
		// $user_id=$interest->user_id;
        // $user=User::Find($user_id);
        // $principal=$balance;
        // $principal=(97/100)*$principal;
		// $transaction_date=Carbon::now('EAT');
		// $maturity_date=Carbon::now('EAT')->addDays(1);
		// $cust_id=$interest->cust_id;

		// //Get the user package
		// $dozen_id=$user->dozen_id;
		// $dozen=Dozen::find($dozen_id);
		// $cust_id=Ledger::where('user_id',$user_id)->first()->id;
		// $rate=$dozen->rate;
		// $amount=(($rate/100)*$principal) + $principal;
		// $interest=($rate/100)*$principal;

		// $invest=Invest::create([
		// 	'transaction_id'=>1,
		// 	'type'=>'RIV'
		// 	]);

		// $invest_id=$invest->id;
		// $invest=invest::find($invest_id);

		// $transaction=Transaction::create([
		// 	'transaction_no'=>$invest_id,
		// 	]);

		// //Get the transaction of the inserted id
		// $transaction_id=$transaction->id;

		// //Update the invest id withe transaction number
		// $periodical_update=array(
		// 	'transaction_id'=>$transaction_id
		// 	);
		// $invest->update($periodical_update);

		// //Create the interests table
		// $interests=Interest::create([
		// 	'principal'=>$principal,
		// 	'amount'=>$amount,
		// 	'interest'=>$interest,
		// 	'rate'=>$rate,
		// 	'transaction_date'=>$transaction_date,
		// 	'user_id'=>$user_id,
		// 	'transaction_id'=>$transaction_id,
		// 	'cust_id'=>$cust_id,
		// 	'maturity_date'=>$maturity_date,
		// 	'mature_flag'=>'1',
        //     'paid_flag'=>'0',
        //     'status'=>0
        // ]);
        
        // //Update the user statement
        // //Transfer the commission directly into the referral account
		// Statement::create([
        //     'amount'=>$balance,
        //     'transaction_date'=>$transaction_date,
        //     'transaction_type'=>'Debit',
        //     'description'=>'Journal',
        //     'particulars'=>'Reinvestment',
        //     'category'=>'Deposit',
        //     'invest_id'=>$invest_id,
        //     'ledger_id'=>7,
        //     'cust_id'=>$cust_id,
        //     'user_id'=>$user_id
		// 	]);

		// DB::commit();

		// Session::flash('message', ' Successfully reinvested amount.');
		// return redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
