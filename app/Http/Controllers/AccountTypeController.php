<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Type;
use App\Category;
use App\Routine;
use App\Division;
use Redirect;
use Session;
use DB;
use Auth;
use App\User;

class AccountTypeController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
        
	}

protected  $rules = ['name' => ['required', 'min:3','unique:Types'],
					'description' => ['required'],
					//'code'=>['required','numeric'],	
					];

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		 $revenue_flag=1;
		 $expense_flag = 2;
		 $assets_flag=3;
		 $liablities_flag=4;

		// $values = array($expense_flag);
		$revenue=Type::where('division_id',$revenue_flag)->orderBy('created_at', 'desc')->get();
		$expenses=Type::where('division_id',$expense_flag)->orderBy('created_at', 'desc')->get();
		$assets=Type::where('division_id',$assets_flag)->orderBy('created_at', 'desc')->get();
		$liablities=Type::where('division_id',$liablities_flag)->orderBy('created_at', 'desc')->get();

		return view('account_types.index',compact('revenue','expenses','assets','liablities'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$system_categories=Category::lists('name','id');
		$sys_divisions=Division::pluck('name')->all();
		return view('account_types.create',compact('sys_divisions'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
		$this->validate($request, $this->rules);
		$fund_id=1;
	 
		$division_id=$request->input('division_id');


DB::beginTransaction();

        $type= new Type(array(
        	'name' =>$request->input('name'),
        	'description'=>$request->input('description'),
           	'user_id'=>Auth::user()->id
        	));

		$division = Division::find($division_id);

		$type = $division->types()->save($type);

DB::commit();

        if($type)
        {

       	Session::flash('message', 'Successfully created vote head');
        return Redirect::to('voteheads');
        }

        Session::flash('message','Failed to create record');
         return Redirect:: to('voteheads');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$account_type=Type::find($id);
       
        // $account_type=$subjects->toArray();
        return View ('account_types.show',compact('account_type'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)

	{

		$system_categories=Category::lists('name','id');
		$account_type = Type::find($id);

        // show the edit form and pass the nerd
        return View('account_types.edit',compact('account_type','system_categories'));
             
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{	
		$account_type = Type::find($id);

		 if(count($account_type->accounts))
		{
			
	        Session::flash('message',' Update failed, Vote Head has account Transactions');
	        return Redirect:: to('voteheads');
			
		}

	 $rules = ['name' => ['required', 'min:3','unique:Types,id,{$id}'],
					'description' => ['required'],	];

		$this->validate($request, $rules);
		//$account_type = new account_type();
        $account_type = Type::find($id);
        $account_type->update($request->all());

        
        // redirect
        Session::flash('message', 'Successfully updated vote head');
        return Redirect::to('voteheads');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		


		$account_type = Type::find($id);

		 if(count($account_type->accounts))
		{
			
	        Session::flash('message',' Deletion Failed Vote Head Has Account Transactions');
	        return Redirect:: to('voteheads');
			
		}

	 	else  
	 	{
	
	 		
	 		$account_type->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the vote head');
        return Redirect::to('voteheads');
	 				
		 
	 	}


		

		
       
	}

}
