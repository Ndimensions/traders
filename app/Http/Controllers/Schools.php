<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ledger;
use App\Brief;
use App\Interest;
use App\School;
use Session;
use Redirect;


use Illuminate\Http\Request;

class Schools extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
        

        
    }

protected  $rules = ['name' =>	 ['required', 'min:3'],
					 'mission' => ['required'],	];

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cash_type=1;
		$cash_ledgers=Ledger::select('id')
						->where('type_id',$cash_type)->get()->toArray();

        $cash_debits=Brief::whereIn('ledger_id',$cash_ledgers)
							->where('transaction_type','Debit')
							->get()->sum('amount');   

		$cash_credits=Brief::whereIn('ledger_id',$cash_ledgers)
							->where('transaction_type','Credit')
							->get()->sum('amount'); 

		$cash_balance=$cash_debits - $cash_credits;

		$liabilities=Interest::where('paid_flag',0)
								->get()->sum('principal');
		
		$school_info=School::first();
		
        return View ('school.index',compact('cash_balance','liabilities','school_info'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View('school.create');
	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request )
	{

        $this->validate($request, $this->rules);

         $School = School::create(array(
        	'name' =>$request->input('name'),
        	'address' => $request->input('address'),
        	'town' =>$request->input('town'),
        	'county' => $request->input('county'),
        	'email' => $request->input('email'),
        	'postal_code' =>$request->input('postal_code'),
        	'telephone' => $request->input('telephone'),
        	'mobile1' =>$request->input('mobile1'),
        	'mobile2' => $request->input('mobile2'),
        	'mission' =>$request->input('mission'),
        	'vision' => $request->input('vision'),
        	'core_values' =>$request->input('core_values'),
        	'other_description' => $request->input('other_description')
        	 
        	));
        
        if($School)
        {

       	Session::flash('message', 'Successfully created record');
        return Redirect::to('settings');
        }

        Session::flash('message','Failed to create record');
         return Redirect:: to('settings/create');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $school=school::find($id);
        
        // $subject=$subjects->toArray();
        return View ('school.show',compact('school'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
		// get the School
        $school_info = school::find($id);

        // show the edit form and pass the nerd
        return View('school.edit',compact('school_info'));
             
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{
		 
        $this->validate($request, $this->rules);
		
		$school = School::find($id);

		     // $school->name =$request->input('name');
       //  	 $school->address = $request->input('address');
       //  	 $school->town =$request->input('town');
       //  	 $school->county = $request->input('county');
       //  	 $school->postal_code =$request->input('postal_code');
       //  	 $school->telephone = $request->input('telephone');
       //  	 $school->mobile1 = $request->input('mobile1');
       //  	 $school->mobile2 = $request->input('mobile2');
       //  	 $school->mission =$request->input('mission');
       //  	 $school->vision = $request->input('vision');
       //  	 $school->core_values =$request->input('core_values');
       //  	 $school->other_description = $request->input('other_description');

       //  	 $school->save();
        
        $school->update($request->all());

        if($school)
        {

       	Session::flash('message', 'Successfully updated record');
        return Redirect::to('settings');
        }

        Session::flash('message','Failed to update record');
         return Redirect:: to('settings/'.$id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $school = School::find($id);
        $school->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the school!');
        return Redirect::to('schools');
	}

}
