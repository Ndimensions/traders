<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Carbon;
use Session;
use infobip\api\client\GetAccountBalance;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\client\SendSingleTextualSms;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest;
use Redirect;
use App\Payment;
use App\Message;
use DB;

class SMSCenterController extends Controller {

	//Get the balance of the account id
	public function getBalance(){

	// Initializing GetAccountBalance client with appropriate configuration
	$client = new GetAccountBalance(new BasicAuthConfiguration('AFYAYETU', 'se92b1Jz'));
	// Executing request
	$response = $client->execute();

	$accountBalance=$response->getBalance();
	$currency=$response->getCurrency();
	dd($accountBalance);
	return view('sms.balance_check',compact('accountBalance','currency'));


 		}


	//Handle the request to make a payment
	public function getPayments(){

		return view('sms.payment_entry');
	}

	//Hadndle the post of the payment entry
	public function postPayments(Request $request){

		$transaction_no;	//Mpesa transaction number

		$transaction_no=$request->input('transaction_no');
		$amount=$request->input('amount');

		//Ensure that the transaction number is not already entered.
		if (Payment::where('transaction_no', '=', $transaction_no)->exists()) {
   		// transaction found
			Session::flash('message', 'The transaction no is already entered.');
			return Redirect::back();
		}


		DB::beginTransaction();
		//Create the payments record
		$payments=Payment::create([
			'user_id'=>Auth::user()->id,
			'transaction_no'=>$transaction_no,
			'entry_date'=>Carbon::now('EAT'),
			'post_flag'=>0
			]);

		DB::commit();

		// Initializing SendSingleTextualSms client with appropriate configuration
		$client = new SendSingleTextualSms(new BasicAuthConfiguration('SMSCENTER1', '8YkTasf1'));

		// Creating request body
		$requestBody = new SMSTextualRequest();
		$requestBody->setFrom('AFYA_Y_CBHF');
		$requestBody->setTo(['254724332321']);
		$requestBody->setText("The following has been paid from AFYA YETU-". $amount. ':-'. $transaction_no);
 
		// Executing request
		try {
		    $response = $client->execute($requestBody);
		    $sentMessageInfo = $response->getMessages()[0];
		    // echo "Message ID: " . $sentMessageInfo->getMessageId() . "\n";
		    // echo "Receiver: " . $sentMessageInfo->getTo() . "\n";
		    // echo "Message status: " . $sentMessageInfo->getStatus()->getName();
		   // echo "Price".$sentMessageInfo->getPrice();
		} catch (Exception $exception) {
		    // echo "HTTP status code: " . $exception->getCode() . "\n";
		    // echo "Error message: " . $exception->getMessage();
		}

		// Session::flash('message', 'Awaiting verification.');
		// return Redirect::to('smscenter/search');


		Session::flash('message', 'Awaiting verification.');
		return Redirect::to('smscenter/search');

	}

	


	public function getSearch(){

		$payments=Payment::all();

		return view('sms.payment_index',compact('payments'));

	}

	//Get the history of the SMS Sent

	public function getHistory(){

		 return view('sms.search_history');
	}

	public function postHistory(Request $request){

		$date_one;	//First date to search in range
		$date_two;	//Second daate to search in range

		$date_one=$request->input('date_one');
		$date_two=$request->input('date_two');

		if($date_one>$date_two){
			Session::flash('message', 'The second date should be greater than the first date.');
			return Redirect::back()->withInput();
		}

		 //Convert the dates to YYYY-mm-dd
        $date_one=date('Y-m-d', strtotime($date_one));
        $date_two=date('Y-m-d', strtotime($date_two));

        //Search the SMS between the two dates
        $messages=Message::whereBetween('transaction_date',array($date_one,$date_two))
        					->get();
        $count=$messages->count();

        if($count == 0){
        		Session::flash('message', 'Messages not found.');
			return Redirect::back()->withInput();
        }

        return view('sms.messages_index',compact('messages'));

	}


	//Update payments by the seller
	public function getUpdates(){

		return view('sms.payment_update');

	}

	public function postUpdates(Request $request){
		
		$transaction_no;	//Get the Mpesa Transaction no
		$amount;			//Amount paid by the customer
		$post_flag;			//Ensures that the amount is paid and cleared

		$transaction_no=$request->input('transaction_no');
		$amount=$request->input('amount');

		//if the tranaction number does not exist reject
		if (!Payment::where('transaction_no', '=', $transaction_no)
					->where('post_flag','=',0)->exists()) {
   		// transaction found
			Session::flash('message', 'Transaction not found.');
			return Redirect::back();
		}

		//Ensure that the transaction number is not already entered.
		if (Payment::where('transaction_no', '=', $transaction_no)
					->where('post_flag','=',1)->exists()) {
   		// transaction found
			Session::flash('message', 'The amount is already posted.');
			return Redirect::back();
		}

		//Update the amount in the transaction details
		$payment=Payment::where('transaction_no','=',$transaction_no)->first();
		$payment_id=$payment->id;

		$update=array(
			'amount'=>$amount,
			'post_flag'=>1
			);

		$payment=Payment::find($payment_id);
		$payment->update($update);


		Session::flash('message', 'Successfully verified.');
		return Redirect::to('smscenter/search');
	}

}