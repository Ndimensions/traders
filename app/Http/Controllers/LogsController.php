<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon;
use Session;
use Redirect;
use App\User;
use App\Audit;

class LogsController extends Controller {

	//

	//Query by the recruiters giver
    public function getUsers(){

    	//Get all the system users
    	$sys_users=User::lists('user_name','id');
    	
	 	return view('logs.search_logs',compact('sys_users'));
    }


    public function postUsers(Request $request){
 			  
 		//Get the form input
		$user_id = $request->input('user_id');
			 
		//Get the names of the ids
		 
		  $audit=Audit::where('user_id',$user_id)->get();


	        //return ('students.search_res',compact('student'));
			if($audit->count()>0){
				 
				 $user=User::find($user_id);
       
		        // $subject=$subjects->toArray();
		        return View ('logs.user_logs',compact('audit','user'));			 

		    }else{

		    	//Get all the system users
		    	 Session::flash('message','User logs not found.');
		    	$sys_users=User::lists('user_name','id');
		    	
			 	return view('logs.search_logs',compact('sys_users'));
		 	}

		
         
	}

	//Query by the recruiters giver
    public function getDates(){

    	//Get all the system users
    	$sys_users=User::lists('user_name','id');
    	
	 	return view('logs.search_logs',compact('sys_users'));
    }


    public function postDates(Request $request){
 			  
 		//Get the form input
		$user_id = $request->input('user_id');
			 
		//Get the names of the ids
		 
		  $audit=Audit::where('user_id',$user_id)->get();


	        //return ('students.search_res',compact('student'));
			if($audit->count()>0){
				 
				 $user=User::find($user_id);
       
		        // $subject=$subjects->toArray();
		        return View ('logs.user_logs',compact('audit','user'));			 

		    }else{

		    	//Get all the system users
		    	 Session::flash('message','User logs not found.');
		    	$sys_users=User::lists('user_name','id');
		    	
			 	return view('logs.search_logs',compact('sys_users'));
		 	}

		
         
	}
}
