<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class RouteCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url=$request->path();
        $user_routes=array('registration/create','registration','home','cash/vouchers/create','invite','invite/usercommissions','invite/userinterests',
        'invite/userstatement','cash/vouchers');
        $user=Auth::user();
        $is_admin=$user->is_admin;
    
        if(($is_admin == 0) ){
            if(in_array($url, $user_routes)){
                return $next($request);
            }else{
                return redirect('home');
            }
        }else {
            return $next($request);
        }
        
    }
}
