<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financial extends Model
{
    protected $fillable=['name','begin_date','end_date','active'];


    public function scopeCurrentFinancial($query)
    {
         return $query->whereActive('1');
    }

}
