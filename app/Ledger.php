<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Ledger extends Model {

protected $fillable=['division_id','description','name','type_id','task_id','user_id'];

	use SoftDeletes;

	    protected $dates = ['deleted_at'];
	  
	//An account has a ledger entry
	public function account()
	{
		return $this->hasOne('App\Account');
	}

	//A ledger account could have many journal entries
	public function journals()
	{
		return $this->hasMany('App\Journal');
	}

	//Many ledger accounts could be in the trial table for trial balance at the end of day
	public function trials(){
		return $this->hasMany('App\Trial');
	}

	//A ledger has many records in the records table
	public function records()
	{
		return $this->hasMany('App\Record');
	}

	//A ledger account can be associated with many policies
	public function policies()
	{
		return $this->hasMany('App\Policy');
	}

	//A ledger has many entries in the secretaries table oi in the records table
	public function secretaries()
	{
		return $this->hasMany('App\Secretary');
	}

	//A ledger has many records in the records table
	public function items()
	{
		return $this->hasMany('App\Item');
	}

	//A ledger has many records in the notes table
	public function notes()
	{
		return $this->hasMany('App\Note');
	}

	//A ledger belongs to a type(Votehead)
	public function type()
	{
		return $this->belongsTo('App\Type');
	}

	//A ledger belongs to a Division
	public function division()
	{
		return $this->belongsTo('App\Division');
	}

	//A ledger account could have many brief entries
	public function briefs()
	{
		return $this->hasMany('App\Brief');
	}

	//A ledger account could have many bank entries
	public function banks()
	{
		return $this->hasMany('App\Brief');
	}

	//An account has many voucher transactions
	public function vouchers()
	{
		return $this->hasMany('App\Voucher');
	}


}
