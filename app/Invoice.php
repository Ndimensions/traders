<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['amount', 'user_id', 'status', 'ref'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
