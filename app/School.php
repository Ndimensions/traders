<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model {

	//
    protected $fillable = ['name', 'email', 'county',
                            'town','town_code','address',
                            'telephone','mobile1','mobile2',
                            'mission','vision','core_values','other_description'];


public function values()
{
return $this->hasMany('App\Value');
}
     
}

