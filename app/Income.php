<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Income extends Model {

	use SoftDeletes;

    protected $dates = ['deleted_at'];

	protected $fillable=['transaction_id'];
 
	//A periodical is associated with a transaction
	public function transaction()
	{
		return $this->belongsTo('App\Transaction');
	}

}
