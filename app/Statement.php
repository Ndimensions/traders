<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Statement extends Model {

	protected $dates = ['deleted_at'];

	protected $fillable=['ledger_id','amount','user_id','invest_id','transaction_date','transaction_type','description','category','particulars','cust_id'];

}
