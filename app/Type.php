<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Type extends Model {

	use SoftDeletes;

	    protected $dates = ['deleted_at'];

	//
	protected $fillable=['name','description','category_id','division_id','routine_id','user_id','fund_id'];


	
	//A funtion belongs to a fund
	public function fund()
	{
		return $this->belongsTo('App\Fund');
	}
	//A votehead type belongs to a  division
	public function division()
	{
		return $this->belongsTo('App\Division');
	}

 
	// A type has many ledgers
	public function ledgers()
	{
		return $this->hasMany('App\Ledger');
	}

	//A type belongs to a user
	public function user()
	{
		return $this->belongsTo('App\User');
	}
 

	 public function scopeName($query,$id)
 	{

 		return $query->whereId($id);
 	
 	}

 	// A type of account belongs to a category where it finances are withdrawn
 	public function category()
 	{
 		return $this->belongsTo('App\Category');
 	}


}
