@extends ('layouts.plane')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
            <div class="btn-toolbar">
               <h4><strong>Welcome</strong></h4> 
            </div>
        </div>
        <div class="panel-body">
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            <div class="form-group">
            </div>
            <button class="btn-info btn-lg"> Commissions {{ $commissions }}</button>
            <button  class="btn-success btn-lg">  Total Earnings {{ $investments}}</button>
            <button class="btn-warning btn-lg"> Account Balance {{ $balance }}</button>
            </hr>
        </div>
        <br></br>
        <h4><strong>News& Updates</strong></h4>
        <table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th>Number</th>
            <th>Date</th>
            <th>Title</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        
        @foreach($news as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->created_at }}</td>
                <td><strong>{{ $value->title }}</strong> </td>
                <td>{{ $value->description }}</td>
                        
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<p class="text-center">@</p>
</div>
@stop