@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
<h4 class="text-info">Daily Expense Report</h4>
 
   				@include('errors.error_partials')

@if (count($today_expense) === 0)
{{'No  Expenses Entered Today'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
            <td>Date</td>
            <td>Account ID</td>
            <td>Account Name</td>
            <td>Account Vote Head</td>
            <td>Description</td>
            <td>Comments</td>
             <td>Amount Paid</td>
             
            

        </tr>
        </thead>
        <tbody>
             <?php $count=0 ?>
          
        @foreach($today_expense as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                 <td>{{ $value->transaction_date }}</td>
                <td>{{ $value->account->id }}</td>
                <td>{{ $value->account->ledger->name }}</td>
                <td>{{ $value->account->type->name }}</td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->comments}}</td>
                <td>{{ $value->amount}}</td>

                 <?php $count+=$value->amount?>
                
                       
            </tr>
            
        @endforeach
        <tr>
            <td>TOTAL<td><td></td><td></td><td></td><td></td><td></td>
            <td>{{$count}}</td>

            <tr>
        </tbody>
    
            
         
        </tbody>
    </table>

</div>
<?php //echo $today_expense->render(); ?>
</div>
@endif
</div>
@stop
           
 