@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h3 class="text-info text-center">Deleted Fee Transaction Report</h3>
             <table  class="table table-striped">
                <tr>
                    <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td>  <h5>Second Date:    {{$second_date}}</h5></td>
                    <td> <h5>Total Amount:    {{$receipts}}</h5></td>

                </tr>
             </table>
             
   				@include('errors.error_partials')

@if (count($today_fee) === 0)
<h4 class="text-info text-center">{{'Student Fees Not Entered'}}</h4>
@else

 
    <hr/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Transaction date</th>
            <th>Admission Number</th>
            <th>Pupil Name</th>
            <th>Amount Paid</th>
            <th>Total Amount Paid</th>
            <th>Description</th>
            <th> Deleted by</th>
            <th> Deleted on</th>

        </tr>

        </thead>
        <tbody>
        <?php $count=0 ?>

        @foreach($today_fee as $key => $value)

            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date }}</td>
                <td>{{ $value->student->reg_no}}</td>
                <td>{{ $value->student->surname }} {{ $value->student->first_name }} {{ $value->student->last_name }}</td>
                <td>{{ $value->amount }}</td>
                <?php $count+=$value->amount?>
                <td>{{ $count}}</td>
                <td>{{ $value->description }}</td>
                <td>{{$value->user->first_name}} {{$value->user->last_name}}</td>
                <td>{{$value->created_at}}
                
                 
                
                  


                 
            </tr>
            
        @endforeach
        </tbody>
    </table>
</div>
<?php //echo $today_fee->render(); ?>
</div>
@endif
</div>
@stop