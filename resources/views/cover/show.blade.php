@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.school_menu')
        </div>

            </div>
                <div class="panel-body">
             <a class="btn btn-primary" href="{{URL::to('subjects/create')}}"> Create a Subject</a>
            <a class="btn btn-primary" href="{{URL::to('subjects')}}"> View Subjects</a>
            
               <h4>Displaying Subject</h4>

                @include('errors.error_partials')
        </nav>
        @if ( !$subject->count() )

            You have no projects
        @else

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Department</td>

                </tr>
                </thead>
                <tbody>
                 Showing
                    <tr>
                        <td>{{ $subject->id }}</td>
                        <td>{{ $subject->name }}</td>
                        <td>{{ $subject->department }}</td>


                        <!-- we will also add show, edit, and delete buttons -->
                        <td>

                            <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                            <!-- we will add this later since its a little more complicated than the other two buttons -->
                            
                            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                            <a class="btn btn-small btn-success" href="{{ URL::to('subjects/' . $subject->id) }}">Show this Subject</a>

                            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                            <a class="btn btn-small btn-info" href="{{ URL::to('subjects/' . $subject->id . '/edit') }}">Edit this Subject</a>

                        </td>
                    </tr>

                </tbody>
            </table>

</div>
</div>
</div>
    @endif
@stop