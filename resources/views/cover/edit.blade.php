@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.school_menu')
        </div>

            </div>
                <div class="panel-body">
        <a class="btn btn-primary" href="{{URL::to('subjects')}}"> View Subjects</a>

               <h4>Edit</h4>   
        
       @include('errors.error_partials')

        {{ Form::model($subject, array('route' => array('subjects.update', $subject->id), 'method' => 'PUT')) }}

       @include('subjects.edit_partials')

        {{ Form::submit('Edit the Subject!', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
 
</div>
</div>
</div>
@stop