@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
<h3 class="text-info text-center"> Vote Head Report</h3>
             <table  class="table table-striped">
                <tr>
                    <th><h5>First Date:   {{$first_date}}</h5></th>
                    <th>  <h5>Second Date:    {{$second_date}}</h5></th>
                    <th> <h5>Total Amount:    {{$total}}</h5></th>

                </tr>
             </table>
 
   				@include('errors.error_partials')

@if (count($acc_records) === 0)
{{'Expenses not entered.'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>             
            <td>Transaction Date</td>
            <td>Transaction No.</td>
             <td>Votehead</td>
             <td>Description</td>
            <td>Amount</td>
            <td>Total</td>
           

        </tr>
        </thead>
        <tbody>
             <?php $count=0 ?>
          
        @foreach($acc_records as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date}}</td>
                <td>{{ $value->transaction_no}}</td>
                <td>{{ $value->account->type->name}}</td>
                <td>{{ $value->description}}</td>
                <td>{{ $value->amount}}</td>

                <?php $count+=$value->amount ?>
                    <td>{{$count}}</td>                       
            </tr>
            
        @endforeach
        <tr>
            <th>TOTAL<th> <td></td><td></td><td></td><td></td>
            <th>{{$count}}</th>

            <tr>
        </tbody>
    
            
         
        </tbody>
    </table>

</div>
<?php //echo $today_expense->render(); ?>
</div>
@endif
</div>
@stop
           
 