@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h4 class="text-info">Daily Fee Report</h4>
			    
   				@include('errors.error_partials')

@if (count($today_fee) === 0)
<h4 class="text-info text-center">{{'Student Fees Not Entered'}}</h4>
@else

 
    <hr/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
             <th>Date</th>
             <th>Trans. no</th>
            <th>Trans .type</th>
            <th>Admission Number</th>
            <th>Pupil Name</th>
             <th>Description</th>
            <th>Amount Paid</th>
            <th>Total Amount Paid</th>
           
           

        </tr>

        </thead>
        <tbody>
        <?php $count=0 ?>

        @foreach($today_fee as $key => $value)

            <tr>
                <td>{{ $key+1 }}</td>
                  <td>{{ $value->transaction_date }}</td>
                  <td>{{ $value->transaction_no }}</td>
                  <td>{{ $value->transaction_type}}</td>

                <td>{{ $value->student->reg_no}}</td>
                <td>{{ $value->student->surname }} {{ $value->student->first_name }} {{ $value->student->last_name }}</td>
                 <td>{{ $value->description }}</td>
                <td>{{ $value->amount }}</td>
                <?php $count+=$value->amount?>
                <td>{{ $count}}</td>
                
                 
              
                


                 
            </tr>
            
        @endforeach
        </tbody>
    </table>
</div>
 
</div>
@endif
</div>
@stop