@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
<h3 class="text-info text-center"> Father's Vote Head Incomes Report</h3>
             <table  class="table table-striped">
                <tr>
                    <th><h5>First Date:   {{$first_date}}</h5></th>
                    <th>  <h5>Second Date:    {{$second_date}}</h5></th>                   
                    <th> <h5>Total Amount:    {{$total_expense}}</h5></th>

                </tr>
             </table>
 
   				@include('errors.error_partials')

@if (count($total_expense) === 0)
{{'No  Expenses Entered Today'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>             
            <th>Votehead Name</th>
            <th>Amount</th>
            <th>Total</td>
           

        </tr>
        </thead>
        <tbody>
             <?php $count=0 ?>
          
        @foreach($votehead_array as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value}}</td>
                <td>{{$votehead_totals[$key]}}
                    <?php $count+=$votehead_totals[$key] ?>
                    <td>{{$count}}</td>                       
            </tr>
            
        @endforeach
        <tr>
            <th>TOTAL<th> <td></td>
            <th>{{$count}}</th>

            <tr>
        </tbody>
    
            
         
        </tbody>
    </table>

</div>
<?php //echo $today_expense->render(); ?>
</div>
@endif
</div>
@stop
           
 