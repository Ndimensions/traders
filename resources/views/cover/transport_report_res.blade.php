@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
<h3 class="text-info text-center"> Transport Route Invoice Report</h3>
             <table  class="table table-striped">
                <tr>
                    <th><h5>First Date:   {{$first_date}}</h5></th>
                    <th>  <h5>Second Date:    {{$second_date}}</h5></th>
                    <th>  <h5>Route Name:    {{$route}}</h5></th>
                    <th> <h5>Total Amount:    {{$total}}</h5></th>

                </tr>
             </table>
 
   				@include('errors.error_partials')

@if (count($total) === 0)
{{'No results yet.'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td> 
            <td>Transaction Date</td>
            <td>Entered By</td>
            <td>Transaction No</td> 
            <td>Pupil</td>           
            <td>Point</td>
            <td>Amount</td>
            <td>Auto Invoice</td>

            <td>Total</td>
        </tr>
        </thead>
        <tbody>
             <?php $count=0 ?>
          
        @foreach($records as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                  <td>{{$value->fee->transaction_date}}</td>
                  <td>{{$value->fee->user->first_name}}  </td>
                <td>{{$value->fee->transaction_no}}</td>
                <td>{{$value->fee->student->surname}} {{"&nbsp;"}} {{$value->fee->student->first_name}} {{"&nbsp;"}} {{$value->fee->student->last_name}}</td>
                <td>{{$value->fee->comments}}  {{"&nbsp;"}} {{$value->fee->other_comments}} </td>
                 <td>{{$value->debit }}</td>    
                 <?php if($value->automatic_invoice < 1)
                    $status="No";
                    else
                    {
                    $status="Yes";
                    }
                    ?>
                <td>{{ $status}}</td>

                    <?php $count+=$value->debit ?>
                    <td>{{$count}}</td>                       
            </tr>
            
        @endforeach
        <tr>
            <th>TOTAL<th> <td></td><td></td><td></td><td></td><td></td><td></td>
            <th>{{$count}}</th>

            <tr>
        </tbody>
    
            
         
        </tbody>
    </table>

</div>
<?php //echo $today_expense->render(); ?>
</div>
@endif
</div>
@stop
           
 