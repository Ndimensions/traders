@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
<h3 class="text-info text-center">Income Report</h3>
             <table  class="table table-striped">
                <tr>
                    <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td>  <h5>Second Date:    {{$second_date}}</h5></td>
                    <td> <h5>Total Amount:    {{$receipts}}</h5></td>

                </tr>
             </table>
 
   				@include('errors.error_partials')

@if (count($range_expense) === 0)
{{'No  Expenses Entered Today'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
              <th>Date</th>
            <th>Account Name</th>
            <th>Vote Head Name</th>
            <th>Transaction No.</th>
            <th>Ref.</th>

             <th>Description</th>      
            <th>Particulars</th>        
             <th>Amount</th>
             <th>Total</th>
          
        </tr>
        </thead>
        <tbody>
             <?php $count=0 ?>
          
        @foreach($range_expense as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                 <td>{{ $value->transaction_date }}</td>
                 <td>{{ $value->ledger->name }}</td>
                <td>{{ $value->ledger->type->name }}</td>
                <td>{{ $value->transaction->voucher->ref }}</td>

                 <td>{{$value->transaction->voucher->transaction_no}}</td>
                <td>{{ $value->transaction->voucher->description }}</td>
                <td>{{ $value->transaction->voucher->particulars }}</td>
                <td>{{ $value->transaction->voucher->payees_name }}</td>


                  <td>{{ $value->amount}}</td>
                
                 <?php $count+=$value->amount?>
                 <td>{{$count}}</td>
                   </tr>       
        @endforeach
        <tr>
            <td>TOTAL<td><td></td><td></td><td></td><td></td></td><td></td><td></td><td></td>  <td></td>    
            <th>{{$count}}</th>

            <tr>
        </tbody>
    
            
         
        </tbody>
    </table>

</div>
<?php //echo $range_expense->render(); ?>
</div>
@endif
</div>
@stop
           
 