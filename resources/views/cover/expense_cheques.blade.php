@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h3 class="text-info text-center">Father's Expense Type Report</h3>
             <table  class="table table-striped">
                <tr>
                    <td><h5>Account:   {{$account}}</h5></td>
                    <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td>  <h5>Second Date:    {{$second_date}}</h5></td>
                    <td> <h5>Total Amount:    {{$receipts}}</h5></td>

                </tr>
             </table>
             
          
            
            

			    
   				@include('errors.error_partials')

@if (count($expenses) === 0)
<h4 class="text-info text-center">{{'Cheques not issues'}}</h4>
@else

 
    <hr/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
             <th>Transaction Date</th>
            <th>Transaction No.</th>
            <th>Ref</th>  
           <th>Description</th>             
           <th>Particulars</th>             
            <th>Payee's Name</th>       
            <th>Amount</th>
            <th>Totals</th>
           
          

        </tr>

        </thead>
        <tbody>
        <?php $count=0 ?>

        @foreach($expenses as $key => $value)

            <tr>
                <td>{{ $key+1 }}</td>
                
                <td>{{ $value->transaction_date }}</td>
                <td>{{ $value->voucher->transaction_no }}</td>  
                 <td>{{ $value->voucher->ref}}</td>        
                <td>{{ $value->voucher->description }}</td>
                <td>{{ $value->voucher->particulars}}</td>
                <td>{{ $value->payees_name }}</td>
                 <td>{{ $value->amount }}</td>
                <?php $count+=$value->amount?>
                <td>{{ $count}}</td>
                
                 
                 


                 
            </tr>
            
        @endforeach
        </tbody>
    </table>
</div>
<?php //echo $expenses->render(); ?>
</div>
@endif
</div>
@stop