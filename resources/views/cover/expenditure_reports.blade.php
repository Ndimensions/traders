@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">

			                         @include('sub_menu.fr_master_reports')



<hr/>
       <h4 class="info">Displaying Expenditure Reports</h4>
   				@include('errors.error_partials')

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
            <td>Name</td>
            <td>Description</td>

        </tr>
        </thead>
        <tbody>
          
        
           
           

             <tr>
                <td>{{ '1' }}</td>
                <td>{{ ' Expenses' }}</td>
                <td>{{ 'Define Range ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
            <a class="btn btn-small btn-success" href="{{ URL::to('expenditures') }}">Show</a>
                </td>
            </tr>

              

           
 


            <tr>
                <td>{{ '2' }}</td>
                <td>{{ ' Vote Head Balances' }}</td>
                <td>{{ 'Define Range ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success" href="{{URL::to('votehead/report')}}">Show</a>
                </td>
            </tr>

 
   
 
          
            <tr>
                <td>{{ '3' }}</td>
                <td>{{ 'Display all votehead expenses' }}</td>
                <td>{{ 'Define a range to show votehead expenses' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('votehead/expenses') }}">Show</a>
                </td>
            </tr>

       <!--      <tr>
                <td>{{ '4' }}</td>
                <td>{{ 'Display expenditure type report' }}</td>
                <td>{{ 'Define a range to show report' }}</td>


                we will also add show, edit, and delete buttons
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('expense/type/create') }}">Show</a>
                </td>
            </tr> -->

              <!-- <tr>
                <td>{{ '4' }}</td>
                <td>{{ 'Display Expenditure Cheques' }}</td>
                <td>{{ 'Define a range to show report' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <!-- <td>
                    
                <a class="btn btn-small btn-success"  href="{{ URL::to('cheque/expenses/create') }}">Show</a>
                </td> -->
            </tr> -->

 
  
        </tbody>
    </table>
</div>
</div>
</div>
@stop