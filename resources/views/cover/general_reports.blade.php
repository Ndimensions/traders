@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">

        @include('sub_menu.fr_master_reports')



<hr/>
       <h4 class="info">Displaying Income Reports</h4>


   				@include('errors.error_partials')

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
            <td>Name</td>
            <td>Description</td>

        </tr>
        </thead>
        <tbody>
               
              <tr>
                <td>{{ '1' }}</td>
                <td>{{ 'Financial Accounts' }}</td>
                <td>{{ 'Display System Accounts ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('system/accounts') }}">Show</a>
                </td>
            </tr>
              
        <!-- 
               <tr>
                <td>{{ '2' }}</td>
                <td>{{ 'Reports for reversed transactions' }}</td>
                <td>{{ 'Enter dates and transaction types' }}</td>
                we will also add show, edit, and delete buttons
                <td>
                    <a class="btn btn-small btn-success"  href="{{ URL::to('reversed/reports/create') }}">Show</a>
                </td>
            </tr> -->
               <tr>
                <td>{{ '2' }}</td>
                <td>{{ 'Trial Balance' }}</td>
                <td>{{ 'Enter dates' }}</td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <a class="btn btn-small btn-success"  href="{{ URL::to('tb/fathers') }}">Show</a>
                </td>
            </tr>
            <tr>
                <td>{{ '3' }}</td>
                <td>{{ 'General Ledger' }}</td>
                <td>{{ 'Enter dates' }}</td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <a class="btn btn-small btn-success"  href="{{ URL::to('gledger/fathers') }}">Show</a>
                </td>
            </tr>
 
 
        
        </tbody>
    </table>
</div>
</div>
</div>
@stop