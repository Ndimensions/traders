@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h3 class="text-info text-center"> End of Term Reports</h3>
             <table  class="table table-striped">
                <tr>
               
                    <td><h5>Transaction Type:   {{$user->first_name}} {{$user->last_name}}</h5></td>
                    <td><h5>Transaction Type:   {{$transaction_type}}</h5></td>
                  
                </tr>
             </table>
             
          
            
            		    
   				@include('errors.error_partials')

@if (count($tuition_array) === 0)
<h4 class="text-info text-center">{{'Student Fees Not Entered'}}</h4>
@else

 
    <hr/>
    <table id="testTable" class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Date</th> 
            <th>Tuition</th>
            <th>Boarding</th>
            <th>Transport</th>
            <th>Medical</th>
            <th>KCPE</th>
            <th>PEKits</th>
            <th>Interview</th>
            <th>Scouts</th>
            <th>Show</th>
            <th>Tour</th>
            <th>Report Book</th> 
            <th>Admission</th>
            <th>Registration</th>
            <th>Alliance Conference</th>
            <th>Hair</th>
            <th>Gown</th> 
             <th>Conference1</th> 
            <th>Total</th>        
           
        </tr>

        </thead>
        <tbody>
        <?php $count=0 ?>

        @foreach($transaction_date_array as $key => $value)

            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{$day_names_array[$key] }}{{$value}}</td>
                <td>{{$tuition_array[$key]}}</td>               
                <td>{{$boarding_array[$key]}}</td>
                <td>{{$transport_array[$key]}}</td>
                <td>{{$medical_array[$key]}}</td>
                <td>{{$kcpe_array[$key]}}</td>
                 <td>{{$pekits_array[$key]}}</td>
                   <td>{{$interview_array[$key]}}</td>
                   <td>{{$scouts_array[$key]}}</td>
                <td>{{$show_array[$key]}}</td>
                 <td>{{$tour_array[$key]}}</td>
                 <td>{{$reportbook_array[$key]}}</td>
                    <td>{{$admission_array[$key] }}</td>
                    <td>{{$registration_array[$key] }}</td>
                     <td>{{$allianceconference_array[$key] }}</td>
                 <td>{{$hair_array[$key]}}</td>
                  <td>{{$gown_array[$key]}}</td>
                     <td>{{$conference1_array[$key]}}</td>

        <td><?php  echo (($tuition_array[$key]+$boarding_array[$key]+$transport_array[$key]+$medical_array[$key]+$kcpe_array[$key]+
               $pekits_array[$key]+$interview_array[$key]+$scouts_array[$key]+$show_array[$key]+$tour_array[$key]+$reportbook_array[$key]+
                $admission_array[$key]+$registration_array[$key]+$allianceconference_array[$key]+$hair_array[$key]+$gown_array[$key])); ?></td>      
            </tr>
            
        @endforeach
        <tr>
            <th>Total</th><th></th>
            <th><?php echo array_sum($tuition_array); ?></th>
            <th><?php echo array_sum($boarding_array); ?></th>
            <th><?php echo array_sum($transport_array); ?></th>
            <th><?php echo array_sum($medical_array); ?></th>
            <th><?php echo array_sum($kcpe_array); ?></th>
            <th><?php echo array_sum($pekits_array); ?></th>
            <th><?php echo array_sum($interview_array); ?></th>
            <th><?php echo array_sum($scouts_array); ?></th>
            <th><?php echo array_sum($show_array); ?></th>
            <th><?php echo array_sum($tour_array); ?></th>
            <th><?php echo array_sum($reportbook_array); ?></th>
             <th><?php echo array_sum($admission_array); ?></th>
            <th><?php echo array_sum($registration_array); ?></th>
            <th><?php echo array_sum($allianceconference_array); ?></th>
            <th><?php echo array_sum($hair_array); ?></th>
            <th><?php echo array_sum($gown_array); ?></th>
               <th><?php echo array_sum($conference1_array); ?></th>
             
            <th><?php
echo ((array_sum($tuition_array)+array_sum($boarding_array)+array_sum($transport_array)+array_sum($medical_array)+
array_sum($kcpe_array)+array_sum($pekits_array)+array_sum($interview_array)+array_sum($show_array)+array_sum($tour_array)+array_sum($admission_array)+
array_sum($reportbook_array)+array_sum($registration_array)+array_sum($allianceconference_array)+array_sum($hair_array)+array_sum($gown_array) +array_sum($conference1_array)));

                    ?>
            </th>

        </tr>
     
        </tbody>
    </table>
</div>
<?php //echo $today_fee->render(); ?>
</div>
<input type="button" onclick="tableToExcel('testTable', 'W3C Example Table')" value="Export to Excel">

    <script type="text/javascript">
        var tableToExcel = (function() {
var uri = 'data:application/vnd.ms-excel;base64,'
, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
return function(table, name) {
if (!table.nodeType) table = document.getElementById(table)
var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
window.location.href = uri + base64(format(template, ctx))
}
})()
  
  </script>
@endif

</div>
@stop