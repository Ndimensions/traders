@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
                

        @include('errors.error_partials')
        </nav>
        
        <h4>Displaying Income Account Invoice</h4>
           
        

            <table class="table table-hover table-striped table-bordered  ">
                <thead>
                    <tr>
                        <th class='text-info'>Account Name</th>
                          

                    </tr>
                     <tr> 
                        <th>{{$account->name}}</th>
                         <th>{{$total}}</th>
                    </tr>
                    

                <tr>
                </thead>
            </table>

        @if ( !$account_transactions->count() )
        <br></br>
        <div class='flash alert-info'>
            <p class="text-center">Account does not have transactions</p>       
        </div>
        @else
                    

            <table class="table table-hover table-striped table-bordered  ">
                <thead>
                    <td>Number</td>
                    <td>Transaction Date</td>           
                    <td>Particulars</td>
                     <td>Amount</td>
                      <td>Total</td>
                                         
                </tr>
                </thead>
                <tbody>
                     <?php $count=0; ?>
               @foreach($account_transactions as $key=>$value)
                                         <tr>
                          
                        <td>{{ $key+1}}</td>       
                        <td>{{$value->transaction_date}} </td>               
                        <td>{{ $value->particulars }}</td>
                        <td>{{$value->credit}}</td>
                        <td>{{$count+=$value->debit}}</td>

                    </tr>
                @endforeach
                <tr>
                    
                </tr>
                
                </tbody>
            </table>
            @endif

</div>
</div>
</div>
    
@stop