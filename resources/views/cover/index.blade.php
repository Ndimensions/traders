@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">

			   <p>Displaying Available Reports</p>

   				@include('errors.error_partials')

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
            <td>Name</td>
            <td>Description</td>

        </tr>
        </thead>
        <tbody>
          
        
            
            <tr>
                <td>{{ '1' }}</td>
                <td>{{ 'Today Fees' }}</td>
                <td>{{ 'Display the closing balance of student fees' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
            <a class="btn btn-small btn-success" href="{{ URL::to('todays/receipts/') }}">Show</a>
                </td>
            </tr>
 

              

            <tr>
                <td>{{ '2' }}</td>
                <td>{{ ' Student Receipt Fees' }}</td>
                <td>{{ 'Define Range ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
   <a class="btn btn-small btn-success" href="{{ URL::to('bill/create/') }}">Show</a>
                </td>
            </tr>

              <tr>
                <td>{{ '3' }}</td>
                <td>{{ ' Student Deposit Slip Amounts' }}</td>
                <td>{{ 'Define Range ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
   <a class="btn btn-small btn-success" href="{{ URL::to('search/slips') }}">Show</a>
                </td>
            </tr>


          

              <tr>
                <td>{{ '4' }}</td>
                <td>{{ ' Filter Balances' }}</td>
                <td>{{ 'Define Range ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('filter/class') }}">Show</a>
                </td>
            </tr>

            


            <tr>
                <td>{{ '5' }}</td>
                <td>{{ 'Class Pocket Money Balances' }}</td>
                <td>{{ 'Select Class ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('class/pocketmoney') }}">Show</a>
                </td>
            </tr

             <tr>
                <td>{{ '6' }}</td>
                <td>{{ ' Pocket Money Balances' }}</td>
                <td>{{ 'Define Range ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('range/pocketmoney') }}">Show</a>
                </td>
            </tr>

              <tr>
                <td>{{ '7' }}</td>
                <td>{{ 'Financial Policy Balances' }}</td>
                <td>{{ 'Select Policy' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('policy/balances') }}">Quick view</a>
                </td>
            </tr>

             <tr>
                <td>{{ '8' }}</td>
                <td>{{ 'Class Balances' }}</td>
                <td>{{ 'Select Class' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
<!--<a class="btn btn-small btn-success"  href="{{ URL::to('class/balances') }}">Show</a>
-->
<a class="btn btn-small btn-success"  href="{{ URL::to('class/ver') }}">Show</a>
                </td>
            </tr>

            <tr>
                <td>{{ '9' }}</td>
                <td>{{ 'Search Student Transactions' }}</td>
                <td>{{ 'Select transaction type' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('transaction/types') }}">Show</a>
                </td>
            </tr>     

            <tr>
                <td>{{ '10' }}</td>
                <td>{{ 'Search Student Transactions' }}</td>
                <td>{{ 'Enter transaction number' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('transno') }}">Show</a>
                </td>
            </tr>    
            <tr>
                <td>{{ '11' }}</td>
                <td>{{ 'Secretary Fees Report' }}</td>
                <td>{{ 'Enter dates ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('mytransactions/dates') }}">Show</a>
                </td>
            </tr> 

             <tr>
                <td>{{ '12' }}</td>
                <td>{{ 'Secretary Fees Report' }}</td>
                <td>{{ 'Enter dates and  transaction type' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('mytransactions') }}">Show</a>
                </td>
            </tr> 

              <tr>
                <td>{{ '13' }}</td>
                <td>{{ 'Transferred Pupils Report' }}</td>
                <td>{{ 'Shows balances and over payments ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('transfers') }}">Show</a>
                </td>
            </tr>

              <tr>
                <td>{{ '14' }}</td>
                <td>{{ 'Display Secretary monthly reports' }}</td>
                <td>{{ 'Select secretary and month ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('eot/secretary/reports/create') }}">Ok</a>
                </td>
            </tr>
              <tr>
                <td>{{ '15' }}</td>
                <td>{{ 'Create Transport Routes' }}</td>
                <td>{{ 'Enter Routes' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('set/routes') }}">Ok</a>
                </td>
            </tr>

            <tr>
                <td>{{ '16' }}</td>
                <td>{{ 'Transport Route Invoice Reports' }}</td>
                <td>{{ 'Enter dates and select routes' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('trp/reports/create') }}">Ok</a>
                </td>
            </tr>
            

            <tr>
                <td>{{ '17' }}</td>
                <td>{{ 'Automatic Student Invoice' }}</td>
                <td>{{ 'Select Month' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('trp/months/create') }}">Ok</a>
                </td>
            </tr>
             <tr>
                <td>{{ '18' }}</td>
                <td>{{ 'Income Account Transactions ' }}</td>
                <td>{{ 'Enter dates and select income account' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('incomes/rpt/create') }}">Ok</a>
                </td>
            </tr>
        
        </tbody>
    </table>
</div>
</div>
</div>
@stop