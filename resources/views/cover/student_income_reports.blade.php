@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h3 class="text-info text-center"> Transaction Type Report</h3>
             <table  class="table table-striped">
                <tr>
                    <td><h5>Transaction Type:   {{$user->first_name}} {{$user->last_name}}</h5></td>
                    
                    <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td>  <h5>Second Date:    {{$second_date}}</h5></td>
               

                </tr>
             </table>
                      
            
            		    
   				@include('errors.error_partials')

@if (count($today_fee) === 0)
<h4 class="text-info text-center">{{'Student Fees Not Entered'}}</h4>
@else

 
    <hr/>
    <table class="table table-striped table-bordered">
        <thead>
           

            <tr>
                @foreach($votehead_arrays as $key => $value)
               <th>{{$value}}</th>               
                  @endforeach  
                  <th>Total</th>             
            </tr>

               
     
        </thead>
        <tbody>
        <?php $count=0 ?>       


        
    </tr>
    @foreach($Tuition as $key => $value)
               <th>{{$value}}</th>        
                 @endforeach  
                 <th><?php echo array_sum($Tuition);?></th>
               </tr>       
                           
         
        </tbody>
    </table>
</div>
<?php //echo $today_fee->render(); ?>
</div>
@endif
</div>
@stop