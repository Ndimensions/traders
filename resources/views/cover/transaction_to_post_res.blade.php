@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h3 class="text-info text-center"> Transaction: {{$transaction_type}}</h3>
             <table  class="table table-striped">
                <tr>
                   
                    <td><h5>Date:   {{$date}}</h5></td>
                     <td> <h5>Total Amount:    {{$receipts}}</h5></td>

                </tr>
             </table>	

     


   				@include('errors.error_partials')

@if (count($today_fee) === 0)
<h4 class="text-info text-center">{{'Student Fees Not Entered'}}</h4>
@else

 
    <hr/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Receipt Date</th>
            <th>Slip Date</th>
            <th>Slip Bank</th>
            <th>Admission Number</th>
            <th>Class Name</th>
            <th>Pupil Name</th>
            <th>Trans No.</th>
            <th>Description</th>  
            <th>Posted</th>           
            <th>Amount</th>
            <th>Totals</th>
           

        </tr>

        </thead>
        <tbody>
        <?php $count=0 ?>

        @foreach($today_fee as $key => $value)

            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date }}</td>
                <td>{{ $value->slip_date }}</td>
                <td>{{ $value->slip_bank }}</td>
                <td>{{ $value->student->reg_no}}</td>
                <td>{{ $value->student->surname }} {{ $value->student->first_name }} {{ $value->student->last_name }}</td>
                   <td>{{ $value->student->grade->name }}</td>
                <td>{{ $value->transaction_no }}</td>
                <td>{{ $value->description }}</td>
                 <?php if($value->post_flag == 1)
                    $status="Yes";
                    else
                    {
                    $status="No";
                    }
                    ?>
                <td>{{$status}}</td>
                <td>{{ $value->amount }}</td>
                <?php $count+=$value->amount?>
                <td>{{ $count}}</td>
                                               
            </tr>
            
        @endforeach
        </tbody>
    </table>
    <h4 align="center"><a href="#" class="btn btn-small btn-success align:center"
                            data-toggle="modal"
                            data-target="#basicModal"
                            >Post Transactions</a></h4>

     <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Confirm Transactions To Post</h4>
                          </div>

                          <div class="modal-body">
                               Are You Sure You Sure You Want To Post Transactions?
                                
                                {{ Form::open(array('url' => 'posting')) }}
                                
                                   {{Form::hidden('date',$date)}}
                                     {{Form::hidden('transaction_type',$transaction_type)}}  
                                       
                          </div>
                          <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    {{ Form::submit('Post ') }}
                          </div>
                                 {{ Form::close() }}
                    </div>
              </div>
    </div>
</div>
<?php //echo $today_fee->render(); ?>
</div>
@endif
</div>
@stop