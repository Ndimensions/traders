@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h3 class="text-info text-center"> Income Account Transactions</h3>
             <table  class="table table-striped">
                <tr>
                    <td><h5>Account Name:   {{$account_name}}</h5></td>
 
                    <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td> <h5>Second Date:    {{$second_date}}</h5></td>
 
                </tr>
             </table>               
                @include('errors.error_partials')

@if (count($trans) === 0)
<h4 class="text-info text-center">{{'Account does not have transactions.'}}</h4>
@else

 
    <hr/>
    <table id="Stds" class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Receipt Date</th>
            <th>Trans No.</th>
             
             <th>Ref.</th>

             <th>Description</th>      
            <th>Particulars</th>   
             <th>Received</th>            
            <th>Amount</th>
            <th>Totals</th>
           
          

        </tr>

        </thead>
        <tbody>
        <?php $count=0 ?>

        @foreach($trans as $key => $value)

            <tr>
                <td>{{ $key+1 }}</td>
                 <td>{{$value->transaction->voucher->transaction_date}}</td>
                <td>{{$value->transaction->voucher->transaction_no}}</td>
                 <td>{{$value->transaction->voucher->ref}}</td>
                 <td>{{ $value->transaction->voucher->description }}</td>
                <td>{{ $value->transaction->voucher->particulars }}</td>
                <td>{{ $value->transaction->voucher->payees_name }}</td>
 

                <td>{{$value->transaction->voucher->amount}}</td>

            <?php $count+=$value->transaction->voucher->amount;  ?>
                <td>{{ $count}}</td>
                            
                               
            </tr>
            
        @endforeach
        </tbody>
    </table>
</div>

<input type="button" onclick="tableToExcel('Stds', 'Students Table')" value="Export to Excel">

    <script type="text/javascript">
        var tableToExcel = (function() {
var uri = 'data:application/vnd.ms-excel;base64,'
, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
return function(table, name) {
if (!table.nodeType) table = document.getElementById(table)
var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
window.location.href = uri + base64(format(template, ctx))
}
})()
  
  </script></div>
@endif
</div>
@stop