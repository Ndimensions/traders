@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h3 class="text-info text-center"> Transaction Type Report</h3>
             <table  class="table table-striped">
                <tr>
                         <td><h5>Class:   {{$grade_name}}</h5></td>
                    <td><h5>Transaction Type:   {{$transaction_type}}</h5></td>
                    <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td>  <h5>Second Date:    {{$second_date}}</h5></td>
                    <td> <h5>Total Amount:    {{$result_sum}}</h5></td>

                </tr>
             </table>
             
          
            
            

			    
   				@include('errors.error_partials')

@if (count($result_sum) === 0)
<h4 class="text-info text-center">{{'Student Fees Not Entered'}}</h4>
@else

 
    <hr/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Receipt Date</th>
            <th>Slip Date</th>
            <th>Slip Bank</th>
            <th>Admission Number</th>
            <th>Pupil Name</th>
            <th>Trans No.</th>
            <th>Description</th>             
            <th>Amount</th>
            <th>Totals</th>
           
          

        </tr>

        </thead>
        <tbody>
        <?php $count=0 ?>
 

        @foreach($today_fee as $key => $value)

            <tr>
                  
                 <td>{{ $key+1 }}</td>
      
                 
                
                              
            </tr>
            
        @endforeach
        </tbody>
    </table>
</div>
<?php //echo $today_fee->render(); ?>
</div>
@endif
</div>
@stop