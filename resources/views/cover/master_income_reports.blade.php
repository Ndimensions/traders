@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">

         @include('sub_menu.fr_master_reports')



<hr/>
       <h4 class="info">Displaying Income Reports</h4>


   				@include('errors.error_partials')

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
            <td>Name</td>
            <td>Description</td>

        </tr>
        </thead>
        <tbody>
            
              <tr>
                <td>{{ '1' }}</td>
                <td>{{ 'Income Reports' }}</td>
                <td>{{ 'Define Range' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('incomerange/reports') }}">Show</a>
                </td>
            </tr>

              <tr>
                <td>{{ '2' }}</td>
                <td>{{ ' Vote Head Incomes' }}</td>
                <td>{{ 'Define Range ' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success" href="{{URL::to('frvotehead/report')}}">Show</a>
                </td>
            </tr>

              <tr>
                <td>{{ '3' }}</td>
                <td>{{ 'Display all votehead incomes' }}</td>
                <td>{{ 'Define a range to show votehead incomes' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('voteheadincomes') }}">Show</a>
                </td>
            </tr>

<!-- 
                  <tr>
                <td>{{ '4' }}</td>
                <td>{{ 'Search Secretary Income Reports' }}</td>
                <td>{{ 'Select Secretary' }}</td>


                we will also add show, edit, and delete buttons
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('secretary/reports') }}">Show</a>
                </td>
            </tr>
                 -->
 

              <tr>
                <td>{{ '4' }}</td>
                <td>{{ 'Income Account Transactions ' }}</td>
                <td>{{ 'Enter dates and select income account' }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    
<a class="btn btn-small btn-success"  href="{{ URL::to('incomes/rpt/create') }}">Show</a>
                </td>
            </tr>
        
        </tbody>
    </table>
</div>
</div>
</div>
@stop