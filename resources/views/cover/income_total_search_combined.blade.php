@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
                

        @include('errors.error_partials')
        </nav>
        
        <h4>Displaying Income Account Receipts</h4>
           
        

            <table class="table table-hover table-striped table-bordered  ">
                <thead>
                    <tr>
                        <th class='text-info'>Account Name</th>
                          

                    </tr>
                     <tr> 
                            <td>First Date {{$first_date}}</td>
                              <td>Second Date {{$second_date}}</td>
                    </tr>
                    <Tr>
                        <td>{{$transaction_type}}</td>
                    </tR>
                    

                <tr>
                </thead>
            </table>

        @if ( !$account_transactions->count() )
        <br></br>
        <div class='flash alert-info'>
            <p class="text-center">Account does not have transactions</p>       
        </div>
        @else
        
                    

            <table class="table table-hover table-striped table-bordered  ">
                <th 
                                         
                </tr>
                </thead>
                <tbody>
                     <?php $count=0; ?>
<tr>
   
                    @foreach($acc_names_array as $key=>$name)
                    
                        <th>{{$name}}</th>
                   
                    @endforeach
                     </tr>
                     <?php $count=0; ?>
                     <tr>
                      @foreach($account_total as  $ky=>$name)
                    
                        <th>{{$account_total[$ky]}}</th>
                        <?php $count+=$account_total[$ky]?>
                   
                    @endforeach
                     
                    
                </tr>
                <tr>
                    <th>Total</th>
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td></td>
                          <td></td>
                            <td></td>
                              <td></td>
                                <td></td>
                                  <td></td>

                    <td>{{$count}}</td>
                </tr>
                
                </tbody>
            </table>
            @endif

</div>
</div>
</div>
    
@stop