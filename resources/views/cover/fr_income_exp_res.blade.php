<?php namespace App\Http\Controllers;
use URL;
use Form; ?>
@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>
            </div>
            <div class="panel-body">
             <h3 class="text-info text-center">Income & Expenditure Report </h3>
             <h4 class="text-info text-center">Period Between: {{$first_date}} and {{$second_date}}</h4>
           
             @include('errors.error_partials')

            @if ((count($range_expense) === 0) && (count($range_income) === 0) )
            {{'No  Expenses Entered Today'}}
            @else
            <table class="table table-striped table-bordered">
                <thead>
                        <tr><th><strong><caption>Incomes</caption></strong></th></tr>
                <tr>
                    <th>Number</th>
                    <th>Date</th>
                    <th>Vote Head Name</th>
                    <th>Account Name</th>          
                    <th>Amount</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                    <?php $count_inc=0 ?>
                @foreach($range_income as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $value->transaction_date }}</td>
                        <td>{{ $value->ledger->type->name }}</td>
                        <td>{{ $value->ledger->name }}</td>
                        <?php $income_account_sum=FrIncomeExpenditureController::create($value->ledger_id,$first_date, $second_date); ?>
                        <td>{{$income_account_sum}}</td>
                        <?php $count_inc+=$income_account_sum?>   
                    </tr>
                @endforeach
                <tr>
                    <th>TOTAL<th><td></td><td></td><td></td>   
                    <th>{{$count_inc}}</th>
                </tr>
                <tr><th>Expenses</th></tr>
                    <tr>
                    <?php $count_exp=0 ?>
                @foreach($range_expense as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $value->transaction_date }}</td>
                        <td>{{ $value->ledger->type->name }}</td>
                        <td>{{ $value->ledger->name }}</td>
                        <?php $expense_account_sum=FrIncomeExpenditureController::show($value->ledger_id,$first_date, $second_date); ?>
                        <td>{{$expense_account_sum}}</td>                        
                        <?php $count_exp+=$expense_account_sum?>  
                    </tr>
                @endforeach
                <tr>
                    <th>TOTAL<th><td></td><td></td><td></td>   
                    <th>{{$count_exp}}</th>
                    </tr>
                    <tr><td><td></td><td></td><td></td><td></td></tr>
                    <tr>
                    <th>BALANCE<th><td></td><td></td><td></td>
                    <th>{{$count_inc-$count_exp}}</th>
                    </tr>
                </tbody>
                </tbody>
            </table>
</div>
<?php //echo $range_expense->render(); ?>
</div>
@endif
</div>
@stop
           
 