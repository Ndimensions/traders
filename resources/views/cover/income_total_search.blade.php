@extends ('layouts.plane')
@section('page_heading','School Information')

@section('body')
@include('menu.main_menu');

<div class="container">
        <div class="panel panel-default">
            
            <div class="panel-heading">   
                <p></p>
                <div class="btn-toolbar">
                    @include('menu.fnc_menu')
                </div>
            </div>

        <div class="panel-body">
            <p class="text-info">Enter date, select transaction type and Income Account </p>
               
            {{ Form::open(array('url' => 'ledgertot') ) }}
                
            <form role="form">
            @include('errors.error_partials')
                  
         <div class="form-group">
	         <label class="col-md-4 control-label">First Date</label>
	         <div class="col-md-6">
	        	{{ Form::text('first_date', null , array('class' => 'form-control col-md-3','placeholder'=>'YYYY-MM-DD')) }} 
	         </div>

        </div>

				<br> <br/><br> <br/>
         
         <div class="form-group">
            <label class="col-md-4 control-label">Second Date</label>
            <div class="col-md-6">
                	{{ Form::text('second_date', null , array('class' => 'form-control col-md-3','placeholder'=>'YYYY-MM-DD')) }} 
            </div>
        </div>

         <br></br><br></br>

          <div class="form-group">
            <label class="col-md-4 control-label">Transaction Type</label>
            <div class="col-md-6">
                {{ Form::select('transaction_type', array(''=>'Please Select Tran. Type','RECEIPT' => 'RECEIPT','INVOICE'=>'INVOICE',
                                                                        'CREDIT NOTE'=>'CREDIT NOTE','ADJUSTMENT'=>'ADJUSTMENT')) }} 
                 
            </div>
        </div>

         <br></br><br></br>

      <div class="form-group">
            <label class="col-md-4 control-label">Income Account</label>
            <div class="col-md-6">
                
                 <select name="account" class="form-control col-md-2">
                <option value='' selected="selected"> Please select Income Account</option>
                <?php
                foreach($acc_names_array as $value)
                {
                    echo"   <option value='$value'>". $value."</option>";
                }             
                 ?>
                    </select>
                 
            </div>
        </div>

         <br></br><br></br>
 

         <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              
                {{ Form::submit('Search', array('class' => 'btn btn-primary')) }}
              
            </div>
         </div>

                {{ Form::close() }}
 
        </div>
    </div>
</div>
@stop