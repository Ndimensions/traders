@extends ('layouts.plane')
@section('page_heading','School Information')

@section('body')
@include('menu.main_menu');

<div class="container">
        <div class="panel panel-default">
            
            <div class="panel-heading">   
                <p></p>
                <div class="btn-toolbar">
                    @include('menu.student_menu')
                </div>
            </div>

        <div class="panel-body">
    <h1>Student Results</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif


    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
             <td>Surname First Name Last Name</td>
            <td>Class Name</td>
            <td>Registration Number</td>

        </tr>
        </thead>
        <tbody>
          
        @foreach($student as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->surname }} {{ $value->first_name }} {{ $value->last_name }}</td>   
                <td>{{ $value->grade->name }}</td>
                <td>{{ $value->reg_no }}</td>


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'students/del/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                    {{ Form::close() }}

                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('students/' . $value->id) }}">Show this Student</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('students/' . $value->id . '/edit') }}">Edit this Student</a>

                </td>
            </tr>
            
        @endforeach
        </tbody>
    </table>



     </div>
    </div>
 
</div>
@stop