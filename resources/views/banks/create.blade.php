@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>

            <div class="panel-body">
                <a class="btn btn-primary" href="{{URL::to('cheques')}}"> View Recent Transactions</a>
                <h4 class="text-info">Add Cheque Payments</h4>

                @include('errors.error_partials')

                @if(isset($print))


                <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> </button>
                            <h4 class="modal-title" id="myModalLabel">Cheque Payments</h4>
                        </div>

                        <div class="modal-body">
                                <address>
                                <strong>Queen of Peace Academy</strong><br>
                                P.O.Box ---, Karatina<br>
                               
                                <abbr title="Phone">Telephone:</abbr> +254716505906
                                
                                
                                </address>
                                No: <h5 class="pull-right">{{$receipt_id}}</h5>

                                
                                <table class="table table-regular">
                                
                                <tbody>
                                    <thead>
                                        <tr>
                                        <td> </td>
                                        <td></td>
                                        <td>{{$bank->transaction_date }}</td>

                                       

                                    </tr>
                                      <tr>
                                         <th>Bank</th>
                                           <td>{{$bank->bank}}</td>                                     
                                         

                                    </tr>
                                      <tr>
                                         <th>Cheque No:</th>
                                           <td>{{$bank->cheque_number}}</td>                                     
                                         

                                    </tr>
                                    <tr>
                                         <th>FOR WHAT REQUIRED</th>
                                           <td>{{$bank->description}}</td>                                     
                                         

                                    </tr>
                                      
                                    </thead>
                                     
                                    <tr>
                                       <th>Amount</th>
                                        <td>{{$bank->amount}}</td>
                                        
                                    </tr>
                                     
                                </tbody>
                            </table> 
                            <table>
                                <tr>
                                    <td>CHECKED BY:</td>
                                    <td>......................</td>
                                </tr>
                                <tr></tr>
                                <tr></tr>

                                <tr>
                                    <td>PASSSED BY</td>
                                    <td>......................</td>
                                </tr>

                            </table>           
                               
                                <address>
                                <strong>With Thanks</strong><br>
                                <strong>Served By</strong><br>
                                {{Auth::user()->first_name}}  {{Auth::user()->last_name}}
                                </address>


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Print</button>
                        </div>
                    </div>
                </div>
            </div>

                            <a href="#" class="btn btn-lg btn-success"
                            data-toggle="modal"
                            data-target="#basicModal"
                            >Print Receipt</a>

               
                @endif

                {{ Form::open(array('url' => 'cheques')) }}

                @include('includes.user_data')

                @include('banks.edit_partials')

                
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>

                {{  Form::close()  }}
                 
        </div>
    </div>
</div>
@stop