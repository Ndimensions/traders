@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
 
			  

   				@include('errors.error_partials')

          @if(count($trials) == 0)
          {{'Reports not available.'}}
          @else
<button type="submit" onclick="printDiv()" class="btn btn-lg btn-primary pull-right">Print</button>

  <div id="printeodContents">
 <h4>End of Day Balances</h4>
    <table class="table table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
            <td>Date</td>
            <td>Account Name </td>
            <td> Closing Balance</td>
                        

        </tr>
        </thead>
        <tbody>
          
        @foreach($trials as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{$value->created_at}}</td>
                 <td>{{$value->ledger->name}}</td>
                <td>{{$value->amount}}</td>
                                     
            </tr>
              
    </div>
            
        @endforeach
        </tbody>
       
            
         
        </tbody>
    </table>

 <script type="text/javascript">
        function printDiv() {
         var printContents = document.getElementById('printeodContents').innerHTML;
         var originalContents = document.body.innerHTML;

         document.body.innerHTML = printContents;

         window.print();

         document.body.innerHTML = originalContents;
        } 
     </script>
</div>
</div>
</div>
</div>


@endif
@stop