@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
 
			   <h4>System Financial Accounts</h4>
             <a class="btn btn-primary" href="{{URL::to('divisions')}}"> Financial Divisions</a>
             <a class="btn btn-primary" href="{{URL::to('voteheads')}}"> Financial Vote heads</a>
 

 <hr/>

   				@include('errors.error_partials')

          @if(count($ledgers) == 0)
          {{'Accounts not created.'}}
          @else


    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Fund</th>
            <th>Division</th>
             <th>Vote Head</th>
            <th>Account Name </th>
            <th>Decription</th>             

        </tr>
        </thead>
        <tbody>
          
        @foreach($ledgers as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{'Operating Fund'}}</td>
                <td>{{$value->type->division->name}}</td>
                  <td>{{$value->type->name}}</td>
                  <td>{{$value->name}}</td>
                 <td>{{$value->description}}</td>    
                <td>       
              <!-- show the subject (uses the show method found at GET /nerds/{id} 
                    <a class="btn btn-small btn-success" href="{{ URL::to('system/accounts/' . $value->id) }}">Ledger</a>
 
                    <a class="btn btn-small btn-info" href="{{ URL::to('system/accounts/' . $value->id . '/edit') }}">Transactions</a>
            -->
            </td>
            </tr>

              
    </div>
            
        @endforeach
        </tbody>
       
            
         
        </tbody>
    </table>
</div>
</div>
</div>
@endif
@stop