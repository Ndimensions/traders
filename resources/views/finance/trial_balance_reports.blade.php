@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
 
			   <h4>End of Day Balances</h4>

   				@include('errors.error_partials')

          @if(count($ledger_reports) == 0)
          {{'Users Not Added'}}
          @else


    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
            <td> Date</td>
            <td>Account Name </td>
            <td> Closing Balance</td>
                        

        </tr>
        </thead>
        <tbody>
          
        @foreach($ledger_reports as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->ledger->name}}</td>
                <td>{{$value->amount}}</td>
                                  
            </tr>

              
    </div>
            
        @endforeach
        </tbody>
       
            
         
        </tbody>
    </table>
</div>
</div>
</div>
@endif
@stop