@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
                

        @include('errors.error_partials')
        </nav>
        @if ( !$record->count() )

           Account does not have any transactions.
        @else

        <h3>Displaying Account Transactions</h3>
           
        

            <table class="table table-hover table-striped table-bordered  ">
                <thead>
                    <tr>
                         <th>Vote Head Name</th>
                          <th>Account Type</th>
                          <th>Account Name</th>
                       

                    </tr>
                     <tr>
                        <td>{{ucwords($account->type->name)}}</td>
                        <td>{{ucwords($account->division->name)}}</td>
                        <td>{{$account->name}}</td>
                    </tr>
  
  
                <tr>
                    
                    <th>Number</th>
                    <th>Date</th>
                     <th>Description</th>
                     <th>Debit</th>
                     <th></th>
                    <th>Credit</th>
                    <th>Balance</th>

                                    
                </tr>
                </thead>
                <tbody>
                   
                        <?php 

                        $db_count=0;
                        $cr_count=0;
                        $count=0;

                         ?>
               @foreach($record as $key=>$value)
                        <td>{{ $key+1}}</td>
                        <td>{{ $value->transaction->voucher->transaction_date }}</td>                         
                        <td>{{ $value->transaction->voucher->description }}</td>

                        <?php 
                          $transaction_type=$value->transaction_type;
                        ?>

                        @if($transaction_type==='Debit')
                        <td>{{$value->amount}}<td>
                        <td>{{'0'}}</td>

                         <?php $db_count+=$value->amount;
                                $count+=$value->amount;

                         ?>
                           <td>{{$count}}</td>
                        @else
                        
                          <td>{{'0'}}<td>
                           <td>{{$value->amount}}</td>
                          <?php $cr_count+=$value->amount;
                                $count-=$value->amount;
                          ?>
                              <td>{{$count}}</td>
                        @endif                 
                    
                            
                          
                         
                    </tr>
                @endforeach
                <tr>
             <th>Total</th><td></td><td></td>
                    <th>{{$db_count}}</th><td></td><th>{{$cr_count}}</th>

                </tr>
                
                </tbody>
            </table>
            @endif
 
</div>

</div>

</div>
    
@stop