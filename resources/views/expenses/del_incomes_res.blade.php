@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
<h3 class="text-info text-center"> Father's  Incomes Report</h3>
             <table  class="table table-striped">
                <tr>
                    <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td>  <h5>Second Date:    {{$second_date}}</h5></td>
                    <td> <h5>Total Amount:    {{$receipts}}</h5></td>

                </tr>
             </table>
 
   				@include('errors.error_partials')

@if (count($range_expense) === 0)
{{'No  Expenses Entered Today'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
              <th>Date</th>
              <th>Transaction No.</th>
             <th>Ref.</th>
             <th>Description</th>    
             <th>Particulars</th> 
             <th>Received</th>                 
             <th>Amount</th>
             <th>Delete</th>
           

        </tr>
        </thead>
        <tbody>
             <?php $count=0 ?>
          
        @foreach($range_expense as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                 <td>{{ $value->transaction_date }}</td>
                 <td>{{$value->transaction_no}}</td>
                     <td>{{ $value->ref }}</td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->particulars }}</td>
                <td>{{ $value->payees_name }}</td>

                  <td>{{ $value->amount}}</td>

                   <td>
                        <a href="#" class="btn btn-small btn-danger pull-right"
                            data-toggle="modal"
                            data-target="#basicModal{{$value->transaction_id}}"
                            >Delete</a>
               
                </td>
                 <?php $count+=$value->amount?>
                  <div class="modal fade" id="basicModal{{$value->transaction_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Confirm Deletion</h4>
                          </div>

                          <div class="modal-body">
                               Are You Sure You Want To Delete?
                                
                                {{ Form::open(array('url' => 'del/frincconfirmtrans', 'class' => 'pull-right')) }}
                                {{Form::hidden('value',$value->id)}}
                               
                                       
                                       
                          </div>
                          <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                          </div>
                                 {{ Form::close() }}
                    </div>
              </div>
    </div>
                       
            </tr>
            
        @endforeach
        <tr>
  
            <tr>
        </tbody>
    
            
         
        </tbody>
    </table>

</div>
<?php //echo $range_expense->render(); ?>
</div>
@endif
</div>
@stop
           
 