 
 
    <div class="panel panel-default">
         
            <div class="panel-body">
            
            <h4>Search Account</h4>
            
                @include('errors.error_partials')

                {{ Form::open(array('route' => 'account.search')) }}

                 <div class="form-group">
                    {{ Form::label('account_name', 'Account Name') }}
                    {{ Form::text('account_name', null, array('class' => 'form-control col-md-5')) }}         
                </div>

              
                {{ Form::submit('Save') }}

                {{  Form::close()  }}
        </div>
    </div>

 