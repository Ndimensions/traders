@extends ('layouts.plane')

@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>

            <div class="panel-body">
            <a class="btn btn-primary" href="{{URL::to('fees')}}"> View Recent Receipts</a>
            <a class="btn btn-primary" href="{{URL::to('fees/create')}}">  Enter New</a>

            <h4>Enter A Deposit</h4>

                @include('errors.error_partials')

                @if(isset($print))
                
              <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> </button>
                            <h4 class="modal-title" id="myModalLabel">Payment Voucher </h4>
                       <br/>
                        </div>

            <div class="modal-body text-center" >
            <div id="printContents"  >                   
 
                   
          <div class="print2" style="margin-left:20px;  ">                      
                           

            <table padding="2px" align="center">

                <tr>
                  <th colspan="5"> KARATINA CATHOLIC CHURCH </th>
                </tr>
                <tr>
                  <th>P.O.BOX 30,KARATINA</th>
                </tr>
                <tr>
                   <th>(061) 72248</th>
                </tr>
                <tr><td>&nbsp;</td></tr>
                  <tr>
                   <th>PAYMENT VOUCHER</th>
                </tr>

           </table>
      </br>


        <table cellspacing="4px" align="right">
           <tr>
            <td >Voucher No: {{$voucher->transaction_no}}</td>
          </tr>   
        </table>

        <table cellspacing="4px">
           <tr>
            <td >Payee's Name: {{$voucher->payees_name}}</td>
          </tr>   
        </table>


      </br>
    </br>

        <table border="1px" width="100%" >
          <tr>
            <td>DATE</td>
            <td>PARTICULARS</td>
            <td>AMOUNT</td>
          </tr>
          <tr>
            <td>{{$voucher->transaction_date}}</td>
              <td>{{$voucher->description}}</td>
               <td>{{number_format($voucher->amount)}}</td>
          </tr>
        </table>

</br>
 
        <table cellspacing="3">
          <td>Amount in words:</td>  
          <td><?php $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                echo ucfirst($f->format($voucher->amount).'&ensp;'.'shillings only'); ?></td>
        </table>
 

        <p> I certify that the above account is correct and was incurred under the above authority quoted and that 
          the amount paid is chargeable to the following.</p>


           <table padding="4px" border="1 px solid" width="100%">
              <tr>
                <td>CHARGEABLE VOTEHEAD</td>
               
                <td>AMOUNT</td>
              </tr>
              <tr>
                <td style="height:10px;" height="10"> {{$votehead}}</td>
                <td>{{number_format($voucher->amount)}}</td>
              </tr>
              <tr>
                <td>TOTAL AMOUNT</td>
                <td>{{number_format($voucher->amount)}}</td>
           </table>

           <br/>
           <br/>

          <table cellspacing="4px" align="right">
           <tr>
            <td >{{$trans}}</td>
          </tr>   
        </table>

          <hr/>
          <br/>
         


        <table  width="100%">
            <tr>
              <td>Date</td> <td>...................................</td>  <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>     
            </tr>
            <tr>
             <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Signature</td>  <td>...................................</td>  <td>&nbsp;<td><td>&nbsp;<td><td>&nbsp;  <td>....................................</td>
              </tr> 

              <tr>
              <td>&nbsp;</td>     <td>Parish Priest</td>                        <td>&nbsp;</td><td>&nbsp;<td><td>&nbsp;<td> <td>Clerk/Secretary</td>
            </tr>

            <tr>
            <td>&nbsp;</td>     <td>Rev. Fr. Herman Kiboi</td>                        <td>&nbsp;</td><td><td>&nbsp;<td><td>&nbsp; <td>{{Auth::user()->first_name}}  {{Auth::user()->last_name}}.</td>
            </tr>

        </table>  
        <br/>
      </br>
    </br>
  </br>
  <table>

    <tr><td>Printed from/Computer name:</td><td><?php echo gethostbyaddr($_SERVER['REMOTE_ADDR']).'-'.$_SERVER['REMOTE_ADDR']; ?></td>
    </tr>
  </table>
        
                  </div>

          </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                    </div>
                     
                </div>
            </div>
        </div>
                       
                <a href="#" class="btn btn-lg btn-success"
                data-toggle="modal"
                data-target="#basicModal">View Transaction</a>

    <button type="submit" onclick="printDiv()" class="btn btn-lg btn-primary">Print</button>

                  

    <script type="text/javascript">
        function printDiv() {
         var printContents = document.getElementById('printContents').innerHTML;
         var originalContents = document.body.innerHTML;

         document.body.innerHTML = printContents;

         window.print();

         document.body.innerHTML = originalContents;
        } 
     </script>
     
    </div>

                @endif
 
                @include('includes.user_data')                
        </div>
    </div>
</div>
@stop