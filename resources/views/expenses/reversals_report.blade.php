@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <h3 class="text-info text-center"> Expenses Reversal Report Before Posting</h3>
             <table  class="table table-striped">
                <tr>
            
                     <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td>  <h5>Second Date:    {{$second_date}}</h5></td>
                    <td> <h5>Total Amount:    {{$total_amount}}</h5></td>
 
                </tr>
             </table>            		    
   				@include('errors.error_partials')

@if (count($reversals) === 0)
<h4 class="text-info text-center">{{'Student Fees Not Entered'}}</h4>
@else

 
    <hr/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Reversal Date</th> 
            <th>Trans. No</th>
            <th>Transaction Date</th>         
            <th>Transaction Type</th>
            <th>Transaction Remarks</th>
            <th>Reversed By</th>
             <th>Particulars</th>
            <th>Amount</th>  
            <th>Totals</th>        
          

        </tr>

        </thead>
        <tbody>
        <?php $count=0 ?>

        @foreach($reversals as $key => $value)

            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->deletion_date }}</td>
                <td>{{ $value->transaction_no }}</td>
                <td>{{ $value->transaction_date }}</td>
                <td>{{ $value->transaction_type }}</td>                 
                 <td>{{ $value->transaction_remarks}}</td>
                 <td>{{$value->deleted_by}}</td>
                 <td>{{ $value->particulars }}</td>
                <td>{{ $value->amount }}</td>
                <?php $count+=$value->amount?>
                <td>{{ $count}}</td>
                
                               
            </tr>
            
        @endforeach
        </tbody>
    </table>
    <h4 class="pull-right">Printed By:{{Auth::user()->first_name}} {{Auth::user()->last_name}}  {{Carbon::now('EAT')}}</h4>

</div>
<?php //echo $reversals->render(); ?>
</div>
@endif
</div>
@stop