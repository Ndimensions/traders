<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    @guest
    <title>{{ config('app.name', 'Laravel') }}</title>
    @else
    <ul   class="nav nav-tabs"  >
    @if(Auth::user()->is_admin == 0)
    <li role="presentation"><a href="{{URL::to('home')}}"><strong>Home</strong></a></li>
    <li role="presentation"><a href="{{URL::to('cash/vouchers/create')}}"><strong>Invest</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite')}}"><strong>Referrals</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/usercommissions')}}"><strong>Commissions</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/userinterests')}}"><strong>Total Earnings</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/userstatement')}}"><strong>Statement</strong></a></li>
    @else
    <li role="presentation"><a href="{{URL::to('home')}}"><strong>Home</strong></a></li>
    <li role="presentation"><a href="{{URL::to('settings')}}"><strong>Investments</strong></a></li>
    <li role="presentation"><a href="{{URL::to('cash/vouchers/create')}}"><strong>Invest</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite')}}"><strong>Invite</strong></a></li>
    <li role="presentation"><a href="{{URL::to('settings')}}"><strong>Commissions</strong></a></li>
    <li role="presentation"><a href="{{URL::to('users')}}"><strong>Users</strong></a></li>
    <li role="presentation"><a href="{{URL::to('master/reports')}}"><strong>Reports</strong></a></li>
    @endif
    </ul>
    @endguest

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
