@extends ('layouts.plane')
@section('page_heading','School Information')

@section('body')
@include('menu.main_menu');

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
     
        <div class="btn-toolbar">
           
        </div>
        @include('sub_menu.investment')
            </div>
                <div class="panel-body">
                    @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
            <table class="table table-bordered">
                <tr>
                    <Td></Td>
                    <td>Cash</td><td>Liabilities</td><td>Balance</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td class="btn-info">{{ $cash_balance }}</td> 
                    <td class="btn-warning">{{ $liabilities }}</td>
                    <td class="btn-success">{{ $cash_balance - $liabilities }}</td>
                    
                </tr>
            </table>
             
        </div>
</div>
</div>
</div>
</div>
@stop