@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

        </div>
            <div class="panel-body">
            <a class="btn btn-primary" href="{{URL::to('accounts')}}"> View accounts</a>

            <h4>Edit</h4>   
            
            @include('errors.error_partials')

            {{ Form::model($account, array('route' => array('accounts.update', $account->id), 'method' => 'PUT')) }}

            @include('accounts.edit_partials')

           <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                         {{ Form::submit('Update Account', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>

            {{ Form::close() }}
 
        </div>
    </div>
</div>
@stop