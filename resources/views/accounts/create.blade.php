@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>
        </div>
        <div class="panel-body">
            <a class="btn btn-primary" href="{{URL::to('accounts')}}"> View Accounts</a>
            <h4 class="text-info">Open an Account </h4>
                @include('errors.error_partials')
                {{ Form::open(array('url' => 'accounts')) }}
            <hr/>
        <div class="form-group">
            {{ Form::label('account_type', 'Vote Head Name',array('class'=>'col-md-2 control-label')) }}
            <div class="col-md-6">
            {{ Form::select('type_id', $account_types ,null, array('class' => 'form-control col-md-3')) }} 
            </div>
        </div>
        <br></br>  <br></br>
        <div class="form-group">
            {{ Form::label('name', 'Account Name',array('class'=>'col-md-2 control-label')) }}
             <div class="col-md-6">
            {{ Form::text('name', null, array('class' => 'form-control col-md-5')) }}
        </div>
        </div>
         <br></br>  <br></br>
        <div class="form-group">
            {{ Form::label('description', 'Description',array('class'=>'col-md-2 control-label')) }}
             <div class="col-md-6">
            {{ Form::text('description',null, array('class' => 'form-control')) }}
        </div>
        </div>
        <br></br>  <br></br>
        <hr/>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
            </div>
        </div>

        {{  Form::close()  }}
        </div>
    </div>
</div>
@stop