@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <a class="btn btn-primary" href="{{URL::to('accounts/create')}}"> New Account</a>
             
          <!--   <a class="btn btn-primary" href="{{URL::to('search/accounts')}}">Search Accounts </a> -->

			   <h4>Displaying Accounts</h4>

   				@include('errors.error_partials')
 

      <table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Name</th>
            <th>Vote Head</th>
      
        <th>Account Details</th>
    


        </tr>
        </thead>
        <tbody>
        <tr><td>Revenue Accounts</td></tr>
        @foreach($revenues as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->name }} </td>
               <td>{{ $value->type->name }} </td>

            
                


                 <td>
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Want To Delete?
                                            
                                            {{ Form::open(array('url' => 'accounts/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>

                     <a class="btn btn-small btn-success" href="{{ URL::to('accounts/' . $value->id) }}">Show this Account</a>

                 
                     


                </td>
                         
            </tr>
            
        @endforeach

         <tr><td>Expense Accounts</td></tr>
        @foreach($expenses as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->name }} </td>
                <td>{{ $value->type->name }} </td>
                


                 <td>
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Want To Delete?
                                            
                                            {{ Form::open(array('url' => 'accounts/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>

                     <a class="btn btn-small btn-success" href="{{ URL::to('accounts/' . $value->id) }}">Show this Account</a>

                 
                 


                </td>
                         
            </tr>
            
        @endforeach

         <tr><td>Asset accounts</td></tr>
        @foreach($assets as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->name }} </td>
              <td>{{ $value->type->name }} </td>
                


                 <td>
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Want To Delete?
                                            
                                            {{ Form::open(array('url' => 'accounts/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>

                     <a class="btn btn-small btn-success" href="{{ URL::to('accounts/' . $value->id) }}">Show this Account</a>

                 
                 


                </td>
                         
            </tr>
            
        @endforeach
         <tr><td>Lialities accounts</td></tr>
        @foreach($liabilities as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->name }} </td>
                <td>{{ $value->type->name }} </td>
                


                 <td>
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Want To Delete?
                                            
                                            {{ Form::open(array('url' => 'accounts/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>

                     <a class="btn btn-small btn-success" href="{{ URL::to('accounts/' . $value->id) }}">Show this Account</a>

                 
                 


                </td>
                         
            </tr>
            
        @endforeach

        </tbody>
       
            
         
    </table>
</div>
 </div>
 
</div>
@stop
