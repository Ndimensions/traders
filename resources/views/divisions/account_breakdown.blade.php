@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
            <a class="btn btn-small btn-success" href="{{ URL::to('expenses/create') }}">Enter New Expense</a>
            
                

        @include('errors.error_partials')
        </nav>
        @if ( !$expense->count() )
        <br></br>
        <div class='flash alert-info'>
            <p class="text-center">No Records Available</p>
           
            
        </div>
        @else
        <h4>Displaying Vote Head Balance</h4>
           
        

            <table class="table table-hover table-striped table-bordered  ">
                <thead>
                    <tr>
                        <th class='text-info'>Vote Head Name</th>
                        <th class='text-info'>Vote Head Description</th>
                        <th class='text-info'>Vote Head Comments</th>
                         

                    </tr>
                     <tr> 
                        <th>{{$type->name}}</th>
                        <th>{{$type->description}}</th>
                        <th>{{$type->comments}}</th>
                    </tr>
                    

                <tr>
                    
                    <td>Number</td>
                    <td>Date</td>
                    <td>Received By</td>
                    <td>Description</td>
                    <td>Commments</td>
                    <td> Amount</td>
                    <td> Total</td>                    
                </tr>
                </thead>
                <tbody>
                     
               @foreach($expense as $key=>$value)
                     
                    <tr>
                          
                        <td>{{ $key+1}}</td>
                        <td>{{ Carbon\Carbon::parse($value->transaction_date)->toFormattedDateString() }}</td>
                        <td>{{ $value->user->first_name }} {{ $value->user->last_name }}</td>
                        <td>{{ $value->description }}</td>
                        <td>{{ $value->comments }}</td>
                        <td>{{ $value->amount }}</td>
                         
                         
                        <!-- we will also add show, edit, and delete buttons -->
                        <td>

                            <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                            <!-- we will add this later since its a little more complicated than the other two buttons -->
                            
                            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->

                            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
 
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>{{$total}}</td>
                </tr>
                
                </tbody>
            </table>
            @endif

</div>
</div>
</div>
    
@stop