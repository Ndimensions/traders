@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
            <a class="btn btn-primary" href="{{URL::to('functions')}}"> View Expenses Functions</a>

               <h4>Add a Financial Function</h4>
                @include('errors.error_partials')
             <div class="form-group">
            {{ Form::label('Category', 'Division') }}
            <p>EXPENSE</p>
         </div>

           {{ Form::open(array('url' => 'functions')) }}

            @include('functions.edit_partials')
            <br></br>

       

          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              
                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
              
            </div>
         </div>

            {{  Form::close()  }}
</div>
</div>
</div>
@stop