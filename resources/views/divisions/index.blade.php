@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
              @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
               

			   <h4>Displaying Financial  Divisions</h4>

   				@include('errors.error_partials')

                @if(count($divisions) == 0)
                {{'Vote-heads  not entered'}}
                @else


    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <td>Number</td>
            <td>Divison Code</td>
             <td> Fund</td>
            <td> Name</td>
             
           
            <td>Description</td>
          

        </tr>
        </thead>
        <tbody>
          
        @foreach($divisions as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{$value->division_code}}</td>
                <td>{{ $value->fund->name }}</td>
                <td>{{ $value->name }}</td>

                 
                <td>{{ $value->description }}</td>
                
                 


                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                  

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Sure You Want To Delete?
                                            
                                            {{ Form::open(array('url' => 'functions/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>

                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                     
                     
 
                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
 
                </td>
            </tr>
            
        @endforeach
        </tbody>
    </table>
</div>
</div>
@endif
</div>
@stop