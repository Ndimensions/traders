@extends ('layouts.plane')
@section('page_heading','School Information')

@section('body')
@include('menu.main_menu');

<div class="container">
        <div class="panel panel-default">
            
            <div class="panel-heading">   
                <p></p>
                <div class="btn-toolbar">
                    @include('menu.fnc_menu')
                </div>
            </div>

        <div class="panel-body">
            <p class="text-info">Search vote head balances</p>
               
            {{ Form::open(array('url' => 'votehead/report') ) }}
                      
            <form role="form">
            @include('errors.error_partials')
                  
         <div class="form-group">
	         <label class="col-md-4 control-label">First Date</label>
	         <div class="col-md-6">
	        	{{ Form::text('first_date', null , array('class' => 'form-control col-md-3','placeholder'=>'YYYY-MM-DD')) }} 
	         </div>

        </div>

				<br> <br/><br> <br/>
         
         <div class="form-group">
            <label class="col-md-4 control-label">Second Date</label>
            <div class="col-md-6">
                	{{ Form::text('second_date', null , array('class' => 'form-control col-md-3','placeholder'=>'YYYY-MM-DD')) }} 
            </div>
        </div>

         <br></br><br></br>

          <div class="form-group">

                <label class="col-md-4 control-label">Vote Head</label>
                <select name="votehead">
                    <option></option>
                @isset($acctypes)
                {
                @foreach($acctypes as $value)
                <div class=" col-md-6">
                      <option  class="form control col-mod-3" value={{$value->id}}> {{$value->name}}</option>
                </div>
                @endforeach
                }
                </select>
       
        </div>

         <br></br><br></br>
 

         <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              
                {{ Form::submit('Search', array('class' => 'btn btn-primary')) }}
              
            </div>
         </div>

                {{ Form::close() }}
 
        </div>
    </div>
</div>
@stop