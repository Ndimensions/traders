@extends ('layouts.plane')
@section('page_heading','School Information')

@section('body')
@include('menu.main_menu');

<div class="container">
        <div class="panel panel-default">
            
            <div class="panel-heading">   
                <p></p>
                <div class="btn-toolbar">
                    @include('menu.parish_menu')
                </div>
            </div>

        <div class="panel-body">
            <h4 class="text-info">Search Parish Expenses Categories</h4>
               
            {{ Form::open(array('url' => 'pcexptypes/form') ) }}
                      
            <form role="form">
            @include('errors.error_partials')
                  
         <div class="form-group">
	         <label class="col-md-4 control-label">First Date</label>
	         <div class="col-md-6">
            <input type="date" name="first_date"  class="form-control col-md-4" placeholder="Date" required="required">
	         </div>

        </div>

				<br> <br/><br> <br/>
         
         <div class="form-group">
            <label class="col-md-4 control-label">Second Date</label>
            <div class="col-md-6">
            <input type="date" name="second_date"  class="form-control col-md-4" placeholder="Date" required="required">
            </div>
        </div>

         <br></br><br></br>

          <div class="form-group">

                <label class="col-md-4 control-label">Select Account</label>
                <select name="account" required="required">
                    <option></option>
                @isset($cash_bank)
                {
                @foreach($cash_bank as $value)
                <div class=" col-md-6">
                      <option  class="form control col-mod-3" value={{$value->id}}> {{$value->name}}</option>
                </div>
                @endforeach
                }
                </select>
       
        </div>

         <br></br><br></br>
 

         <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              
                {{ Form::submit('Search', array('class' => 'btn btn-primary')) }}
              
            </div>
         </div>

                {{ Form::close() }}
 
        </div>
    </div>
</div>
@stop