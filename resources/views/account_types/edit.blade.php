@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
        <a class="btn btn-primary" href="{{URL::to('voteheads')}}"> View Vote Heads</a>

               <h4>Edit</h4>   
        
       @include('errors.error_partials')

        {{ Form::model($account_type, array('route' => array('voteheads.update', $account_type->id), 'method' => 'PUT')) }}

        @include('account_types.edit_partials')

        {{ Form::submit('Update Votehead', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
 
</div>
</div>
</div>
@stop