@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
                

        @include('errors.error_partials')
        </nav>
        @if ( !$type->count() )
        <br></br>
        <div class='flash alert-info'>
            <p class="text-center">No Records Available</p>
           
            
        </div>
        @else
        <h4>Displaying Vote Head Balance</h4>
           
        

            <table class="table table-hover table-striped table-bordered  ">
                <thead>
                    <tr>
                        <th class='text-info'>Vote Head Name</th>
                        <th class='text-info'>Vote Head Description</th>
                        <th class='text-info'>Vote Head Comments</th>
                         

                    </tr>
                     <tr> 
                        <th>{{$type->name}}</th>
                        <th>{{$type->description}}</th>
                        <th>{{$type->comments}}</th>
                    </tr>
                    

                <tr>
                    
                    <td>Number</td>           
                    <td>Account Name</td>
                     <td> Amount</td>
                                         
                </tr>
                </thead>
                <tbody>
                     
               @foreach($account_array_name as $key=>$value)
                     
                    <tr>
                          
                        <td>{{ $key+1}}</td>                       
                        <td>{{ $value }}</td>
                        <td>{{$account_total[$key]}}</td>                     
                         
                       
                    </tr>
                @endforeach
                <tr>
                    <th>Total</th><td> <th>{{$total}}</th> 
                   
                </tr>
                
                </tbody>
            </table>
            @endif

</div>
</div>
</div>
    
@stop