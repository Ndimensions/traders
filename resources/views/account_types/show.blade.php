@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <a class="btn btn-primary" href="{{URL::to('acctypes/create')}}"> Add Vote Head</a>
            <a class="btn btn-primary" href="{{URL::to('acctypes')}}"> View Vote Heads</a>
            
               <h4>Displaying Vote Head</h4>

                @include('errors.error_partials')
        </nav>
        @if ( !$account_type->count() )

            You have no projects
        @else

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td>ID</td>
                    <td> Account Name</td>
                    <td>Other Identifier</td>
                    <td>Description</td>

                </tr>
                </thead>
                <tbody>
                 Showing
                    <tr>
                        <td>{{ $account_type->id }}</td>
                        <td>{{ $account_type->name }}</td>
                        <td>{{ $account_type->other_identifier }}</td>
                        <td>{{ $account_type->description }}</td>


                        <!-- we will also add show, edit, and delete buttons -->
                        <td>

                            <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                            <!-- we will add this later since its a little more complicated than the other two buttons -->
                            
                            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->

                            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                            <a class="btn btn-small btn-info" href="{{ URL::to('acctypes/' . $account_type->id . '/edit') }}">Edit this Vote Head</a>

                        </td>
                    </tr>

                </tbody>
            </table>

</div>
</div>
</div>
    @endif
@stop