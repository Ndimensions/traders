@extends ('layouts.plane')
@section('page_heading','School Information')

@section('body')
@include('menu.main_menu');

<div class="container">
        <div class="panel panel-default">
            
            <div class="panel-heading">   
                <p></p>
                <div class="btn-toolbar">
                    @include('menu.landc_menu')
                </div>
            </div>

        <div class="panel-body">
            <h4 class="text-info">Search Locals & Associations Category Withdraws</h4>
               
            {{ Form::open(array('url' => 'landcategory/report') ) }}
                      
            <form role="form">
            @include('errors.error_partials')
                  
         <div class="form-group">
	         <label class="col-md-4 control-label">First Date</label>
	         <div class="col-md-6">
             <input type="date" name="first_date"  class="form-control col-md-4" placeholder="Date" required="required">	         </div>

        </div>

				<br> <br/><br> <br/>
         
         <div class="form-group">
            <label class="col-md-4 control-label">Second Date</label>
            <div class="col-md-6">
             <input type="date" name="second_date"  class="form-control col-md-4" placeholder="Date" required="required">            </div>
        </div>

         <br></br><br></br>

          <div class="form-group">

                <label class="col-md-4 control-label">Select Vote Head</label>
                <select name="votehead">
                    <option></option>
                @isset($acctypes)
                {
                @foreach($acctypes as $value)
                <div class=" col-md-6">
                      <option  class="form control col-mod-3" value={{$value->id}}> {{$value->name}}</option>
                </div>
                @endforeach
                }
                </select>
       
        </div>

         <br></br><br></br>
 

         <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              
                {{ Form::submit('Search', array('class' => 'btn btn-primary')) }}
              
            </div>
         </div>

                {{ Form::close() }}
 
        </div>
    </div>
</div>
@stop