@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
              @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             <a class="btn btn-primary" href="{{URL::to('voteheads/create')}}"> Create a Vote Head</a>
             
<!--              <a class="btn btn-primary" href="{{URL::to('search/votehead')}}">Search Vote Heads </a>
 --><!--
            <a class="btn btn-primary" href="{{URL::to('functions')}}">Vote-head Functions</a>
 -->
			   <h4>Displaying Vote Heads</h4>

   				@include('errors.error_partials')
 


   <table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr>
            <th>Number</th>
            <th>Vote Head Name</th>
            <th>Description</th>
            <th></th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        <tr><td>Revenue</td></tr>
        @foreach($revenue as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                 <td>{{ $value->name }}</td>
                <td>{{ $value->description }}</td>
              
               <td>
                    
 
                </td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Sure You Want To Delete {{$value->name }} votehead ?
                                            
                                            {{ Form::open(array('url' => 'voteheads/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>
                      </td>

            </tr>
            
        @endforeach

         <tr><td>Expenses</td></tr>
        @foreach($expenses as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                 
                 <td>{{ $value->name }}</td>
                <td>{{ $value->description }}</td>
              
               <td>
                    
 
                </td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Sure You Want To Delete {{$value->name}} votehead?
                                            
                                            {{ Form::open(array('url' => 'voteheads/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>
                      </td>

            </tr>
            
        @endforeach
         <tr><td>Assets</td></tr>
        @foreach($assets as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
               
                
                 <td>{{ $value->name }}</td>
                <td>{{ $value->description }}</td>
              
               <td>
                    
 
                </td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Sure You Want To Delete {{$value->name}} votehead?
                                            
                                            {{ Form::open(array('url' => 'voteheads/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>
                      </td>

            </tr>
            
        @endforeach
         <tr><td>Liablities</td></tr>
        @foreach($liablities as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                  <td>{{ $value->name }}</td>
                <td>{{ $value->description }}</td>
              
               <td>
                     
 
                </td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Sure You Want To Delete {{$value->name}} votehead?
                                            
                                            {{ Form::open(array('url' => 'voteheads/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>
                      </td>

            </tr>
            
        @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@stop