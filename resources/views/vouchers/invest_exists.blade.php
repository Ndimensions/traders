@extends ('layouts.plane')
 

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
            <div class="btn-toolbar">
            <h4 class="text-info">Invest</h4>
            </div>
        </div>
            <div class="panel-body">
                @include('errors.error_partials')
                <h4 class="btn-warning btn-lg"> You have a current investment, please wait for maturity</h4>
        

        </div>
    </div>
</div>
@stop