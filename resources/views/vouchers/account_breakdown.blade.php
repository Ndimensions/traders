@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
            <a class="btn btn-small btn-success" href="{{ URL::to('expenses/create') }}">Enter New Expense</a>
            
                

        @include('errors.error_partials')
        </nav>
        @if ( !$expense->count() )

           Account does not have any transactions.
        @else
        <h4>Displaying expense</h4>
           
        

            <table class="table table-hover table-striped table-bordered  ">
                <thead>
                    <tr>
                        <td>Account Name</td>
                        <td>Vote Head Name</td>

                    </tr>
                     <tr>
                        <td>{{$account->account->name}}</td>
                        <td>{{$account_type_name}}</td>
                        
                    </tr>
                    

                <tr>
                    
                    <th>Number</th>
                    <th>Date</th>
                    <th>Received By</th>
                    <th>Description</th>
                    <th>Commments</th>
                    <th> Amount</th>
                    <th> Total</th>                    
                </tr>
                </thead>
                <tbody>
                     
               @foreach($expense as $key=>$value)
                     
                    <tr>
                          
                        <td>{{ $key+1}}</td>

                        <td>{{ Carbon\Carbon::parse($value->transaction_date)->toFormattedDateString() }}</td>
                        <td>{{ $value->user->first_name }} {{ $value->user->last_name }}</td>
                        <td>{{ $value->description }}</td>
                        <td>{{ $value->comments }}</td>
                        <td>{{ $value->amount }}</td>
                         
                         
                        <!-- we will also add show, edit, and delete buttons -->
                        <td>

                             
 
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td></td><td></td><td></td><td></td><td></td><td></td>
                    <td>{{$total}}</td>
                </tr>
                
                </tbody>
            </table>
            @endif
<?php echo $expense->render(); ?>
</div>

</div>

</div>
    
@stop