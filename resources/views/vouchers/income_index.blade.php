@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
         
             <a class="btn btn-primary" href="{{URL::to('sadaka/create')}}">Sadaka </a>            
                  <a class="btn btn-primary" href="{{URL::to('stole/create')}}">Stole </a>
                <a class="btn btn-primary" href="{{URL::to('bookshop/create')}}">Bookshop </a>
                  <a class="btn btn-primary" href="{{URL::to('weekdaysadaka/create')}}">Weekday Sadaka</a> 
                  <a class="btn btn-primary" href="{{URL::to('income/journal')}}">Inomes Journal</a>
                  <a class="btn btn-primary" href="{{URL::to('fathersincome')}}">Incomes</a>

			   <h4>Displaying Incomes </h4>

   				@include('errors.error_partials')

@if (count($vouchers) === 0)
{{'Expense transactions not entered'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
             <th>Date</th>
             <th>Account Name</th>
             <th>Transacion No.</th>
             <th>Ref No.</th>
            <th>Description</th>
             <th>Amount</th>    
        
            <th>View Account</th>    
            <th>Account Transactions</th> 

        </tr>
        </thead>
        <tbody>
          
        @foreach($vouchers as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
              
                <td>{{ $value->transaction_date }}</td>
                 <td>{{$value->account->ledger->name}}</td>
                  <td>{{ $value->transaction_no }}</td>
                 <td>{{ $value->ref_number}}</td>
                <td>{{ $value->description }}</td>
                 <td>{{ $value->amount}}</td>
                <?php if($value->active = 1)
                    $status="Yes";
                    else
                    {
                    $status="No";
                    }
                    ?>
           
             

                <td>
                   
                    <a class="btn btn-small btn-success" href="{{ URL::to('accounts/' . $value->account->id) }}">Account Details</a></td>
                    
                     
                      <td><a class="btn btn-small btn-success" href="{{ URL::to('account/transactions/' . $value->account->ledger_id) }}">Transactions</a></td>

                      
            </tr>
            
        @endforeach
        </tbody>
        <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                          </div>

                          <div class="modal-body">
                               Are You Sure You Sure You Want To Delete?
                                
                                  {{ Form::open(array('url' => 'expenses/' . $value->id, 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                       
                                       
                          </div>
                          <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                          </div>
                                 {{ Form::close() }}
                    </div>
              </div>
        </div>
            
         
        </tbody>
    </table>

</div>
</div>
<?php echo $vouchers->render(); ?>
@endif
</div>
@stop
           
 