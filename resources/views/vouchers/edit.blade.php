@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

        </div>
            <div class="panel-body">
             

            <h4>Edit</h4>   
            
            @include('errors.error_partials')

            {{ Form::model($expense, array('route' => array('expenses.update', $expense->id), 'method' => 'PUT')) }}

            @include('expenses.edit_partials')

            {{ Form::submit('Edit this Expense!', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
 
        </div>
    </div>
</div>
@stop