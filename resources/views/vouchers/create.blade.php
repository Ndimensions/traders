@extends ('layouts.plane')
 

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
            <div class="btn-toolbar">
            <h4 class="text-info">Invest</h4>
            </div>
        </div>
            <div class="panel-body">
                @include('errors.error_partials')
                <h4 class="btn-success btn-lg"> You qualify for a maximum investment of Ksh   {{ $dozen_min }} - {{ $dozen_max}} at {{ $rate}}%</h4>

                <h4>Steps to invest</h4>
                <p>1. Go to M-Pesa Lipa na M-Pesa </p>
                <p>2. Select Buy Goods and Services </p>
                <p>3. Enter the Till No 5193893 </p>
                <p>4. Enter amount to invest</p>
                <p>5. Pay</p>
                <p>6. Enter the M-Pesa transaction code for verification</p>
               

                {{ Form::open(array('url' => 'cash/vouchers')) }}
              
                @include('vouchers.edit_partials')
                
                {{Form::hidden('dozen_id',$dozen_id)}}
                
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                    {{ Form::submit('PAY', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>

                {{  Form::close()  }}
        </div>
    </div>
</div>
@stop