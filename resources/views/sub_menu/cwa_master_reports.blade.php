   <h3 class="info"> CWA Cash Book Reports</h4>
            <a class="btn btn-info" href="{{URL::to('cwagenerals')}}">General Reports</a>
            <a class="btn btn-info" href="{{URL::to('cwainc/reports')}}">Income Reports</a>
            <a class="btn btn-warning" href="{{URL::to('cwaexpenditure/reports')}}">Expenditure Reports</a>
            <a class="btn btn-success" href="{{URL::to('cwaincomeexpenditure')}}">Income and Expenditure Balances</a>
            <a class="btn btn-success" href="{{URL::to('hearseincomeexpenditure')}}">Hearse Income and Expenditure Balances</a>