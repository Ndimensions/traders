@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>
        </div>
        <div class="panel-body">
            <h4>Displaying Income Journal</h4>
   				@include('errors.error_partials')
        @if (count($journals) === 0)
        {{'Transactions not entered'}}
        @else

        <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
             <th>Date</th>                
            <th>Transaction Number</th>    
            <th>Ref</th>      
            <th>Description</th>  
            <th>Particulars</th>           
            <th>Received </th>          
            <th>Amount </th>
        </tr>
        </thead>
        <tbody>
          
        @foreach($journals as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction->voucher->transaction_date }}</td>                               
                <td>{{ $value->transaction->voucher->transaction_no }}</td>  
                <td>  {{$value->transaction->voucher->ref}}</td>
              
                <td>  {{$value->transaction->voucher->description }}</td>
                <td>  {{$value->transaction->voucher->particulars }}</td>
                <td>  {{$value->transaction->voucher->payees_name }}</td>

                <td>{{ $value->transaction->voucher->amount}}</td>       
 
                        <td>
                        <a href="#" class="btn btn-small btn-danger pull-right"
                            data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Reverse</a>
               
                </td>
            </tr>

             <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                    <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Confirm Reversal</h4>
                          </div>

                          <div class="modal-body">
                               Are You Sure You Want To Reverse?
                                
                                {{ Form::open(array('url' => 'income/journal/' . $value->id, 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                       
                                       
                          </div>
                          <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                {{ Form::submit('Reverse', array('class' => 'btn btn-danger')) }}
                          </div>
                                 {{ Form::close() }}
                    </div>
              </div>
    </div>
            
        @endforeach
        </tbody>
    </table>

 

</div>
{{ Form::open(array('url' => 'postinc/fathers')) }}

{{ Form::submit('Post ', array('class' => 'btn-small btn-success align:center')) }}
                        
{{ Form::close() }}
 
</div>
<?php //echo $journals->render(); ?>
@endif
</div>
@stop
           
 