@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

            </div>
                <div class="panel-body">
             
<h3 class="text-info text-center">Trial Balance</h3>
             <table  class="table table-striped">
                <tr>
                    <td><h5>First Date:   {{$first_date}}</h5></td>
                    <td>  <h5>Second Date:    {{$second_date}}</h5></td>
 
                </tr>
             </table>
 
   				@include('errors.error_partials')

@if (count($accounts) === 0)
{{'Accounts not created.'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Account Name</th>
             <th>Debit</th>
             <th>Credit</th>
           
        </tr>
        </thead>
        <tbody>
             <?php $count=0; $total=0; $debit_total=0; $credit_total=0;?>
          
        @foreach($accounts as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                 <td>{{ $value->name }}</td>

                 <?php
                    $a=$debit_trans_array[$key];
                    $debit_total+=$a;
                    $b=$credit_trans_array[$key];
                    $credit_total+=$b;
                    $c=$a - $b; ?>

                    @if( $c<1 )
                    <td>{{'0'}}</td>
                    <td>{{$c*-1}}</td>
                    @else
                    <td>{{$c}}</td>
                    <td>{{'0'}}</td>
                    @endif 
 

                 </tr>       
        @endforeach
        <tr>
            <td>TOTAL<td>
            <td>{{$debit_total }}</td>
            <td>{{$credit_total }}</td>
             

            <tr>
        </tbody>
    
            
         
        </tbody>
    </table>

</div>
<?php //echo $range_expense->render(); ?>
</div>
@endif
</div>
@stop
           
 