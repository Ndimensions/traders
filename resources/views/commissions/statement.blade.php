@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            
            <h4><strong>Displaying statement</strong></h4>
        </div>
            </div>
            <div class="panel-body">
			
   				@include('errors.error_partials')
      <table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr> 
            <th>#</th>
            <th>Date</th>
            <th>Description</th>
            <th>Paid</th>
            <th>Withdraw</th>
            <th>Earnings</th>
        </tr>
        </thead>
        <tbody>
        <?php $total_earning=0; ?>
        <?php $total_withdraw=0; ?>
        @foreach($statement as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date }} </td>
                <td>{{ $value->particulars }}</td>
                @if($value->transaction_type == 'Credit')
                <td>{{ $value->amount }} </td>
                <?php $total_earning+=$value->amount; ?>
                <td></td>
                @elseif($value->transaction_type == 'Debit')
                <?php $total_withdraw+=$value->amount; ?>
                <td></td>
                <td>{{ $value->amount }}</td>
                @endif
                <td>{{ $total_earning - $total_withdraw }}</td>
            </tr>
        @endforeach
        </tbody>
        <tr>
            <th>Total</th>
            <th></th>
            <th></th>
            <th>{{ $total_earning}}</th>
            <th>{{ $total_withdraw }}</th>
            <th>{{ $total_earning - $total_withdraw}}</th>
             
        </tr>
    </table>
    <table class="table table-responsive table-striped table-condensed table-bordered">
        <tr>
            <td>#</td>
            <td>Date</td>
            <th>Description</th>
            <td>Amount</td>
        </tr>
        @foreach($withdraw_requests as $key=>$withdraw)
        <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $withdraw->transaction_date }}</td>  
        <td>Withdraw request</td>
        <td>{{ $withdraw->amount}}</td>
        </tr>
        @endforeach
        <tr>
            <th>Total</th>
            <th></th>
            <th></th>
            <th>{{ $withdraw_requests->sum('amount')}}</th>
        </tr>
    </table>
    <?php $balance= $total_earning - $total_withdraw - $withdraw_requests->sum('amount') ?>
    <button class="btn-warning btn-lg text-center"> Account Balance {{ $balance }}</button>
    <br>
    <h4 class="btn-info btn-lg"> Withdraw Request</h4>
    </hr>
    {{ Form::open(array('url' => 'invite/withdrawrequest')) }}
    <div class="form-group">
            <label class="col-md-2 control-label">Amount</label>
            <div class="col-md-6">
            <input type="number" name="amount"  class="form-control col-md-5" placeholder="Enter amount to withdraw" 
            min="500"  max="{{ $balance }}" required="required">
        </div>
      </div>
      <br></br>  <br></br>
    {{Form::hidden('dozen_id','')}}
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
        {{ Form::submit('Request', array('class' => 'btn btn-primary')) }}
        </div>
    </div>
    {{  Form::close()  }}
</div>
</div>
</div>
@stop
