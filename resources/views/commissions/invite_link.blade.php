@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
             
            <h4><strong>Displaying your referrals</strong></h4>
        </div>
            </div>
            <div class="panel-body">
           
            <p>Your active invite link is  {{ $link }} </p>
            <br></br>
            
               @include('errors.error_partials')
    @if($commissions->count()>0)
      <table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr> 
            <th>#</th>
            <th>Date</th>
            <th>Amount</th>
            <th>User</th>
            <!-- //<th>Status</th> -->
        </tr>
        </thead>
        <tbody>
        <?php $count=0;
              $total_earned=0; ?>
        @foreach($commissions as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <?php $count+=1 ?>
                <td>{{ $value->transaction_date }} </td>
                <td>{{ $value->amount }} </td>
                <?php  $total_earned+=$value->amount ?>
                <td>{{ $value->user->name }}</td>
                <!-- @if($value->mature_flag==1)
                <td>{{ 'In process'}}</td>
                @else
                <td>{{ 'Paid' }}</td>
                @endif        -->
            </tr>
        @endforeach
        </tbody>
        <tr>
            <th>Total users</th> <th> {{ $count }}</th>
            <th>Total earned</th> <th> {{ $total_earned }} </th>
        </tr>
    </table>
@else 
<h4><strong>Referrals not found</strong></h4>
@endif
</div>
</div>
</div>
@stop
