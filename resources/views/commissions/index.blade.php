@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            <h4><strong>Displaying Commissions</strong></h4>
        </div>
            </div>
            <div class="panel-body">
			
   				@include('errors.error_partials')
      <!-- <table class="table table-responsive table-striped table-condensed table-bordered">
          <h4><caption><strong>Paid Commissions</strong></caption></h4>
        <thead>
        <tr> 
            <th>#</th>
            <th>Date</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($paid_commissions as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date }} </td>
                <td>{{ $value->amount }} </td>
            </tr>
        @endforeach
        </tbody>
        <tr>
            <td>Total</td>
            <td></td>
            <td>{{  $paid_commissions->sum('amount')}}</td>
        </tr>
    </table> -->
<!-- </hr> -->

<table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr> 
            <th>#</th>
            <th>Date</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($in_process as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date }} </td>
                <td>{{ $value->amount }} </td>
            </tr>
        @endforeach
        </tbody>
        <tr>
            <td>Total</td>
            <td></td>
            <td>{{  $in_process->sum('amount')}}</td>
        </tr>
    </table>
</div>
</div>
</div>
@stop
