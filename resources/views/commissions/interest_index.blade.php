@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
             
            <h4><strong>Displaying Investments Earnings</strong></h4>
        </div>
            </div>
            <div class="panel-body">
            
     @include('errors.error_partials')
      <table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr> 
            <th>#</th>
            <th>Date</th>
            <th>Investment</th>
            <th>Interest</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($paid_interests as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date }} </td>
                <td>{{ $value->principal }}</td>
                <td>{{ $value->amount - $value->principal}}</td>
                <td>{{ $value->amount }} </td>
            </tr>
        @endforeach
        </tbody>
        <tr>
            <td>Total</td>
            <td></td>
            <td>{{  $paid_interests->sum('principal') }}</td>
            <td>{{  $paid_interests->sum('amount')  }}</td>
            <td>{{  $paid_interests->sum('amount') - $paid_interests->sum('principal') }}</td>
        </tr>
        
    </table>
    
    {{ Form::open(array('url' => 'reinvest')) }}
    <table class="table table-responsive table-striped table-condensed table-bordered">
        <tr>
            <th>Actual Balance</th>
            <th>{{$balance}}</th>
            @if($reinvest!=true)
            <input type="hidden" name="record" value="{{ $record }}" >
            <th>{{ Form::submit('Reinvest', array('class' => 'btn btn-primary')) }}</th>
           
            @endif
        </tr>
    </table>
    {{  Form::close()  }}
</hr>
<h4>In process Investments</h4>
<table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr> 
            <th>#</th>
            <th>Date</th>
            <th>Maturity Date</th>
            <th>Principal</th>
            <th>Earning</th>
            <th>Amount</th>
            
        </tr>
        </thead>
        <tbody>
        @foreach($in_process as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date }} </td>
                <td>{{ $value->maturity_date }} </td>
                <td>{{ $value->principal }}</td>
                <td>{{ $value->amount - $value->principal}}</td>
                <td>{{ $value->amount }} </td>
               
            </tr>
        @endforeach
        </tbody>
        <tr>
            <td>Total</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{  $in_process->sum('amount')}}</td>
        </tr>
    </table>
</div>
</div>
</div>
@stop
