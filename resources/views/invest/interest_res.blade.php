@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
       
        <div class="btn-toolbar">
          
        </div>
        @include('sub_menu.investment')
            </div>
                <div class="panel-body">
               
			   

   				@include('errors.error_partials')

        
    @if($interests->count()>0)
    <h4>Displaying investment payables</h4>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Number</th>
            <th>Date</th> 
            <th>Maturity date</th>      
            <th>User</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Principal</th> 
            <th>Amount</th>   
            <th>Interest</th>     
            <th>Days</th>      
        </tr>
        </thead>
        <tbody>
        
        @foreach($interests as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date}}</td>
                <td>{{ $value->maturity_date}}</td>
                <td>{{ $value->user->name}}</td>
                <td>{{ $value->user->email}}</td>
                <td>{{ $value->user->telephone}}</td>
                <td>{{ $value->principal }}</td>
                <td>{{ $value->amount }}</td>
                <td>{{ $value->amount - $value->principal }}</td>
                <td>{{ 'Days'}}</td>                              
            </tr>
            
        @endforeach
        </tbody>
    </table>
</div>
    {{ Form::open(array('url' => 'cashbook/postinterests')) }}
    @foreach($interests as $key => $value)
    <input type="hidden" name="interests[]" value="{{ $value->id }}">
    @endforeach
    {{ Form::submit('Approve', array('class' => 'btn btn-small btn-success align:center')) }}
    {{ Form::close() }} 
@else
<h4>Investments not available</h4> 
@endif
</div>
</div>
@stop
           
 