@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
       
        <div class="btn-toolbar">
          
        </div>
        @include('sub_menu.investment')
            </div>
                <div class="panel-body">
               
			   <h4>Displaying Withdraw requests</h4>

   				@include('errors.error_partials')

@if (count($withdraws) === 0)
{{'Transactions not found.'}}
@else

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Number</th>
            <th>Date</th> 
            <th>Name</th>      
            <th>Email</th>
            <th>Mobile</th>            
            <th>Amount</th>   
             
        </tr>
        </thead>
        <tbody>
        
        @foreach($withdraws as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->transaction_date}}</td>
                <td>{{ $value->user->name}}</td>
                <td>{{ $value->user->email}}</td>
                <td>{{ $value->user->telephone}}</td>
                <td>{{ $value->amount }}</td>     
                <td>
                {{ Form::open(array('url' => 'cashbook/postwithdraws')) }}
                <input type="hidden" name="request" value="{{ $value->id }}">
                </div>
                          {{ Form::submit('Confirm payment') }}
                </div>
                 {{ Form::close() }}
                </td>
            </tr>
            
                </div>
              </div>
            </div>
        @endforeach
        </tbody>
    </table>
</div>
</div>
<?php //echo $journals->render(); ?>
@endif
</div>
@stop
           
 