@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
 
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            
            <h4 class="text-info">Confirm Registration</h4>
        </div>
            </div>
            <div class="panel-body">
                

                @include('errors.error_partials')

                @if(isset($print))
                <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> </button>
                            <h4 class="modal-title" id="myModalLabel">Cash Voucher Receipt</h4>
                        </div>

                        <div class="modal-body">
                                <address>
                                <strong text-align:center;>KARATINA CATHOLIC CHURCH</strong><br>
                                <strong text-align:center;>P.O.Box 30, Karatina</strong><br>
                               
                                <abbr title="Phone" text-align:center;><strong>Tel:</abbr> (061) 72248</strong>
                                
                                
                                </address>
                                No: <h5 class="pull-right">{{$receipt_id}}</h5>
                                
                                <table class="table table-regular">
                                
                                <tbody>
                                    <thead>
                                        <tr>
                                        <td> </td>
                                        <td></td>
                                        <td>{{$voucher->transaction_date }}</td>
                                       

                                    </tr>
                                    <tr>
                                         <th>FOR WHAT REQUIRED</th>
                                          <td>{{$voucher->description}}</td>
                                        
                                         
                                    </tr>
                                      
                                    </thead>
                                     
                                    <tr>
                                        <th>Amount</th>
                                        <td>{{$voucher->amount}}</td>
                                        
                                    </tr>
                                     
                                </tbody>
                            </table> 
                            <table>
                                <tr>
                                    <td>CHECKED BY:</td> 
                                    <td>...................................................</td>
                                </tr>
                                <tr></tr>
                                <tr></tr>

                                <tr>
                                    <td>PASSSED BY</td>
                                    <td>...................................................</td>
                                </tr>

                            </table>           
                               
                                <address>
                                <strong>With Thanks</strong><br>
                                <strong>Served By</strong><br>
                                {{Auth::user()->first_name}}  {{Auth::user()->last_name}}
                                </address>


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button"  class="btn btn-primary">Print</button>
                        </div>
                    </div>
                </div>
            </div>

                            <a href="#" class="btn btn-lg btn-success"
                            data-toggle="modal"
                            data-target="#basicModal"
                            >Print Receipt</a>


    <script type="text/javascript">
        function printDiv() {
         var printContents = document.getElementById('printContents').innerHTML;
         var originalContents = document.body.innerHTML;

         document.body.innerHTML = printContents;

         window.print();

         document.body.innerHTML = originalContents;
        } 
     </script>
            
            @endif

            {{ Form::open(array('url' => 'registration')) }}

            <h3 class="btn btn-warning">Previous code rejected</h3>
            <h4>Steps to activate your account</h4>
            <p>1. Go to M-Pesa Lipa na M-Pesa </p>
            <p>2. Select Buy Goods and Services </p>
            <p>3. Enter the Till No 5193893 </p>
            <p>4. Enter amount 1100</p>
            <p>5. Pay</p>
            <p>6. Enter the M-Pesa transaction code for verification</p>
            <p>7. Wait for 5 minuters for your account to go live</p>
         <br></br>
         
         <div class="form-group">
                    <label class="col-md-2 control-label">M-Pesa Code</label>
                    <div class="col-md-6">
                    {{ Form::text('code', null, array('class' => 'form-control col-md-5')) }}
                </div>
            </div>
            <br></br>
         <br></br>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                    {{ Form::submit('Activate', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>

                {{  Form::close()  }}
                 
        </div>
    </div>
</div>
@stop