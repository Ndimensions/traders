@extends ('layouts.plane')

@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar"> 
        </div>
        </div>
        <div class="panel-body">
        <h4>Registration</h4>
        @include('errors.error_partials')
         <!-- @if(isset($print))
                
              <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> </button>
                            <h4 class="modal-title" id="myModalLabel">Registration</h4>
                       <br/>
                        </div>

            <div class="modal-body text-center" >
            <div id="printContents"  >                   
 
                   
          <div class="print2" style="margin-left:20px;  ">                      
                           

               <table pdding="2px" align="center">
                <tr>
                  <th colspan="5"> RICHHUB TRADERS </th>
                </tr>
                <tr>
           </table>
      </br>


        <table cellspacing="4px" align="right">
           <tr>
            <td >Voucher No: {{$voucher->transaction_no}}</td>
          </tr>   
        </table>

        <table cellspacing="4px">
           <tr>
            <td >Payee's Name: {{$voucher->payees_name}}</td>
          </tr>   
        </table>


      </br>
    </br>

        <table border="1px" width="100%" >
          <tr>
            <td>DATE</td>
            <td>PARTICULARS</td>
            <td>AMOUNT</td>
          </tr>
          <tr>
            <td>{{$voucher->transaction_date}}</td>
              <td>{{$voucher->description}}</td>
               <td>{{$voucher->amount}}</td>
          </tr>
        </table>

</br>
 
        <table cellspacing="3">
          <td>Amount in words:</td>  
          
        </table>
 
 
 

           <br/>
           <br/>

          <table cellspacing="4px" align="right">
           <tr>
            <td >{{$trans}}</td>
          </tr>   
        </table>

          

    <tr><td>Printed from/Computer name:</td><td><?php echo gethostbyaddr($_SERVER['REMOTE_ADDR']).'-'.$_SERVER['REMOTE_ADDR']; ?></td>
    </tr>
  </table>
        
                  </div>

          </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           </div>
                    </div>
                     
                </div>
            </div>
        </div>
                       
                <a href="#" class="btn btn-lg btn-success"
                data-toggle="modal"
                data-target="#basicModal">View Transaction</a>

    <button type="submit" onclick="printDiv()" class="btn btn-lg btn-primary">Print</button>

                  

    <script type="text/javascript">
        function printDiv() {
         var printContents = document.getElementById('printContents').innerHTML;
         var originalContents = document.body.innerHTML;

         document.body.innerHTML = printContents;

         window.print();

         document.body.innerHTML = originalContents;
        } 
     </script>
     
    </div>

                @endif -->
 
                               
        </div>
    </div>
</div>
@stop