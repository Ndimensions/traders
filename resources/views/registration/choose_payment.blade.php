@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="text-info">Confirm Registration</h4>
        <div class="btn-toolbar">
        </div>
            </div>
            <div class="panel-body">
            <a class="btn btn-lg btn-primary" href="{{URL::to('registration/create')}}"> M~Pesa</a>

            <a class="btn btn-lg  btn-warning" href="{{URL::to('pregistration/create')}}"><i class="fa fa-paypal"></i> PayPal</a>
            @include('errors.error_partials')


        </div>
    </div>
</div>
@stop
