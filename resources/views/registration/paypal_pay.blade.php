@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')

<div class="container">
    <div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="btn-toolbar">
                    <p class="text-info">Confirm Registration</p>
                </div>
            </div>
            <div class="panel-body" id="body">
                @include('errors.error_partials')
                <h4><strong>Dear {{$user->name}}</strong></h4>
                <h4>Steps to activate your account</h4>
                <p>1. Click the button below to pay on Paypal</p>
                <br>
                <form action="{{ route('payment') }}" method="POST">
                    {{ csrf_field() }}
                    <button class="btn btn-lg  btn-warning" type="submit"><i class="fa fa-paypal"></i> PAY</button>
                </form>
            </div>
            <div class="panel-footer">
                @if (session('status'))
                    <div class="alert alert-danger alert-dismissable">
                        <button class="btn close"></button>
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@stop
