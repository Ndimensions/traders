@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
 
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            
            <h4 class="text-info">Awaiting Registration</h4>
        </div>
            </div>
            <div class="panel-body">
                
                @include('errors.error_partials')

            <h4>Please wait for a few minutes as your M-Pesa payment is  verified</h4>
                 
        </div>
    </div>
</div>
@stop