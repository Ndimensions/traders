@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('sub_menu.investment')
        </div>
            </div>
                <div class="panel-body">
			   <h4>System users accounts</h4>

   				@include('errors.error_partials')

          @if(count($payments) == 0)
          {{'M-Pesa payments not found.'}}
          @else


    <table   class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Number</th>
            <th>Date</th>
            <th>Code</th>
            <th>Phone</th>
            <th>Name</th>
            <th>Email</th>
            <th>Approve</th>
            <th>Reject</th>
        </tr>
        </thead>
        <tbody>
          
        @foreach($payments as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->created_at }}</td>
                <td>{{ $value->code}}</td>
                <td>{{ $value->user->telephone}}</td>
                <td>{{ $value->user->name}}</td>
                <td>{{ $value->user->email}}</td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                     <a class="btn btn-small btn-info " href="{{ URL::to('registration/' . $value->id ) }}">Approve</a> </td>
                   <td>  <a class="btn btn-small btn-danger " href="{{ URL::to('registration/' . $value->id . '/edit') }}">Reject</a>
                        </td>
                        
            </tr>
        @endforeach
        </tbody>         
        </tbody>
    </table>
</div>
</div>
</div>
@endif
@stop