@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
 
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            
            <h4 class="text-info">Confirm Registration</h4>
        </div>
            </div>
            <div class="panel-body">
            
            @include('errors.error_partials')

            {{ Form::open(array('url' => 'registration')) }}

            <!-- <div class="form-group">
                    <label class="col-md-2 control-label">Amount</label>
                    <div class="col-md-6">
                    {{ Form::text('amount', null, array('class' => 'form-control col-md-5','required'=>'required')) }}
                </div>
            </div>
            <br></br> -->
            <h4>Steps to activate your account</h4>
            <p>1. Go to M-Pesa Lipa na M-Pesa </p>
            <p>2. Select Buy Goods and Services </p>
            <p>3. Enter the Till No 5193893 </p>
            <p>4. Enter amount 1100</p>
            <p>5. Pay</p>
            <p>6. Enter the M-Pesa transaction code for verification</p>
            <p>7. Wait for 5 minuters for your account to go live</p>
         <br></br>
         
         <div class="form-group">
                    <label class="col-md-2 control-label">M-Pesa Code</label>
                    <div class="col-md-6">
                    {{ Form::text('code', null, array('class' => 'form-control col-md-5')) }}
                </div>
            </div>
            <br></br>
         <br></br>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                    {{ Form::submit('Activate', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>

                {{  Form::close()  }}
                 
        </div>
    </div>
</div>
@stop