@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <div class="btn-toolbar">
            
        </div>
             @include('sub_menu.investment')
            </div>
                <div class="panel-body">
             <a class="btn btn-primary" href="{{URL::to('bundles/create')}}"> New Bundle</a>
             

			   <h4>Displaying Available Bundles</h4>

   				@include('errors.error_partials')
 

      <table class="table table-responsive table-striped table-condensed table-bordered">
        <thead>
        <tr>
            
            <th>Number</th>
            <th>Name</th>
            <th>Min</th></th>
            <th>Max</th>
            <th>Expiry Date</th>
             <th>Rate</th>
    


        </tr>
        </thead>
        <tbody>
        
        @foreach($dozens as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->name }} </td>
                <td>{{ $value->min }}</td>
                <td>{{ $value->max }} </td>
                <td>{{ $value->expirydate }} </td>
                <td>{{ $value->rate }}</td>
                <td>
                    <a href="#" class="btn btn-small btn-danger pull-right"  data-toggle="modal"
                            data-target="#basicModal{{$value->id}}"
                            >Delete</a>

                    <div class="modal fade" id="basicModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> Confirm Delete</h4>
                                      </div>

                                      <div class="modal-body">
                                           Are You Sure You Want To Delete?
                                            
                                            {{ Form::open(array('url' => 'bundles/' . $value->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                                   
                                                   
                                      </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                      </div>
                                             {{ Form::close() }}
                                </div>
                          </div>
                    </div>

                     <a class="btn btn-small btn-success" href="{{ URL::to('bundles/' . $value->id . '/edit') }}">Edit</a>

            </td>        
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@stop
