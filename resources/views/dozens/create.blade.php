@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.school_menu')
        </div>

            </div>
            <div class="panel-body">
            <a class="btn btn-primary" href="{{URL::to('bundles')}}"> View Bundles</a>
            <h4 class="text-info">Create a New Bundle </h4>
            
                @include('errors.error_partials')

                {{ Form::open(array('url' => 'bundles')) }}

                
                                 <hr/>

    
               <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Name:</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="idno" name="name"
                        placeholder="Enter the Bundle name" required>
                    
                </div>
                </div>
               <br></br>
                <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Min:</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" id="idno" name="min"
                        placeholder="Enter the minimum amount" required>
                    
                </div>
                </div> 
                <br></br>
              
                <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Max:</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" id="idno" name="max"
                        placeholder="Enter the maximum amount" required>
                    
                </div>
                </div> 
                <br></br>
                 
                 
                <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Expiry Date:</label>
                <div class="col-md-6">
                    <input type="date" class="form-control" id="idno" name="date"
                        placeholder="Enter the expiry date" required>
                </div>
                </div> 
                <br></br>
                <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Rate:</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" id="idno" name="rate"
                        placeholder="Enter the interest rate" required>
                </div>
                </div> 
                <br></br>     
                <hr/>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                         {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
                {{  Form::close()  }}
        </div>
    </div>
</div>
@stop