@extends ('layouts.plane')
@section('page_heading','Form')

@section('body')
@include('menu.main_menu');
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">   
        <p></p>
        <div class="btn-toolbar">
            @include('menu.fnc_menu')
        </div>

        </div>
            <div class="panel-body">
            <a class="btn btn-primary" href="{{URL::to('dozens')}}"> View dozens</a>

            <h4>Edit</h4>   
            
            @include('errors.error_partials')

            {{ Form::model($dozen, array('route' => array('bundles.update', $dozen->id), 'method' => 'PUT')) }}

            
               <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Name:</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="idno" name="name" value="{{$dozen->name}}" 
                        placeholder="Enter the Bundle name" required>
                    
                </div>
                </div>
               <br></br>
                <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Amount:</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" id="idno" name="amount" value="{{$dozen->amount}}" 
                        placeholder="Enter the Bundle price" required>
                    
                </div>
                </div> 
                <br></br>
                <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Expiry Date:</label>
                <div class="col-md-6">
                    <input type="date" class="form-control" id="idno" name="date" value="{{$dozen->expirydate}}" 
                        placeholder="Enter the expiry date" required>
                    
                </div>
                </div> 
                <br></br>
                <div class="form-group row ">
                <label class="control-label col-sm-2" for="name">Rate:</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" id="idno" name="rate" value="{{$dozen->rate}}" 
                        placeholder="Enter the expiry date" required>
                    
                </div>
                </div> 

           <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                         {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>

            {{ Form::close() }}
 
        </div>
    </div>
</div>
@stop