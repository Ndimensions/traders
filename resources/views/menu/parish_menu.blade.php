  
<nav class="navbar navbar-inverse">
<ul  class="nav nav-pills"  >
     <li role="presentation"><a href="{{URL::to('parish')}}">{{'Parish'}}</a></li>
      <li role="presentation"><a href="{{URL::to('pcvoteheads')}}">{{'Votehead'}}</a></li>
       <li role="presentation"><a href="{{URL::to('pcaccounts')}}">{{'Accounts'}}</a></li>
       <li role="presentation"><a href="{{URL::to('pcincomesdash')}}">{{'Incomes'}}</a></li>
       <li role="presentation"><a href="{{URL::to('pcexpensesdash')}}">{{'Expenses'}}</a></li>
       <li role="presentation"><a href="{{URL::to('parishreport')}}">{{'Reports'}}</a></li>
       <li role="presentation"><a href="{{URL::to('stations')}}">{{'OutStations'}}</a></li>
       <li role="presentation"><a href="{{URL::to('districts')}}">{{'Jumuias'}}</a></li>
       <li role="presentation"><a href="{{URL::to('members/create')}}">{{'Members'}}</a></li>
              <li role="presentation"><a href="{{URL::to('tithes/stations')}}">{{'Enter 10%'}}</a></li>

        <li role="presentation"><a href="{{URL::to('titherpts')}}">{{'Tithe Reports'}}</a></li>

</ul>
</nav>
 