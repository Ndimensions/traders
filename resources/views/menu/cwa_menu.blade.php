  
<nav class="navbar navbar-inverse">
<ul  class="nav nav-pills"  >
     <li role="presentation"><a href="{{URL::to('cwa')}}">{{'CWA'}}</a></li>
      <li role="presentation"><a href="{{URL::to('cwvoteheads')}}">{{'Voteheads'}}</a></li>
       <li role="presentation"><a href="{{URL::to('cwaccounts')}}">{{'Accounts'}}</a></li>
       <li role="presentation"><a href="{{URL::to('cwaincomesdash')}}">{{'Incomes'}}</a></li>
       <li role="presentation"><a href="{{URL::to('cwexpensesdash')}}">{{'Expenses'}}</a></li>
       <li role="presentation"><a href="{{URL::to('cwareport')}}">{{'Reports'}}</a></li>

</ul>
</nav>
 