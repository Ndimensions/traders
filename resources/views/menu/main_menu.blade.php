<div> 
<div class="main_menu">
<ul   class="nav nav-tabs"  >
@if(Auth::user()->is_admin == 0)
    <li role="presentation"><a href="{{URL::to('home')}}"><strong>Home</strong></a></li>
    <li role="presentation"><a href="{{URL::to('cash/vouchers/create')}}"><strong>Invest</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite')}}"><strong>Referrals</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/usercommissions')}}"><strong>Commissions</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/userinterests')}}"><strong>Total Earnings</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/userstatement')}}"><strong>Statement</strong></a></li>
@else
    <li role="presentation"><a href="{{URL::to('home')}}"><strong>Home</strong></a></li>
    <li role="presentation"><a href="{{URL::to('cash/vouchers/create')}}"><strong>Invest</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/usercommissions')}}"><strong>Commissions</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/userinterests')}}"><strong>Total Earnings</strong></a></li>
    <li role="presentation"><a href="{{URL::to('invite/userstatement')}}"><strong>Statement</strong></a></li>
    <li role="presentation"><a href="{{URL::to('users')}}"><strong>Users</strong></a></li>  
    <li role="presentation"><a href="{{URL::to('settings')}}"><strong>Investments</strong></a></li>
    <li role="presentation"><a href="{{URL::to('master/reports')}}"><strong>Reports</strong></a></li>   
@endif
            
</div>          
</ul>
</div>
     
 
            	