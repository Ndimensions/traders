
@if(isset($grades))
 
<nav class="navbar navbar-inverse">
<ul  class="nav nav-pills"  >
  
  @foreach($grades as $menu_item)
    <li role="presentation"><a href="{{URL::to($menu_item->path)}}">{{$menu_item->display_name}}</a></li>

  @endforeach
       </ul>
 </nav>
  @endif
 
 
 